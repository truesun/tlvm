var timeline, lang;
var timelineDataCont = {
    "events": []
};
$(document).ready(function () {
    $('.history-timeline-tit-box').append('<div id="testInd"></div>');
    $('.history-timeline-tit-box').append('<div id="testData"></div>');
    $('.history-timeline-area').prepend('<div id="timeLineTag" class="time-line-tag-box"></div>');
    $('.history-timeline-area').append('<div id="timeLineNone" class="timeline-none"></div>');
    var srcVal = $('#searchkey').val();
    var apiLink, timeId;
    // lang = getCookie('CacheLang');
    lang = CookiesLang;
    var kind = getLinkVal('Kind');
    if (kind == '2') {
        timeId = getLinkVal('Id') || getCookie('Id');
    } else {
        timeId = getLinkVal('LIid') || getCookie('Id');
    }
    console.log('時期ID', timeId);
    if (lang == 'zh') {
        if (srcVal) {
            apiLink = '/Api/WebServiceCn?Type=3&id=' + srcVal;
        } else {
            apiLink = '/Api/WebServiceCn?Type=1&id=' + timeId;
        }
        getTimeLineCn(apiLink);
    } else {
        if (srcVal) {
            apiLink = '/Api/WebServiceEn?Type=3&id=' + srcVal;
        } else {
            apiLink = '/Api/WebServiceEn?Type=1&id=' + timeId;
        }
        getTimeLineEn(apiLink);
    }


    $('.tl-headline').find('a').each(function (ind, item) {
        $(this).attr('target', '_blank');
    });
});
function getTimeLineCn(apiLink, click) {
    $.get(apiLink, function (data) {
        if (data == "[]") {
            $('#timeLineNone').show();
            $('#timeLineNone').html('無資料。');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        var eventData = JSON.parse(data);
        $.each(eventData, function (ind, val) {
            var startTime, endTime, timeStamp, title, tagListL, tagList, tagListTxt, eventsListData;

            if (val.StarttimeDD !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM + '/' + val.StarttimeDD;
            } else if (val.StarttimeMM !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM;
            } else {
                startTime = val.StarttimeYY;
            }

            timeStamp = Date.parse(new Date(startTime));

            if (val.EndtimeDD !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM + '/' + val.EndtimeDD;
            } else if (val.EndtimeMM !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM;
            } else {
                endTime = val.EndtimeYY;
            }

            if (val.Title_url) {
                title = '<a href="' + val.Title_url + '" target="_blank">' + val.Title_tw + '</a>';
            } else {
                title = val.Title_tw;
            }

            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_tw + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }

            if (!val.EndtimeYY) {
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                    }
                    //val.Content_tw + tagListTxt
                    //'<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                }
            } else {
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "end_date": {
                        "year": val.EndtimeYY,
                        "month": val.EndtimeMM,
                        "display_date": endTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                    }
                    //val.Content_tw + tagListTxt
                    //'<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                }
            }
            timelineDataCont.events.push(eventsListData);
        });
    });

    if (click) {

        var TimeLineControlOptions = {
            language: 'zh-tw',
            scale_factor: '1',
        };
    } else {
        if (!timelineDataCont.events.length) {
            $('#timeLineNone').show();
            $('#timeLineNone').html('無資料。');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        var fKind = getLinkVal('Kind');
        var srcTimeId;
        if (fKind == '1') {
            srcTimeId = getLinkVal('Id') || getCookie('Lid');
        } else {
            srcTimeId = '0';
        }
        var mylistIndex;
        if (srcTimeId !== '0') {
            mylistIndex = timelineDataCont.events.findIndex(function (item) {
                return item.timeId == srcTimeId;
            });

            if (mylistIndex == '-1') {
                mylistIndex = 0;
            }
        } else {
            mylistIndex = 0;
        }

        var TimeLineControlOptions = {
            language: 'zh-tw',
            start_at_slide: mylistIndex,
            scale_factor: '1',
        };
    }

    timeline = new TL.Timeline('timelineArea', timelineDataCont, TimeLineControlOptions);
    //== 一開始進來要隱藏事件標籤 ==
    $('.his-time-tab').hide();
    tagClick();
}

function getTimeLineEn(apiLink, click) {
    $.get(apiLink, function (data) {
        if (data == "[]") {
            $('#timeLineNone').show();
            $('#timeLineNone').html('No Data.');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        var eventData = JSON.parse(data);
        $.each(eventData, function (ind, val) {
            var startTime, endTime, timeStamp, title, tagListL, tagList, tagListTxt, eventsListData;
            if (val.StarttimeDD !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM + '/' + val.StarttimeDD;
            } else if (val.StarttimeMM !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM;
            } else {
                startTime = val.StarttimeYY;
            }
            timeStamp = Date.parse(new Date(startTime));

            if (val.EndtimeDD !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM + '/' + val.EndtimeDD;
            } else if (val.EndtimeMM !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM;
            } else {
                endTime = val.EndtimeYY;
            }

            if (val.Title_url) {
                title = '<a href="' + val.Title_url + '" target="_blank">' + val.Title_en + '</a>';
            } else {
                title = val.Title_en;
            }

            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    if (val.Name_en !== '') {
                        tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_en + '</a>');
                    }
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }

            if (!val.EndtimeYY) {
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_en
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                    }
                    //val.Content_en + tagListTxt
                    //'<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                }
            } else {
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_en
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "end_date": {
                        "year": val.EndtimeYY,
                        "month": val.EndtimeMM,
                        "display_date": endTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                    }
                    //val.Content_en + tagListTxt
                    //'<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                }
            }
            timelineDataCont.events.push(eventsListData);
        });
    });

    if (click) {
        var TimeLineControlOptions = {
            language: 'en',
            scale_factor: '1',
        };
    } else {
        if (!timelineDataCont.events.length) {
            $('#timeLineNone').show();
            $('#timeLineNone').html('No Data.');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        var fKind = getLinkVal('Kind');
        var srcTimeId;
        if (fKind == '1') {
            srcTimeId = getLinkVal('Id') || getCookie('Lid');
        } else {
            srcTimeId = '0';
        }

        var mylistIndex;
        if (srcTimeId !== '0') {
            mylistIndex = timelineDataCont.events.findIndex(function (item) {
                return item.timeId == srcTimeId;
            });
            if (mylistIndex == '-1') {
                mylistIndex = 0;
            }
        } else {
            mylistIndex = 0;
        }

        var TimeLineControlOptions = {
            language: 'en',
            start_at_slide: mylistIndex,
            scale_factor: '1',
        };
    }

    timeline = new TL.Timeline('timelineArea', timelineDataCont, TimeLineControlOptions);
    //== 一開始進來要隱藏事件標籤 ==
    $('.his-time-tab').hide();
    tagClick();
}

function tagClick() {
    $('.tagBtn').on('click', function () {
        var tagId = $(this).data('tagid');
        var tagTxt = $(this).html();
        timelineDataCont = {
            "events": []
        };
        if (lang == 'zh') {
            apiLink = '/Api/WebServiceCn?Type=2&id=' + tagId;
            getTimeLineCn(apiLink, true);
            $('#timeLineTag').html('標籤：' + tagTxt);
            //= 顯示事件時期 標題隱藏 =
            $('.history-timeline-tit-box h2').hide();
            $('.his-time-tab').show();
        } else {
            apiLink = '/Api/WebServiceEn?Type=2&id=' + tagId;
            getTimeLineEn(apiLink, true);
            $('#timeLineTag').html('Tag：' + tagTxt);
            //= 顯示事件時期  標題隱藏 =
            $('.history-timeline-tit-box h2').hide();
            $('.his-time-tab').show();
        }
        $('.tl-headline').find('a').each(function (ind, item) {
            $(this).attr('target', '_blank');
        });
    });
}
