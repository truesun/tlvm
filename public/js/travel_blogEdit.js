var choseReqTxt;
$(document).ready(function () {
    //== 上頁返回 ==
    $('.blog-edit-cancle-btn').on('click', function () {
        history.back();
    });
    var actLang = CookiesLang;//動態使用
    // var lang = getCookie('CacheLang');//語系
    var reqTxt;
    lang == 'zh' ? reqTxt = '必填' : reqTxt = '必填';//Required
    lang == 'zh' ? choseReqTxt = '至少選擇一個' : choseReqTxt = '至少選擇一個';//Required
    //== 必填中英 ==
    $.extend($.validator.messages, {
        required: reqTxt,
    });
    var validator = $("#blogForm").validate({
        // rules: {
        //     blogType: 'required',
        // },
        messages: {
            blogType: choseReqTxt,
        },
        //- messages:{
        //-     account:{
        //-         required: reqTxt,
        //-     },
        //-     pwd:{
        //-         required: reqTxt,
        //-     },
        //- },
        errorPlacement: function (error, element) {
            if (element.is('input[name="blogType"]')) {
                error.appendTo($("#blogTypeBox"));
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            alert("完成");
            form.submit();
        }
    });

    $('#blogTypeBox').find('input[type="checkbox"]').each(function () {
        $(this).on('change', function () {
            typeChoose();
        });
    });
    // $('.blog-submit-btn').on('click', function () {
    //     typeChoose();
    //     $("#blogForm").submit();
    //     // console.log(typeCheckNum);
    //     // var _form = $("#blogForm").valid();
    //     // if (_form == true && typeCheckNum > 0) {
    //     //     //如果驗證都過了 就會執行這裡的動作
    //     //     // submit();
    //     // }
    // });
    //=== 上傳預覽圖片 ===
    //- $("#banner").on('change',function () {
    //-     readURL(this,'bannerImg');
    //- });
    changeImg('#banner', 'bannerPreview');
    changeImg('#blogImg', 'blogImgPreview');
    //=== 排序功能 ===
    new Sortable(blogSortArea, {
        handle: '.sort', // handle's class
        animation: 150
    });

    //=== 編輯功能 ===
    var lang = getCookie('CacheLang');//語系
    //= 排序盒子 =
    var $sortArea = $('#blogSortArea');
    //= 編輯流水號 =
    var noNum = 1;
    //= 編輯時已設欄位，取得最後流水號 =
    var noNumList = [];
    $('#blogSortArea').find('.blog-edit-box').each(function (ind, val) {
        var useNum = $(val).data('nonum');
        noNumList.push(useNum);
    });
    if (noNumList.length > 0) {
        noNumList.sort(function (a, b) {
            return b - a;
        });
        noNum = noNumList[0] + 1;
    } else {
        noNum = 1;
    }

    //= 標題 =
    $('#addTitBtn').on('click', function () {
        //預作語系
        var tit, note;
        lang == 'zh' ? tit = '段落標題' : tit = '段落標題';
        lang == 'zh' ? note = '請輸入標題' : note = '請輸入標題';
        var titId = 'blogTit' + noNum;
        var titDom = '<div class="blog-edit-box" data-nonum="' + noNum + '">' +
            '<div class="blog-edit-tit blog-edit-sort-tit">' +
            '<label for="' + titId + '">' + tit + '</label>' +
            '<div class="sort-box">' +
            '<div class="icon sort"><i class="fas fa-bars"></i></div>' +
            '<div class="icon del"><i class="fas fa-times"></i></div>' +
            '</div></div>' +
            '<div class="blog-input-box">' +
            '<input class="required blog-input-sty" type="text" id="' + titId + '" name="' + titId + '" placeholder="' + note + '">' +
            '</div></div>';
        $sortArea.append(titDom);
        noNum++;
    });
    //= 段落 =
    $('#addPartBtn').on('click', function () {
        //預作語系
        var tit, note, sizeTxt;
        lang == 'zh' ? tit = '段落' : tit = '段落';
        lang == 'zh' ? note = '段落內容...' : note = '段落內容...';
        lang == 'zh' ? sizeTxt = '字級' : sizeTxt = '字級';
        var partId = 'blogCont' + noNum;
        var partSizeId = 'blogContSize' + noNum;
        var partDom = '<div class="blog-edit-box" data-nonum="' + noNum + '">' +
            '<div class="blog-edit-tit blog-edit-sort-tit">' +
            '<label for="' + partId + '">' + tit + '</label>' +
            '<div class="sort-box"><div class="icon sort"><i class="fas fa-bars"></i></div>' +
            '<div class="icon del"><i class="fas fa-times"></i></div>' +
            '</div></div>' +
            '<div class="blog-input-box">' +
            '<textarea class="blog-edit-textarea required" id="' + partId + '" name="' + partId + '" placeholder="' + note + '"></textarea>' +
            '<div class="blog-edit-sel-box">' +
            '<div class="tit">' + sizeTxt + '</div>' +
            '<select class="travel-sel" id="' + partSizeId + '" name="' + partSizeId + '"><option value="1">14px</option><option value="2">16px</option><option value="3">18px</option><option value="4">20px</option></select>' +
            '</div></div></div>';
        $sortArea.append(partDom);
        noNum++;
    });
    //= 圖片 =
    $('#addImgBtn').on('click', function () {
        //預作語系
        var tit, imgLinkTxt, imgLink, leftTxt, centerTxt, rightTxt, noteOne, noteTwo;
        lang == 'zh' ? tit = '插入圖片' : tit = '插入圖片';
        lang == 'zh' ? imgLinkTxt = '輸入圖片說明' : imgLinkTxt = '輸入圖片說明';
        lang == 'zh' ? imgLink = '輸入連結 ex: https://xxx.com.tw' : imgLink = '輸入連結 ex: https://xxx.com.tw';
        lang == 'zh' ? leftTxt = '置左' : leftTxt = '置左';
        lang == 'zh' ? centerTxt = '置中' : centerTxt = '置中';
        lang == 'zh' ? rightTxt = '置右' : rightTxt = '置右';
        lang == 'zh' ? noteOne = '如圖檔寬度大於730px，將以滿版呈現' : noteOne = '如圖檔寬度大於730px，將以滿版呈現';
        lang == 'zh' ? noteTwo = '圖檔僅支援png、jpg、gif等格式' : noteTwo = '圖檔僅支援png、jpg、gif等格式';
        var imgId = 'blogImg' + noNum;
        var imgIdLink = 'blogImgLink' + noNum;
        var imgIdLinkTxt = 'blogImgLinkTxt' + noNum;
        var imgPlaceId = 'blogImgPlace' + noNum;
        var imgDom = '<div class="blog-edit-box" data-nonum="' + noNum + '">' +
            '<div class="blog-edit-tit blog-edit-sort-tit">' +
            '<label for="' + imgId + '">' + tit + '</label>' +
            '<div class="sort-box"><div class="icon sort"><i class="fas fa-bars"></i></div><div class="icon del"><i class="fas fa-times"></i></div>' +
            '</div></div>' +
            '<div class="blog-edit-img">' +
            '<input class="required" type="file" name="' + imgId + '" id="' + imgId + '"></div>' +
            '<div class="blog-input-box">' +
            '<input class="blog-input-sty" type="text" id="' + imgIdLinkTxt + '" name="' + imgIdLinkTxt + '" placeholder="' + imgLinkTxt + '">' +
            '<input class="blog-input-sty" type="text" id="' + imgIdLink + '" name="' + imgIdLink + '" placeholder="' + imgLink + '"></div>' +
            '<div class="blog-edit-sel-box">' +
            '<div class="note-box"><span class="note"><i class="fas fa-exclamation-circle"></i> ' + noteOne + '</span><span class="note"><i class="fas fa-exclamation-circle"></i> ' + noteTwo + '</span></div>' +
            '<select class="travel-sel travel-sel-place" id="' + imgPlaceId + '" name="' + imgPlaceId + '"><option value="1">' + leftTxt + '</option><option value="2">' + centerTxt + '</option><option value="3">' + rightTxt + '</option></select>' +
            '</div></div>';
        $sortArea.append(imgDom);
        noNum++;
        //綁圖片預覽功能
        changeImg('#' + imgId, imgId + 'Preview');
    });
    //= 連結 =
    $('#addLinkBtn').on('click', function () {
        //預作語系
        var tit, note, noteLink;
        lang == 'zh' ? tit = '連結' : tit = '連結';
        lang == 'zh' ? note = '輸入文字' : note = '輸入文字';
        lang == 'zh' ? noteLink = '輸入連結 ex: https://xxx.com.tw' : noteLink = '輸入連結 ex: https://xxx.com.tw';
        var linkTxtId = 'linkTxt' + noNum;
        var linkId = 'link' + noNum;
        var linkDom = '<div class="blog-edit-box" data-nonum="' + noNum + '">' +
            '<div class="blog-edit-tit blog-edit-sort-tit">' +
            '<label for="' + linkTxtId + '">' + tit + '</label>' +
            '<div class="sort-box"><div class="icon sort"><i class="fas fa-bars"></i></div><div class="icon del"><i class="fas fa-times"></i></div>' +
            '</div></div>' +
            '<div class="blog-input-box">' +
            '<input class="required blog-input-sty" type="text" id="' + linkTxtId + '" name="' + linkTxtId + '" placeholder="' + note + '">' +
            '<input class="required blog-input-sty" type="text" id="' + linkId + '" name="' + linkId + '" placeholder="' + noteLink + '">' +
            '</div></div>';
        $sortArea.append(linkDom);
        noNum++;
    });
    //= 引用 =
    $('#addQuoteBtn').on('click', function () {
        //預作語系
        var tit, note;
        lang == 'zh' ? tit = '引用' : tit = '引用';
        lang == 'zh' ? note = '引用內容...' : note = '引用內容...';
        var quoteId = 'blogQuote' + noNum;
        var quoteDom = '<div class="blog-edit-box" data-nonum="' + noNum + '">' +
            '<div class="blog-edit-tit blog-edit-sort-tit">' +
            '<label for="' + quoteId + '">' + tit + '</label>' +
            '<div class="sort-box"><div class="icon sort"><i class="fas fa-bars"></i></div><div class="icon del"><i class="fas fa-times"></i></div>' +
            '</div></div>' +
            '<div class="blog-input-box">' +
            '<textarea class="blog-edit-textarea required" id="' + quoteId + '" name="' + quoteId + '" placeholder="' + note + '"></textarea>' +
            '</div></div>';
        $sortArea.append(quoteDom);
        noNum++;
    });

    //= 刪除 =
    $(document).on('click', '.del', function () {
        $(this).parent().parent().parent().remove();
    });
});
//=== 上傳圖片 ===
function changeImg(id, pImg) {
    $(id).on('change', function () {
        var $previewImg = $('#' + pImg);//圖片dom，如果有更換須先移除上一個加入的
        $previewImg.remove();
        readURL(this, pImg);
    });
}
//=== 圖片預覽 ===
function readURL(input, previewImg) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var previewImgDom = '<img src="" id="' + previewImg + '">';//圖片img
            var changeId = $(input).attr('id');
            if (changeId == 'banner') {
                //因為banner圖片需要一個div包覆
                $(input).parent().find('.preview-box').append(previewImgDom);
            } else {
                $(input).parent().append(previewImgDom);
            }

            // var previewImgDom = '<img src="" id="' + previewImg + '">';//圖片img
            // $(input).parent().append(previewImgDom);
            var $previewImg = $('#' + previewImg);//圖片dom
            //- console.log(e.target.result);
            $previewImg.attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        $(input).parent().find('img').remove();
    }
}
var typeCheckNum;
//=== 文章分類至少選一判斷 ===
function typeChoose() {
    typeCheckNum = $('#blogTypeBox').find('input[type="checkbox"]:checked').length;
    // console.log(typeCheckNum);
    if (typeCheckNum > 0) {
        $('#blogTypeBox').find('.req-note').remove();
    } else {
        var hasReqNote = $('.req-note').is(':visible');
        if (!hasReqNote) {
            $('#blogTypeBox').append('<div class="req-note">' + choseReqTxt + '</div>');
        }
    }
}
