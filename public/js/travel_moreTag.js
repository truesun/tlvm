$(document).ready(function () {
    //如果超過這個高度才有更多鈕
    hideMoreBtn();
    //標籤打開 隱藏
    var $moreTagBtn = $('#travelQuoteTagMoreBtn');
    var $allTagBox = $('#travelQuoteAllTagBox');
    $moreTagBtn.on('click', function () {
        var tagOpen = $allTagBox.hasClass('act-open');
        if (tagOpen) {
            $allTagBox.removeClass('act-open');
            $moreTagBtn.find('i').removeClass('fa-arrow-up');
            $moreTagBtn.find('i').addClass('fa-arrow-down');
            //- var itag = 0;
            //- $('.tagshow').each(function (e) {
            //-     if (itag > 19) {
            //-         $(this).hide();
            //-     } else
            //-         $(this).show();
            //-     itag++;
            //- });
        } else {
            $allTagBox.addClass('act-open');
            $moreTagBtn.find('i').addClass('fa-arrow-up');
            $moreTagBtn.find('i').removeClass('fa-arrow-down');
            //- $('.tagshow').each(function (e) {
            //-     $(this).show();
            //- });
        }
    });
});

$(window).resize(function () {
    hideMoreBtn();
});

//如果超過這個高度才有更多鈕
function hideMoreBtn() {
    var tagsH = $('#travelQuoteAllInnerTagBox').outerHeight();
    if (tagsH < 43) {
        $('#travelQuoteTagMoreBtn').hide();
    } else {
        $('#travelQuoteTagMoreBtn').show();
    }
}