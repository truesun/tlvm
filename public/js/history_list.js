$(document).ready(function () {
    //標題切換
    // var lang = getCookie('CacheLang');
    var lang = CookiesLang;
    var actOne = true;
    $('#timeTit2').on('click', function () {
        if (actOne) {
            //== 開啟 時期 狀態
            if (lang == 'zh') {
                $('#timeTit span').html('時期代表標籤');
                $('#timeTit2 span').html('臺灣文學史代表標籤');
            } else {
                $('#timeTit span').html('Period tags');
                $('#timeTit2 span').html('Literary History tags');
            }
            $('#moreTagBtn').hide();
            $('#moreTagBtn2').show();
            $('#timeTagArea').hide();
            $('#timeTagArea2').show();
            hideMoreBtn2();
            actOne = false;
        } else {
            //== 開啟 臺灣文學史 狀態
            if (lang == 'zh') {
                $('#timeTit span').html('臺灣文學史代表標籤');
                $('#timeTit2 span').html('時期代表標籤');
            } else {
                $('#timeTit span').html('Literary History tags');
                $('#timeTit2 span').html('Period tags');
            }
            $('#moreTagBtn').show();
            $('#moreTagBtn2').hide();
            $('#timeTagArea').show();
            $('#timeTagArea2').hide();
            hideMoreBtn();
            actOne = true;
        }
    });

    //如果超過這個高度才有更多鈕
    hideMoreBtn();
    //標籤打開 隱藏
    var tagListData = [
        { 'tagBtn': '#moreTagBtn', 'tagBox': '#articleAllTagBox' },
        { 'tagBtn': '#moreTagBtn2', 'tagBox': '#articleAllTagBox2' }
    ]
    $.each(tagListData, function (ind, val) {
        var $btn = $(val.tagBtn);
        var $tagBox = $(val.tagBox);
        $btn.on('click', function () {
            var tagOpen = $tagBox.hasClass('act-open');
            if (tagOpen) {
                $tagBox.removeClass('act-open');
                $btn.find('i').removeClass('fa-angle-up');
                $btn.find('i').addClass('fa-angle-down');
            } else {
                $tagBox.addClass('act-open');
                $btn.find('i').addClass('fa-angle-up');
                $btn.find('i').removeClass('fa-angle-down');
            }
        });
    });
});
$(window).resize(function () {
    var showOne = $('#timeTagArea').is(':visible');
    if (showOne) {
        hideMoreBtn();
    }
    var showTwo = $('#timeTagArea2').is(':visible');
    if (showTwo) {
        hideMoreBtn2();
    }
});
//如果超過這個高度才有更多鈕
function hideMoreBtn() {
    var tagsH = $('#articleAllInnerTagBox').outerHeight();
    if (tagsH < 32) {
        $('#moreTagBtn').hide();
    } else {
        $('#moreTagBtn').show();
    }
}
function hideMoreBtn2() {
    var tagsH = $('#articleAllInnerTagBox2').outerHeight();
    if (tagsH < 32) {
        $('#moreTagBtn2').hide();
    } else {
        $('#moreTagBtn2').show();
    }
}