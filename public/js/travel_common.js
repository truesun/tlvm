var travelHamSwitch = false;
$(document).ready(function () {
    var $travelHam = $("#travelHam");
    var $travelNav = $('#travelNav');
    var windowW = $(window).width();

    if (windowW < 1200) {//768
        $travelNav.css({ 'display': 'none' });
    } else {
        $travelNav.css({ 'display': 'flex' });
    }
    $travelHam.on("click", function (e) {
        if (travelHamSwitch) {
            $travelHam.removeClass("is-active");
            //- $travelNav.css({ 'display': 'flex' });
            $travelNav.slideUp();
            travelHamSwitch = false;
        } else {
            $travelHam.addClass("is-active");
            //- $travelNav.css({ 'display': 'none' });
            $travelNav.slideDown();
            travelHamSwitch = true;
        }
    });

    var $opTravelSupNav = $('.item-mc');
    $opTravelSupNav.on('click', function () {
        windowWAct = $(window).width();
        if (windowWAct < 1200) {//768
            var $travelSupNav = $(this).children('.tsup-nav');
            var $icon = $(this).children('.icon');
            if ($travelSupNav.is(':visible')) {
                $icon.removeClass('icon-minus');
                $travelSupNav.slideUp();
            } else {
                $icon.addClass('icon-minus');
                $travelSupNav.slideDown();
            }
        }
    });
});
$(window).resize(function () {
    var windowW = $(window).width();

    if (windowW < 1200) {//768
        travelHamSwitch ? $('#travelNav').show() : $('#travelNav').hide();
    } else {
        $('#travelNav').css({ 'display': 'flex' });
        $('#travelHam').removeClass("is-active");
        travelHamSwitch = false;
        //- $('.n-nav-sup').removeAttr('style');
        $('.tsup-nav').removeAttr('style');
        $('.item-mc').find('.icon').removeClass('icon-minus');
    }
});