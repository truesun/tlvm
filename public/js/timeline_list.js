var timeline, lang;
var timelineData = {
    "events": []
};
$(document).ready(function () {
    $('.history-timeline-area').prepend('<div id="timeLineTag" class="time-line-tag-box"></div>');
    $('.history-timeline-area').append('<div id="timeLineNone" class="timeline-none"></div>');
    var srcVal = $('#searchkey').val();
    var apiLink;
    // lang = getCookie('CacheLang');
    lang = CookiesLang;
    // console.log('cookie是：', getCookie('CacheLang'))
    console.log('cookie是：', CookiesLang);
    if (lang == 'zh') {
        if (srcVal) {
            apiLink = '/Api/WebServiceCn?Type=3&id=' + srcVal;
        } else {
            apiLink = '/Api/WebServiceCn?Type=1&id=3';
        }
        getTimeLineCn(apiLink);
    } else {
        if (srcVal) {
            apiLink = '/Api/WebServiceEn?Type=3&id=' + srcVal;
        } else {
            apiLink = '/Api/WebServiceEn?Type=1&id=3';
        }
        getTimeLineEn(apiLink);
    }

    $('.tl-headline').find('a').each(function (ind, item) {
        $(this).attr('target', '_blank');
    });
});

function getTimeLineCn(apiLink) {
    $.get(apiLink, function (data) {
        if (data == "[]") {
            $('#timeLineNone').show();
            $('#timeLineNone').html('無資料。');
            $('#timelineArea').hide();
        } else {
            $('#timelineArea').show();
        }
        var eventData = JSON.parse(data);
        $.each(eventData, function (ind, val) {
            var startTime, endTime, title, tagListL, tagList, tagListTxt, eventsListData;
            if (val.StarttimeDD !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM + '/' + val.StarttimeDD;
            } else if (val.StarttimeMM !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM;
            } else {
                startTime = val.StarttimeYY;
            }

            if (val.EndtimeDD !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM + '/' + val.EndtimeDD;
            } else if (val.EndtimeMM !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM;
            } else {
                endTime = val.EndtimeYY;
            }

            if (val.Title_url) {
                title = '<a href="' + val.Title_url + '" target="_blank">' + val.Title_tw + '</a>';
            } else {
                title = val.Title_tw;
            }

            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_tw + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }

            if (!val.EndtimeYY) {
                eventsListData = {
                    "timeId": val.Lid,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                    }
                    //"text": val.Content_tw + tagListTxt
                    //"text": '<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                }
            } else {
                eventsListData = {
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "end_date": {
                        "year": val.EndtimeYY,
                        "month": val.EndtimeMM,
                        "display_date": endTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                    }
                    //"text": val.Content_tw + tagListTxt
                    // "text": '<div class="his-time-tab">' + val.LIName_tw + '</div>' + val.Content_tw + tagListTxt
                }
            }
            timelineData.events.push(eventsListData);
        });
    });


    var TimeLineControlOptions = {
        language: 'zh-tw',
        scale_factor: '1',
    };

    timeline = new TL.Timeline('timelineArea', timelineData, TimeLineControlOptions);
    tagClick();
}

function getTimeLineEn(apiLink) {
    $.get(apiLink, function (data) {
        if (data == "[]") {
            $('#timeLineNone').show();
            $('#timeLineNone').html('No Data.');
            $('#timelineArea').hide();
        } else {
            $('#timelineArea').show();
        }
        var eventData = JSON.parse(data);
        $.each(eventData, function (ind, val) {
            var startTime, endTime, title, tagListL, tagList, tagListTxt, eventsListData;

            if (val.StarttimeDD !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM + '/' + val.StarttimeDD;
            } else if (val.StarttimeMM !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM;
            } else {
                startTime = val.StarttimeYY;
            }

            if (val.EndtimeDD !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM + '/' + val.EndtimeDD;
            } else if (val.EndtimeMM !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM;
            } else {
                endTime = val.EndtimeYY;
            }

            if (val.Title_url) {
                title = '<a href="' + val.Title_url + '" target="_blank">' + val.Title_en + '</a>';
            } else {
                title = val.Title_en;
            }

            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    if (val.Name_en !== '') {
                        tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_en + '</a>');
                    }
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }

            if (!val.EndtimeYY) {
                eventsListData = {
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_en
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                    }
                    //val.Content_en + tagListTxt
                    // '<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                }
            } else {
                eventsListData = {
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_en
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "display_date": startTime
                    },
                    "end_date": {
                        "year": val.EndtimeYY,
                        "month": val.EndtimeMM,
                        "display_date": endTime
                    },
                    "text": {
                        "headline": title,
                        "text": '<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                    }
                    //val.Content_en + tagListTxt
                    //'<div class="his-time-tab">' + val.LIName_en + '</div>' + val.Content_en + tagListTxt
                }
            }
            timelineData.events.push(eventsListData);
        });
    });

    var TimeLineControlOptions = {
        language: 'en',
        scale_factor: '1',
    };

    timeline = new TL.Timeline('timelineArea', timelineData, TimeLineControlOptions);
    tagClick();
}

function tagClick() {
    $('.tagBtn').on('click', function () {
        var tagId = $(this).data('tagid');
        var tagTxt = $(this).html();
        timelineData = {
            "events": []
        };
        if (lang == 'zh') {
            apiLink = '/Api/WebServiceCn?Type=2&id=' + tagId;
            getTimeLineCn(apiLink);
            $('#timeLineTag').html('標籤：' + tagTxt);
        } else {
            apiLink = '/Api/WebServiceEn?Type=2&id=' + tagId;
            getTimeLineEn(apiLink);
            $('#timeLineTag').html('Tag：' + tagTxt);
        }
        $('.tl-headline').find('a').each(function (ind, item) {
            $(this).attr('target', '_blank');
        });
    });
}
