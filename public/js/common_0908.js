var scTop;
var $pageNav, $pageCont, pageNavTop, navH, pageSideW;
var hamSwitch = false;
$(document).ready(function () {
    // var apiDomain = api_Domain;//動態使用 - 上ＦＴＰ時請記得的改回來，還有語系抓取的
    var apiDomain = 'https://demotlvm.site/';//靜態使用
    $.ajaxSetup({
        async: false,
        crossDomain: true,
        beforeSend: function (xhr, options) {
            options.url = apiDomain + options.url;
        }
    });
    var itemAL = $('#navLiteraryList').find('a').length;
    if (itemAL == 3) {
        $('#navLiteraryList').addClass('sub-literaryList2');
    } else if (itemAL == 5) {
        $('#navLiteraryList').addClass('sub-literaryList3');
    }
    var itemBL = $('#navThemeList').find('a').length;
    if (itemBL == 2) {
        $('#navThemeList').addClass('sub-themeList2');
    }
    var windowWAct;
    var navHoverList = ['navLiterary', 'navTheme', 'navMaker'];
    $.each(navHoverList, function (key, val) {
        var $hoverList = $('#' + val + 'List');
        $hoverList.css({ 'top': '-10px' });
        $('#' + val + 'Btn, #' + val + 'List').on('mouseenter', function () {
            windowWAct = $(window).width();
            if (windowWAct > 1200) {
                var marginTop = $('nav').outerHeight();
                $hoverList.stop().animate({ top: marginTop - 2 }, 300);
            }
        });
        $('#' + val + 'Btn, #' + val + 'List').on('mouseleave', function () {
            windowWAct = $(window).width();
            if (windowWAct > 1200) {
                $hoverList.stop().animate({ top: '-10px' }, 300);
            }
        });
    });

    var $hamburger = $("#hamBtn");
    var $navItem = $('#navItem');
    var windowW = $(window).width();

    if (windowW < 1182) {
        $navItem.css({ 'display': 'none' });
    } else {
        $navItem.css({ 'display': 'flex' });
    }
    $hamburger.on("click", function (e) {
        if (hamSwitch) {
            $hamburger.removeClass("is-active");
            $navItem.css({ 'display': 'flex' });
            $navItem.slideUp();
            hamSwitch = false;
        } else {
            $hamburger.addClass("is-active");
            $navItem.css({ 'display': 'none' });
            $navItem.slideDown();
            hamSwitch = true;
        }
    });

    var $opSupNav = $('[opSupNav]');
    $opSupNav.on('click', function () {
        windowWAct = $(window).width();
        if (windowWAct < 1201) {
            var $navSup = $(this).children('.n-nav-sup');
            $navSup.is(':visible') ? $navSup.slideUp() : $navSup.slideDown();
        }
    });

    // $('#lang').on('change', function () {
    //     var langV = $(this).val();
    //     var form = document.querySelector("#langchanger");
    //     if (form) {
    //         form.innerHTML = '<input type="hidden" name="lang" value="' + langV + '"/>';
    //     } else {
    //         var form = document.createElement("form");
    //         var lang = document.createElement("input");
    //         lang.value = langV;
    //         lang.type = "hidden";
    //         lang.name = "lang";
    //         form.appendChild(lang);
    //         form.setAttribute("action", "/Home/Language");
    //         form.setAttribute("method", "post");
    //         form.id = "langchanger";
    //         document.body.appendChild(form);
    //     }
    //     form.submit();
    // });

    var lightBoxList = [{ 'btn': '#privacyBtn', 'box': '#privacyBox' }, { 'btn': '#questBtn', 'box': '#questBox' }, { 'btn': '#rPrivacyBtn', 'box': '#privacyBox' }]
    $.each(lightBoxList, function (ind, val) {
        var $btn = $(val.btn);
        var $box = $(val.box);
        $btn.on('click', function () {
            var boxShow = $box.is(':visible');
            if (boxShow) {
                $box.fadeOut();
                $('#mask').fadeOut();
            } else {
                $box.fadeIn();
                $('#mask').fadeIn();
            }
            boxShow ? $box.fadeOut() : $box.fadeIn();
        });
    });
    $('#mask,.light-close-btn').on('click', function () {
        $('#mask').fadeOut();
        $('.light-wrap,#routeLightWrap,#lightTxtWrap,#lightExImgWrap').fadeOut();
    });
    $('#lightTxtClose').on('click', function () {
        $('#mask').fadeOut();
        $('#lightTxtWrap').fadeOut();
    });

    $('#goTop,#travelGoTop').on('click', function () {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
    });


    $('.side-fixed').parent().prepend('<div id="pageNavTop"></div>');
    if (!!$('#pageNavTop').length) {
        pageNavTop = $('#pageNavTop').offset().top;
    }

    $(".scroll-cont,.light-scroll").mCustomScrollbar();

    //== 推廣地圖session ==
    var sideRecom = getSession('sideRecom');
    if (sideRecom !== 'on') {
        $('#travelEnterArea').removeClass('close');
    }
    //== 地圖推廣開關 ==
    $('#travelEnterBtn').on('click', function () {
        var hasClose = $('#travelEnterArea').hasClass('close');
        hasClose ? $('#travelEnterArea').removeClass('close') : $('#travelEnterArea').addClass('close');
        if (!sideRecom) {
            sessionStorage.setItem('sideRecom', 'on');
        }
    });
    //新增地圖推廣入口 travelEnterSw
    var travelEnterSw = new Swiper('#travelEnterSw', {
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: "#travelEnterNext",
            prevEl: "#travelEnterPrev",
        },
    });

    //新增推廣集章 lightStepSw
    var lightStepSw = new Swiper('#lightStepSw', {
        slidesPerView: 1,
        spaceBetween: 0,
        on: {
            slideChange() {
                var actIndex = this.activeIndex;
                // var actIndex = this.realIndex;//loop時使用 因為會出錯需要真實ind
                // console.log(actIndex);
                var actStep = actIndex + 1;
                $('.step-box').removeClass('act');
                $('#stepC' + actStep).addClass('act');
            }
        }
    });
    var stepCBtnList = ['stepC1', 'stepC2', 'stepC3', 'stepC4'];
    $.each(stepCBtnList, (ind, val) => {
        var $btn = $('#' + val);
        $btn.on('click', (e) => {
            lightStepSw.slideTo(ind);
        });
    });
    //== 集章步驟session ==
    var cStepLg = getSession('cStepLg');
    if (cStepLg !== 'on') {
        $('#lightStepWrap').removeClass('close');
        $('#mask').show();
        lightStepSw.update();
        bgFix();
    }
    $('#cStepSideBtn').on('click', (e) => {
        $('#lightStepWrap,#mask').fadeIn();
        lightStepSw.update();
        bgFix();
    });
    $('#lightStepClose').on('click', (e) => {
        $('#lightStepWrap,#mask').fadeOut();
        bgUnFix();
        if (!cStepLg) {
            sessionStorage.setItem('cStepLg', 'on');
        }
    });


    // var lang = getCookie('CacheLang');//靜態使用
    var lang = CookiesLang;//動態使用
    var pwdVTxt, pwdVLTxt;
    if (lang == 'zh') {
        pwdVTxt = '只能使用英數';
        pwdVLTxt = '最少五字元';
    } else {
        pwdVTxt = 'Not Valid';
        pwdVLTxt = 'Not Valid';
    }
    jQuery.validator.addMethod("pwdV", function (value, element) {
        return this.optional(element) || /^[A-Za-z0-9]/.test(value);
    }, pwdVTxt);
    jQuery.validator.addMethod("pwdV2", function (value, element) {
        return this.optional(element) || /^(?=.*\d)(?=.*[a-zA-Z])/.test(value);
    }, pwdVTxt);
    jQuery.validator.addMethod("pwdVL", function (value, element) {
        return this.optional(element) || value.length > 4;
    }, pwdVLTxt);

    var pageLink = window.location.pathname;
    // var actLang = CookiesLang;//動態使用
    var actLang = getCookie('CacheLang');//靜態使用
    var buildInfo = '<div class="error-page">' +
        '<div class="build-cont">' +
        '<div class="error-info">' +
        '<div class="error-txt build-txt">Sorry！網站建置中</div>' +
        '<div class="error-en">Coming Soon...</div>' +
        '<a class="btn" href="/' + actLang + '">回到首頁 / Back Home</a>' +
        '</div>' +
        '</div>' +
        '</div>';

    if (pageLink == '/en/Writer/WriterList' || pageLink == '/en/Writer/WriterStoryList' || pageLink == '/zh/Writer/WriterList' || pageLink == '/zh/Writer/WriterStoryList') {
        var writerL = $('.writer-cont').length;
        if (writerL == 0) {
            $('.page-wrap').append(buildInfo);
            $('.page-wrap').children('.main-cont').remove();
            $('.page-wrap').addClass('page-wrap-build');
        }

        // if (actLang == 'en') {
        //     $('.side-tag-box').remove();
        // }
    }

    if (pageLink == '/en/Theme/ExhibitionList') {
        var exBoxL = $('.exhibition-box').length;
        if (exBoxL == 0) {
            $('.page-wrap').append(buildInfo);
            $('.page-wrap').children('.main-cont').remove();
            $('.page-wrap').removeClass('page-gray-wrap');
            $('.page-wrap').addClass('page-wrap-build');
        }
    }

    if (pageLink == '/en/Theme/ExhibitionCont1') {
        $('.exhibition-part-a-tit').each(function () {
            var titA = $(this).html();
            if (titA == '') {
                $(this).parent().remove();
            }
        });
    }

    if (pageLink == '/en/Theme/ExhibitionCont2') {
        $('.exhibition-part-b-cont .tit').each(function () {
            var titB = $(this).html();
            if (titB == '') {
                $(this).parent().parent().remove();
            }
        });
    }

    if (pageLink == '/en/Create/CreatePlatform') {
        var exBoxL = $('.exhibition-box').length;
        var hasArt = $('.article-banner-cont').find('.article-banner-img').is(':visible');
        if (!hasArt) {
            $('.article-banner-area').append(buildInfo);
            $('.article-banner-area').children('.main-cont').remove();
            $('.creat-article-list-area').remove();
            $('.article-banner-area').addClass('article-build');
        }

        // if (actLang == 'en') {
        //     $('.creat-article-tag-box').remove();
        // }
    }

    if (pageLink == '/en/Writer/ThemeList') {
        $('.theme-box').each(function () {
            var themeTit = $(this).find('.tit').html();
            $(this).find('.route-btn-list a').each(function () {
                var routeTit = $(this).html();
                if (!routeTit) {
                    $(this).remove();
                }
            });
            if (!themeTit) {
                $(this).remove();
            }
        });
        var themeBoxL = $('.theme-box').length;
        if (themeBoxL == 0) {
            $('.page-wrap').append(buildInfo);
            $('.page-wrap').children('.main-cont').remove();
            $('.page-wrap').removeClass('inner-single-page');
            $('.page-wrap').addClass('page-wrap-build');
        }
    }

    if (pageLink == '/en/Game/GameList' || pageLink == '/zh/Game/GameList') {
        var gameBoxL = $('.game-box').length;
        if (gameBoxL == 0) {
            $('.page-wrap').append(buildInfo);
            $('.page-wrap').children('.main-cont').remove();
            $('.page-wrap').addClass('page-wrap-build');
        }
    }

    if (pageLink == '/en/History/HistoryList' && actLang == 'en') {
        // $('.inner-single-page').addClass('page-wrap');
        // $('.page-wrap').append(buildInfo);
        // $('.page-wrap').children('.main-cont').remove();
        // $('.page-wrap').removeClass('inner-single-page');
        // $('.page-wrap').addClass('page-wrap-build');
    }

    if (pageLink == '/en') {
        $('#LiteraryBanner').find('.swiper-slide').each(function () {
            var areaTit01 = $(this).find('.literary-tit .tit').html();
            if (areaTit01 == '') {
                $(this).remove();
            }
        });
        var LiteraryL = $('#LiteraryBanner').find('.swiper-slide').length;
        if (LiteraryL == 0) {
            $('#indArea01').remove();
        }

        $('.writer-cont').each(function () {
            var areaTit02 = $(this).find('.writer-info h3').html();
            if (areaTit02 == '') {
                $(this).remove();
            }
        });
        var writerL = $('.writer-cont').length;
        if (writerL == 0) {
            $('#indArea02').remove();
        }

        $('.ind-theme-box').each(function () {
            var areaTit03 = $(this).find('.ind-theme-txt h3').html();
            if (areaTit03 == '') {
                $(this).remove();
            }
        });
        var themeL = $('.ind-theme-box').length;
        if (themeL == 0) {
            $('#indArea03').remove();
        }
    }

    if (pageLink == '/en/Theme/ExhibitionArticleCont' && actLang == 'en') {
        var btnL = $('#articlePageBox').find('a').length;
        var nextTit = $('#articlePageBox').find('.next-box .page-info .name').html();
        console.log(nextTit);
        if (nextTit == '') {
            $('#articlePageBox').find('.next-box').remove();
        }
        if (nextTit == '' & btnL == 1) {
            console.log('空直');
            $('#articlePageBox').remove();
        }
    }
});

$(window).resize(function () {
    var windowW = $(window).width();

    if (windowW < 1182) {
        hamSwitch ? $('#navItem').show() : $('#navItem').hide();
    } else {
        $('#navItem').css({ 'display': 'flex' });
        $('#hamBtn').removeClass("is-active");
        hamSwitch = false;
        $('.n-nav-sup').removeAttr('style');
    }

    navH = $('#navArea').outerHeight();
    if (!!$('#pageNavTop').length) {
        pageNavTop = $('#pageNavTop').offset().top;
    }

    pageSideW = $('.page-side').width();
    $('.side-fixed').css({ 'width': pageSideW + 'px' });

    if (windowW < 769) {
        $('.side-fixed').removeAttr('style');
    }
});

$(window).scroll(function () {
    var windowScrollNum = $(window).scrollTop();
    var $fixNav = $('#fixNav');
    var $nav = $('#navFixBox');
    var navH = $nav.outerHeight();
    var fixH = $('.nav-info-area').outerHeight();
    if (windowScrollNum > fixH) {
        $fixNav.css({ 'padding-top': navH });
        $nav.css({ 'position': 'fixed', 'top': '0' });
    } else {
        $fixNav.css({ 'padding-top': '0' });
        $nav.css({ 'position': 'relative', 'top': '0' })
    }

    if (windowScrollNum > 300) {
        $('#goTop').css({ 'bottom': '10px' });
        fixGoTop();
        //== 地圖推廣 ==
        $('#travelEnterArea').addClass('fixed');
    } else {
        $('#goTop').css({ 'bottom': '-100px', 'top': 'auto' });
        $('#goTop').removeClass('gotop-fix');
        //== 地圖推廣 ==
        $('#travelEnterArea').removeClass('fixed');
    }

    //= 地景網 gotop =
    if (windowScrollNum > 300) {
        $('#travelGoTop').css({ 'bottom': '10px' });
        fixTravelGoTop();
    } else {
        $('#travelGoTop').css({ 'bottom': '-100px', 'top': 'auto' });
        $('#travelGoTop').removeClass('gotop-fix');
    }

    //== 地圖推廣 ==
    // var windowW = $(window).width();
    // if (windowScrollNum > 300 && windowW > 768) {
    //     $('#travelEnterArea').addClass('fixed');
    // } else {
    //     $('#travelEnterArea').removeClass('fixed');
    // }

    var fixData = {
        'topNav': '#navArea',
        'sideNav': '.side-fixed',
        'pageCont': '.page-cont',
        'footer': 'footer',
        'topMargin': 10,
        'bottomMargin': 80,
        'sideContW': '.page-side'
    };
    scrollNAvFix(fixData);
});

function alertTxt(txt) {
    $('#mask,#lightTxtWrap').fadeIn();
    $('#lightTxt').html(txt);
}

function fixGoTop() {
    var windowScrollNum = $(window).scrollTop();
    var windowH = $(window).height();
    var $footer = $('footer');
    if ($footer.length) {
        var footerTop = $('footer').offset().top;
        var $goTop = $('#goTop');
        if ($goTop.length) {
            // var gotopH = $goTop.height() + 10;
            var gotopH = $goTop.height();
            var gotopHhelf = gotopH / 2;
            if (windowScrollNum + windowH - gotopHhelf - 10 > footerTop) {
                $goTop.addClass('gotop-fix');
                $goTop.css({ 'bottom': 'auto', 'top': '-' + gotopHhelf + 'px' });
            } else {
                $goTop.removeClass('gotop-fix');
                $goTop.css({ 'bottom': '10px', 'top': 'auto' });
            }
        }
        //== 地圖推廣 ==
        var $travelEnterArea = $('#travelEnterArea');
        if ($travelEnterArea.length) {
            var travelEnterAreaH = $travelEnterArea.outerHeight();
            var gotopH = $goTop.height();
            var gotopHhelf = gotopH / 2;
            var travelEnterAreaHTop = travelEnterAreaH + gotopHhelf + 10;// 這裡的10是距離top的距離
            if (windowScrollNum + windowH - gotopHhelf - 10 > footerTop) {// 10是top距離footer的距離
                $travelEnterArea.addClass('gotop-fix');
                $travelEnterArea.css({ 'bottom': 'auto', 'top': '-' + travelEnterAreaHTop + 'px' });
            } else {
                $travelEnterArea.removeClass('gotop-fix');
                $travelEnterArea.removeAttr('style');
            }
            // if (windowScrollNum + windowH > footerTop) {
            //== 兩個位置 ==
            // var windowW = $(window).width();
            // if (windowW > 768) {
            //     //如果大於767一樣卡在top上
            //     if (windowScrollNum + windowH - gotopHhelf - 10 > footerTop) {
            //         $travelEnterArea.addClass('gotop-fix');
            //         $travelEnterArea.css({ 'bottom': 'auto', 'top': '-' + travelEnterAreaHTop + 'px' });
            //     } else {
            //         $travelEnterArea.removeClass('gotop-fix');
            //         $travelEnterArea.removeAttr('style');
            //     }
            // } else {
            //     //如果小於768，要自己卡在footer上
            //     console.log(travelEnterAreaH);
            //     if (windowScrollNum + windowH - 10 > footerTop) {//這裡的 10 是物件bottom的距離
            //         $travelEnterArea.addClass('gotop-fix');
            //         $travelEnterArea.css({ 'bottom': 'auto', 'top': '-' + travelEnterAreaH + 'px' });
            //     } else {
            //         $travelEnterArea.removeClass('gotop-fix');
            //         $travelEnterArea.removeAttr('style');
            //     }

            // }
        }
    }
}

//== 地景網 gotop ==
function fixTravelGoTop() {
    var windowScrollNum = $(window).scrollTop();
    var windowH = $(window).height();
    var $footer = $('.travel-footer-area');
    if ($footer.length) {
        var footerTop = $footer.offset().top;
        var $goTop = $('#travelGoTop');
        if ($goTop.length) {
            var gotopH = $goTop.height();
            var gotopHhelf = gotopH / 2;
            //往上滑的高度 + 上螢幕高度 - gotop一半高 - bottom: 10px 的高度 > footer的距離body top 的高度
            if (windowScrollNum + windowH - gotopHhelf - 10 > footerTop) {
                $goTop.addClass('gotop-fix');
                $goTop.css({ 'bottom': 'auto', 'top': '-' + gotopHhelf + 'px' });
            } else {
                $goTop.removeClass('gotop-fix');
                $goTop.css({ 'bottom': '10px', 'top': 'auto' });
            }
        }
    }
}

function getLinkVal(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]); return null;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function bgFix() {
    scTop = $(document).scrollTop();
    $('body').css({ 'top': 0, 'position': 'fixed', 'width': '100%' });
}
function bgUnFix() {
    $('body').removeAttr('style');
    $('html,body').animate({ scrollTop: scTop }, 0);
}

function scrollNAvFix(fixData) {
    var scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
    var windowW = $(window).width();

    var $footer = $(fixData.footer);
    if ($footer.length) {
        var footerTop = $footer.offset().top;
    } else {
        return;
    }

    navH = $(fixData.topNav).outerHeight();
    var navHPlus = navH + fixData.topMargin;

    pageSideW = $('.page-side').width();

    $pageNav = $(fixData.sideNav);
    $pageCont = $(fixData.pageCont);
    var pageNavH = $pageNav.outerHeight();
    var pageContH = $pageCont.outerHeight();

    var pageNavFixTop = pageNavTop - navHPlus;

    var limtFooter = navHPlus + pageNavH;
    var fixNavBottom = footerTop - limtFooter;
    var pageNavFixBottom = fixNavBottom - fixData.bottomMargin;

    if (pageNavH != pageContH && windowW > 768) {
        if (scrollTop > pageNavFixTop) {

            $pageNav.css({ 'position': 'fixed', 'top': navHPlus, 'width': pageSideW + 'px' });

            if (scrollTop > pageNavFixBottom) {
                $pageNav.parent().css({ 'position': 'relative' });
                $pageNav.css({ 'position': 'absolute', 'bottom': '0', 'top': 'unset' });
            } else {
                $pageNav.removeAttr('style');
                $pageNav.parent().removeAttr('style');
                $pageNav.css({ 'position': 'fixed', 'top': navHPlus, 'width': pageSideW + 'px' });
            }
        } else if (scrollTop < pageNavFixTop) {
            $pageNav.removeAttr('style');
        }
    }
}

//sessionStorage取得 (如果沒有值 原本會取得null 改成取的空值)
function getSession(get) {
    var _sessionV = sessionStorage.getItem(get);
    // _sessionV != null ? _sessionV = _sessionV : _sessionV = '';
    // return _sessionV;
    if (_sessionV != null) {
        //return sessionStorage.getItem(get);
        return _sessionV;
    } else {
        return '';
    }
}
