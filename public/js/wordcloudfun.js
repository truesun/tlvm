const tagCanvas = document.querySelector("#wordCloudBox");
let fontSize, rotate;
var tagArry = [];
const enTagArry = [];
var colors = ['#4297ab', '#40c9e0', '#f35b5b', '#98ce44', '#ffae48'];

$(document).ready(function () {
    var linkLang = window.location.pathname;
    var lang = CookiesLang;
    if (lang == '' && linkLang == '/') {
        lang = 'zh';
    } else if (lang == '' && linkLang == '/zh') {
        lang = 'zh';
    } else if (lang == '' && linkLang == '/en') {
        lang = 'en';
    }
    console.log(linkLang);
    var apiLink;
    lang == 'zh' ? $('#wordCloudArea').addClass('cn') : $('#wordCloudArea').addClass('en');
    lang == 'zh' ? rotate = 0 : rotate = 0;
    lang == 'zh' ? apiLink = '/Api/WordArt?Type=1' : apiLink = '/Api/WordArt?Type=2';
    $.get(apiLink, function (data) {
        var dataTxt = data.split('],[');

        var dataList = dataTxt[0] + ']';
        console.log(JSON.parse(dataList));
        var wordList = JSON.parse(dataList);
        tagArry = [];
        $.each(wordList, function (ind, val) {
            var { name, Atotal, Link } = val;
            if (name != '') {
                tagArry.push([name, Number(Atotal), Link]);
            }
        });

        var colorsList = '[' + dataTxt[1];
        colors = JSON.parse(colorsList);
        console.log(colors);
    });
    const winw = document.body.offsetWidth;

    console.log(lang);
    if (winw >= 481 && lang === 'zh') {
        fontSize = 20;
    } else if (winw < 480 && lang === 'zh') {
        fontSize = 12;
    } else if (winw >= 481 && lang === 'en') {
        fontSize = 16;
    } else if (winw < 481 && lang === 'en') {
        fontSize = 10;
    }

    tagArry.length != 0 ? setWordCloud(tagArry, colors, fontSize, rotate) : $('#wordCloudArea').remove();
});
let resizeTimer = null;
$(window).resize(function () {
    const winw = document.body.offsetWidth;
    if (winw >= 481 && lang === 'zh') {
        fontSize = 20;
    } else if (winw < 480 && lang === 'zh') {
        fontSize = 12;
    } else if (winw >= 481 && lang === 'en') {
        fontSize = 16;
    } else if (winw < 481 && lang === 'en') {
        fontSize = 10;
    }
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        tagArry.length != 0 ? setWordCloud(tagArry, colors, fontSize, rotate) : $('#wordCloudArea').remove();
    }, 500);
});
//== 啟動文字雲 ==
function setWordCloud(tagArry, colors, fontSize, rotate) {
    var option = {
        list: tagArry,
        classes: "tag-cloud-item",
        gridSize: Math.round(5 * $('#wordCloudBox').width() / 1200) + 3,
        weightFactor: fontSize,

        drawOutOfBound: false,
        shrinkToFit: false,
        fontFamily: '"Noto Sans TC", sans-serif',
        color: function (word, weight) {
            //4
            if (weight === 1) {
                return colors[0];
            } else if (weight === 2) {
                return colors[1];
            } else if (weight === 3) {
                return colors[2];
            } else if (weight === 4) {
                return colors[3];
            } else if (weight === 5) {
                return colors[4];
            }
        },
        rotateRatio: rotate,
        rotationSteps: 2,
        backgroundColor: 'transparent',
        shuffle: false,
        clearCanvas: false,
        click: (item) => {
            clickItem(item);
        }
    };
    WordCloud(tagCanvas, option);
    tagCanvas.addEventListener('wordcloudstop', function (e) {
        document.querySelectorAll('.tag-cloud-item').forEach(function (element) {
            const text = element.innerHTML;
            element.innerHTML = `<a target="_blank" style="color: inherit;">${text}</a>`;
        });
        $('.tag-cloud-item').find('a').each(function (ind, val) {
            var valTxt = $(val).text();
            if (valTxt == '') {
                $(val).remove();
            }
        });
    });
}
function clickItem(item) {
    window.open(item[2], '_blank');
}