let scrollBtnContList = [];
let scrollContList = [];
$(document).ready(function () {
    var mapEvent = getLinkVal('Eventid');
    if (mapEvent) {
        slideTo('trackCont2');
    }

    $('#writerTrackNavMbSel').on('click', (e) => {
        const $navList = $('#writerTrackNavList');
        const navShow = $navList.is(':visible');
        navShow ? $navList.slideUp() : $navList.slideDown();
    });
    $('.track-nav').each((ind, val) => {
        const btn = $(val).attr('id');
        const btnId = btn.replace('trackNav', '');
        const cont = 'trackCont' + btnId;
        scrollBtnContList.unshift({ 'btn': btn, 'cont': cont });
    });
    // 擁有的 cont
    $('.writer-track-tit').each((ind, val) => {
        const cont = $(val).attr('id');
        scrollContList.unshift(cont);
    });
    scrollContList.push('trackCont1');
    console.log(scrollBtnContList);
    console.log(scrollContList);
    // 找出多餘項目
    const extraItems = scrollBtnContList.filter(item => !scrollContList.includes(item.cont));
    // 從 scrollBtnContList 中移除多餘項目
    const updatedArr1 = scrollBtnContList.filter(item => !extraItems.some(extra => extra.cont === item.cont));
    console.log(updatedArr1);
    scrollBtnContList.length = 0;
    scrollBtnContList = updatedArr1;
    console.log(scrollBtnContList);
    // 移除btn項目
    for (item of extraItems) {
        $('#' + item.btn).remove();
    }

    //=== nav點擊滑動 ===
    //- $('#track-nav')
    //- var scrollBtnContList = ['trackNav10','trackNav9','trackNav8','trackNav7','trackNav6','trackNav5','trackNav4','trackNav3', 'trackNav2', 'trackNav1'];
    $.each(scrollBtnContList, (ind, val) => {
        const $btn = $('#' + val.btn);
        //- console.log(val.btn,val.cont)
        $btn.on('click', (e) => {
            console.log(val.btn, val.cont)
            slideTo(val.cont);
            const windowW = $(window).width();
            if (windowW <= 1024) {
                $('#writerTrackNavList').slideUp();
            }
        });
    });

    //=== 著作列表項目 更多按鈕 ===
    bookVideoMore();

    //=== 開關 video ===
    $('.videoOpen').on('click', (e) => {
        const videoId = $(e.currentTarget).data('videoid');
        const videoTit = $(e.currentTarget).next().text();
        const videoNote = $(e.currentTarget).next().next().text();

        const videoUrl = 'https://www.youtube.com/embed/' + videoId;
        $('#writerVideo').attr('src', videoUrl);
        $('#writerVideoTit').html(videoTit);
        $('#writerVideoNote').html(videoNote);
        $('#writerVideoArea').fadeIn(300);
        bgFix();
    });
    $('#writerVideoClose,#writerVideoArea').on('click', (e) => {
        $('#writerVideoArea').fadeOut(300);
        $('#writerVideo').attr('src', '');
        bgUnFix();
    });
    // 點擊圖片時阻止事件冒泡，避免觸發關閉操作
    $('#writerVideoTit,#writerVideoNote').on('click', function (event) {
        event.stopPropagation();
    });

    // // = 如果無圖片時，移除圖片 =
    // $('.writer-track-year-box').each((ind, item) => {
    //     console.log(ind);
    //     console.log(item);
    //     const imgSrc = $(item).find('img').attr('src');
    //     if (imgSrc == '/img/default_src.jpg') {
    //         $(item).find('.img').remove();
    //         $(item).addClass('noimg');
    //     }
    // });

    //- $('#storyContBox').mCustomScrollbar();

    //- var lang = getCookie('CacheLang');
    //- //- $(document).on('click', '.tagBtn', function () {
    //- //- 	var tagId = $(this).data('tagid');
    //- //- 	tagSrcPage(tagId);
    //- //- });

    //- var $moreMapTagBtn = $('#moreMapTagBtn');
    //- var $MapTagBox = $('#allMapTagBox');

    //- var tagOpen = false;
    //- $moreMapTagBtn.on('click', function () {
    //- 	var tabBtnTxt;
    //- 	if (tagOpen) {
    //- 		$MapTagBox.removeClass('open');
    //- 		$moreMapTagBtn.find('i').removeClass('fa-arrow-to-bottom');
    //- 		$moreMapTagBtn.find('i').addClass('fa-arrow-to-top');
    //- 		lang == 'zh' ? tabBtnTxt = '展開' : tabBtnTxt = 'More';
    //- 		$moreMapTagBtn.find('span').html(tabBtnTxt);

    //- 		$('.map-all-tag').mCustomScrollbar("destroy");
    //- 		tagOpen = false;
    //- 	} else {
    //- 		$MapTagBox.addClass('open');
    //- 		$moreMapTagBtn.find('i').addClass('fa-arrow-to-bottom');
    //- 		$moreMapTagBtn.find('i').removeClass('fa-arrow-to-top');
    //- 		lang == 'zh' ? tabBtnTxt = '收合' : tabBtnTxt = 'Close';
    //- 		$moreMapTagBtn.find('span').html(tabBtnTxt);

    //- 		$('.map-all-tag').mCustomScrollbar();
    //- 		tagOpen = true;
    //- 	}
    //- });

    //- $(document).on('click', '.tag', function () {
    //- 	var indEvent = $(this).data('tagid');
    //- 	var $eventTagBox = $('#eventsTag' + indEvent);
    //- 	var eventTagBoxShow = $eventTagBox.is(':visible');
    //- 	if (eventTagBoxShow) {
    //- 		$('.tag').removeClass('act');
    //- 		$('.map-event').hide();
    //- 		$('#mapEventBox').hide();
    //- 	} else {
    //- 		$('.tag').removeClass('act');
    //- 		$(this).addClass('act');
    //- 		if ($eventTagBox.length) {
    //- 			$('.map-event').hide();
    //- 			$('#mapEventBox').show();
    //- 			$eventTagBox.show();
    //- 		} else {
    //- 			$('.map-event').hide();
    //- 			$('#mapEventBox').hide();
    //- 		}
    //- 	}
    //- });
});
$(window).resize(function () {
    const windowW = $(window).width();
    console.log(windowW);
    if (windowW <= 1024) {
        $('.track-nav').removeClass('act');
    } else {
        const $navList = $('#writerTrackNavList');
        $('#writerTrackNavList').removeAttr('style');
    }

    //=== 著作列表項目 更多按鈕 ===
    bookVideoMore();
});
$(window).scroll(function () {
    var windowScrollNum = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
    //=== 滾動到第一個 要出現側邊 ===
    //- var areaFirstH = $('#indCont1').offset().top - 20;
    //- if (windowScrollNum > areaFirstH) {
    //- 	$("#sideActArea").addClass('on');
    //- } else {
    //- 	$("#sideActArea").removeClass('on');
    //- }
    const navH = $('#navFixBox').outerHeight();
    const navWriterH = $('#writerTrackNavArea').outerHeight();
    const actH = navH + navWriterH + 20;

    //== 滑到最底部時的話面上的高度 ==
    const pageH = $('body').height();
    const windowH = $(window).height();
    const lastH = pageH - windowH - 10;
    //- console.log(lastH);
    //=== 滾動距離側邊act ===
    //- var scrollList = ['trackCont10','trackCont9','trackCont8','trackCont7','trackCont6','trackCont5','trackCont4','trackCont3', 'trackCont2', 'trackCont1'];
    $.each(scrollBtnContList, function (ind, val) {
        var areaH = $('#' + val.cont).offset().top - actH;//自己的上方高度(-10多一點距離滾到這才會上act)
        //- console.log(ind);
        //- console.log(val);
        //- console.log('windowScrollNum',windowScrollNum);
        //- console.log('areaH',areaH);
        if (windowScrollNum < areaH) {
            //- console.log('= 來1 =');
            //=== div從最下面的元素開始比較，如果滾動距離還小於自己的話，要在array的下一個加上狀態 ===
            //=== 因為超過自己會下在自己但還是在上一個的區域，所以要上在下一個上 ===
            //=== 滾動距離沒有超過 標題2，所以狀態要下在標題1  ===
            //ex:
            //=== 1 === (超過這)
            //= cont 1 =
            //=== 2 === (上滾大於這，不大於上一個，act會在這，但還沒超過 所以要下下一個[array已倒序])
            //= cont 2 =
            // console.log('=== item ===',scrollList[ind+1]);
            if (scrollBtnContList[ind + 1]) {//超過第一個之前會找不到下一個(array)，所以要過濾
                var actId = scrollBtnContList[ind + 1]['cont'].replace('trackCont', '');
                reSetSideAct(actId);
                //- console.log('1',actId);

            } else {
                console.log('不到dom第一個高度');
                //還沒到第一個要移除狀態
                //- $('#actDotBox').find('.dot').removeClass('act');
            }
            //最後一個不夠到達時，計算高度，就下狀態(是array第一個，dom最後一個)
            if (windowScrollNum > lastH) {
                //- console.log('= 來3 =');
                const actId = scrollBtnContList[0]['cont'].replace('trackCont', '');
                //- console.log('111',actId);
                reSetSideAct(actId);
            }
        } else if (windowScrollNum > areaH && ind == 0) {
            //- console.log('= 來2 =');
            //= 如果超過，又剛好是第一個，就直接下在自己，因為沒有下一個了 =
            var actId = val['cont'].replace('trackCont', '');
            reSetSideAct(actId);
            //- console.log('2',actId);
        }
        //- else if (windowScrollNum > lastH) {//最後一個不夠到達時，計算高度，就下狀態(好像不需要 改到上面去 #lastH)
        //- 	//- console.log('= 來3 =');
        //- 	const actId = scrollList[0].replace('trackCont', '');
        //- 	//- console.log('111',actId);
        //- 	reSetSideAct(actId);
        //- }
        //- console.log('-------');
    });
});
//=== 狀態重置 ===
function reSetSideAct(num) {
    var $actDot = $('#trackNav' + num);
    //- var actTit = $('#indTit' + num).html();
    //- console.log(actTit);
    //- $('#sideActTit').html(actTit);
    const windowW = $(window).width();
    if (windowW > 1024) {
        $('#writerTrackNavList').find('.track-nav').removeClass('act');
        $actDot.addClass('act');
    }
    //= 下拉單文字 =
    const actTxt = $actDot.html();
    $('#writerTrackNavMbSel span').html(actTxt);
}

//=== 點擊滑動 ===
const slideTo = (cont) => {
    const navH = $('#navFixBox').outerHeight();
    const navWriterH = $('#writerTrackNavArea').outerHeight();
    const actH = navH + navWriterH + 15;
    $('html,body').stop().animate({ scrollTop: ($('#' + cont).offset().top) - actH }, 'slow');
}

//=== 書本項目列表按鈕和顯示數量 ===
const bookVideoMore = () => {
    const windowW = $(window).width();
    $('.writer-track-more-book').hide();
    $('.writer-track-book-cont').each((ind, val) => {
        const $itemEq3 = $(val).children('.writer-track-book-box').eq(3);
        const $itemEq4 = $(val).children('.writer-track-book-box').eq(4);
        if (windowW <= 768) {
            $itemEq3 && $itemEq3.hide();
            $itemEq4 && $itemEq4.hide();
            const num = $(val).find('.writer-track-book-box:visible').length;
            num == 3 && $(val).parent().children('.writer-track-more-book').show();
        } else {
            $itemEq3 && $itemEq3.show();
            $itemEq4 && $itemEq4.show();
            const num = $(val).find('.writer-track-book-box:visible').length;
            num == 5 && $(val).parent().children('.writer-track-more-book').show();
            num < 5 && $(val).parent().children('.writer-track-more-book').hide();
        }
    });
    //= 作者聲影 =
    const $videoCont = $('.writer-track-video-cont');
    const $itemViedoEq2 = $videoCont.children('.writer-track-video-box').eq(2);
    const $itemViedoEq3 = $videoCont.children('.writer-track-video-box').eq(3);
    if (windowW <= 768) {
        $itemViedoEq2 && $itemViedoEq2.hide();
        $itemViedoEq3 && $itemViedoEq3.hide();
        const num = $videoCont.find('.writer-track-video-box:visible').length;
        num == 2 && $videoCont.parent().children('.writer-track-more-book').show();
    } else {
        $itemViedoEq2 && $itemViedoEq2.show();
        $itemViedoEq3 && $itemViedoEq3.show();
        const num = $videoCont.find('.writer-track-video-box:visible').length;
        num == 4 && $videoCont.parent().children('.writer-track-more-book').show();
        num < 4 && $videoCont.parent().children('.writer-track-more-book').hide();
    }
}