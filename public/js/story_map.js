mapboxgl.accessToken = 'pk.eyJ1IjoidHJ1ZXN1biIsImEiOiJja25nMDBzbmUwamZpMnhtZXlzNXM2YnFqIn0.F2UM5EyLSGmv_lbwkO_4LA';
var map, popup, mapLanguage;//因為設置為function啟動 需要宣告在最外層才可以共同使用
var actThem = 'routeLife1';//== 當前主題 == // routeLife1
var actNum = 0;//== 當前point資料位置 ==
var defaultThemeName = '';// 葉石濤大事記
var actMapKeyId = '';//當前主題key 事件使用
var srcBtnClick = false;//如果用搜尋進來的情況，點選搜尋的話 不可以再跑搜尋事件
// var mapDataList = [];
var pathColorList = ['#CB5050', '#f1b43e', '#4ab93d'];//#f1b43e,#f1d53e//'#50A4CB', '#ca83cb'
var startColorBtn = [];//圖例和按鈕顏色

$(document).ready(function () {
    $('#goTop').remove();
    var apiLink, GroupApiLink, apiThemeLink;
    //=== 作家行跡或地圖故事 ===
    var pageLink = window.location.pathname;
    var pageLinkList = pageLink.split('/');
    var pageActInd = pageLinkList.length - 1;
    var pageAct = pageLinkList[pageActInd];//WriterCont Or WriterStoryCont
    var themeId;
    if (pageAct == 'WriterCont') {
        themeId = 1;
    } else if (pageAct == 'WriterStoryCont') {
        themeId = 2;
    }

    // var lang = getCookie('CacheLang');
    var lang = CookiesLang;
    var mapKey = getLinkVal('Wid') || getCookie('Key');
    actMapKeyId = mapKey;
    lang == 'zh' ? mapLanguage = "zh-Hant" : mapLanguage = "en";
    if (lang == 'zh') {
        //== 設置地圖 ==
        apiLink = '/Api/WebServiceMapCn?Type=1&key=' + mapKey;
        // apiLink = '/WebServiceMapCn.aspx?Type=1&key=' + mapKey;
        loadSetMapCn(apiLink);
        // apiLink = '/Api/WebServiceMapCn?Type=1&key=' + mapKey;
        // getMapCn(apiLink, mapEvent);
        //== 事件群組 未選主題先關起來 ==
        // GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + mapKey;
        // getGroupEventsCn(GroupApiLink);
        //== 所有主題 ==
        apiThemeLink = '/Api/WebServiceMapCn?Type=4&key=' + themeId;
        // apiThemeLink = '/WebServiceMapCn.aspx?Type=4&key=' + themeId;
    } else {
        //== 設置地圖 ==
        apiLink = '/Api/WebServiceMapEn?Type=1&key=' + mapKey;
        // apiLink = '/WebServiceMapEn.aspx?Type=1&key=' + mapKey;
        loadSetMapEn(apiLink);
        // apiLink = '/Api/WebServiceMapEn?Type=1&key=' + mapKey;
        // getMapEn(apiLink, mapEvent);
        //== 事件群組 未選主題先關起來 ==
        // GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + mapKey;
        // getGroupEventsEn(GroupApiLink);
        //== 所有主題 ==
        apiThemeLink = '/Api/WebServiceMapEn?Type=4&key=' + themeId;
        // apiThemeLink = '/WebServiceMapEn.aspx?Type=4&key=' + themeId;
    }

    //=== 主題選擇 ===
    // apiThemeLink = '/WebServiceMapCn.aspx?Type=4&key=1';
    // var apiThemeLink = '/WebServiceMapEn.aspx?Type=4&key=1';//英
    var storyNameBtn = 'storyNameBtn';
    var buildInfo = '<div class="error-page">' +
        '<div class="build-cont">' +
        '<div class="error-info">' +
        '<div class="error-txt build-txt">Sorry！網站建置中</div>' +
        '<div class="error-en">Coming Soon...</div>' +
        '<a class="btn" href="/">回到首頁 / Back Home</a>' +
        '</div>' +
        '</div>' +
        '</div>';
    //=== 置入路線主題 ===
    $.get(apiThemeLink, function (data) {
        var resData = JSON.parse(data);
        console.log(resData);
        var titCont = resData.filter(function (item) {
            return item.Wid == mapKey;
        });
        if (titCont[0].SubjectName == '') {
            console.log('顯示建置中');
            $('.page-wrap').html(buildInfo);
        } else {
            var noteTxt, sendTxt;
            lang == 'zh' ? noteTxt = '＊請勾選至多三項，將於下方地圖展示所有勾選項目之行跡分布' : noteTxt = '';
            $('#mapThemeBox').append('<div class="map-theme-note">' + noteTxt + '</div>');

            $.each(resData, function (ind, val) {
                var { Wid, SubjectName, Pic } = val;
                if (SubjectName !== '') {
                    $('#mapThemeBox').append('<div class="map-theme-box" id="' + storyNameBtn + Wid + '">' +
                        '<div class="img"><img src="' + Pic + '" alt="" /></div>' +
                        '<div class="name">' + SubjectName + '</div>' +
                        '</div>');
                }
            });

            lang == 'zh' ? sendTxt = '送出' : sendTxt = 'submit';
            $('#mapThemeBox').append('<div class="map-theme-btn" id="mapThemeBtn"><i class="fas fa-check-circle"></i> ' + sendTxt + '</div>');
            $('#storyNameBtn' + mapKey).addClass('act');
        }
    });
    //=== 最多選擇三筆 ===
    $(document).on('click', '.map-theme-box', function () {
        var actL = $('#mapThemeBox').find('.act').length;
        var hasAct = $(this).hasClass('act');
        if (actL == 3 && hasAct == false) {
            alert('最多選擇三個主題。');
        } else if (actL !== 3 && hasAct == false) {
            $(this).addClass('act');
        }
    });
    $(document).on('click', '.map-theme-box.act', function () {
        // var actL = $('#mapThemeBox').find('.act').length;
        $(this).removeClass('act');
    });
    //=== 送出選擇主題 ===
    $('#mapThemeBtn').on('click', function () {
        srcBtnClick = true;//為防止用全站搜尋結果讀取狀態
        var choseTheme = [];
        $('.map-theme-box.act').each(function (param) {
            var choseIdTxt = $(this).attr('id');
            var choseId = choseIdTxt.replace(storyNameBtn, '');
            choseTheme.push(choseId);
        });
        if (choseTheme.length) {
            var apiIdList = choseTheme.join(',');
            console.log(apiIdList);
            actMapKeyId = choseTheme[0];
            //=== 重置側邊文字欄先關起來 ===
            // $('#storyCover').show();
            // $('#storyCont').hide();
            $('.map-box').removeClass('map-open-box');
            // $('.map-box').removeAttr('style');
            map.resize();
            $('#storyContBoxBg').hide();

            $('#preBtn,#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
            //== 清除主題事件群組 ==
            $('.map-tag-box').hide();
            $('#mapEventSideArea').hide();
            if (lang == 'zh') {
                //== 設置新地圖 ==
                // apiLink = '/WebServiceMapCn.aspx?Type=1&key=' + apiIdList;
                apiLink = '/Api/WebServiceMapCn?Type=1&key=' + apiIdList;
                loadSetMapCn(apiLink);
                //== 新標籤群組 未選主題先關起來 ==
                // GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + actMapKeyId;
                // getGroupEventsCn(GroupApiLink);
            } else {
                //== 設置新地圖 ==
                // apiLink = '/WebServiceMapEn.aspx?Type=1&key=' + apiIdList;
                apiLink = '/Api/WebServiceMapEn?Type=1&key=' + apiIdList;
                loadSetMapEn(apiLink);
                //== 新標籤群組 未選主題先關起來 ==
                // GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + actMapKeyId;
                // getGroupEventsEn(GroupApiLink);
            }
        } else {
            if (lang == 'zh') {
                alert('請至少選擇一個主題。');
            } else {
                alert('Please Chose One Theme.');
            }
        }
        //== 清除如果有打開的pop ==
        //= 需要清除pop他是新增上去並不是取代 所以要刪除只顯示當前的 =
        var poplgL = $('.mapboxgl-popup').length;
        if (poplgL != 0) {
            $('.mapboxgl-popup').remove();
        }
    });
    //=== 第一筆資料開合 ===
    $('#storyCoverBtn').on('click', function () {
        var isShow = $('#storyCover').is(':visible');
        isShow ? $('#storyCover').slideUp() : $('#storyCover').slideDown();
    });
    //=== 重置按鈕 ===
    // $('#storyCont').hide();
    //== 事件群組先關起來 ==
    $('.map-tag-box').hide();
    $('#mapEventSideArea').hide();
    $('#storyContBox').mCustomScrollbar();//側邊內容滾輪
    //== 將上下則先鎖起來不可以點 ==
    $('#preBtn,#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
    //=== 選擇第一筆資料前往 ===
    $(document).on('click', '.them-btn', function () {//.story-start-btn
        // var btnId = $(this).attr('id').replace('storyStartBtn', '');
        var btnId = $(this).attr('id').replace('themBtn', '');
        var mapKeyId = $(this).data('mapkey');
        actThem = 'routeLife' + btnId;
        actNum = 0;//回到第一筆
        //== 放入當前主題 改變地圖側邊文字出現 ==
        // $('#them span').html(actThem + ':' + defaultThemeName);
        // $('#them span').html(defaultThemeName);
        // $('#storyCont').show();
        //== 開啟側邊欄 ==
        var $actBtn = $(this);
        openSideTxtCont($actBtn);
        //== 選定主題設置此主題群組事件 ==
        //== 事件群組 ==
        // var eventAreaShow = $('.map-tag-box').is(':visible');
        var eventAreaShow = $('#mapEventSideArea').is(':visible');
        //== 新標籤群組 ==(如果當前不同時在更新)
        if (actMapKeyId != mapKeyId) {
            actMapKeyId = mapKeyId;
            changeEventGroup(actMapKeyId);
        } else if (eventAreaShow == false) {
            changeEventGroup(actMapKeyId);
        }
        //=== 前往選擇資料 ===
        var goMapRes = map.getSource(actThem);
        console.log('=== 前往選擇資料 ===');
        console.log(goMapRes);
        setTxtData(goMapRes);
        defaultThemeName = goMapRes._data.themeName;
        //== 如果當前主題事件只有一筆的話，上下則都不能按 ==
        $('#preBtn,#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
        var pointL = goMapRes._data.features.length;
        if (pointL > 1) {
            $('#nextBtn').removeAttr('style');
        }
    });
    //=== 上下則 ===
    // $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
    $('#preBtn').on('click', function () {
        //== 固定不要移動 ==
        // bgFix();
        // var testMapRes = map.getSource('routeLife1');
        var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
        var pointL = testMapRes._data.features.length;
        console.log(pointL);
        actNum = actNum - 1;
        if (actNum == 0) {
            $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
            if (pointL != 1) {
                $('#nextBtn').removeAttr('style');
            }
        } else {
            $('#nextBtn').removeAttr('style');
        }
        //== 設置詳細資訊 ==
        setTxtData(testMapRes);
    });
    $('#nextBtn').on('click', function () {
        //== 固定不要移動 ==
        // bgFix();
        // var testMapRes = map.getSource('routeLife1');
        var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
        var pointL = testMapRes._data.features.length;
        console.log(testMapRes._data);
        console.log(pointL);
        actNum = actNum + 1;
        if (actNum == pointL - 1) {
            $('#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
            $('#preBtn').removeAttr('style');
        } else {
            if (actNum !== 0) {
                $('#preBtn').removeAttr('style');
            }
        }
        //== 設置詳細資訊 ==
        setTxtData(testMapRes);
    });

    //== ＃號標籤收尋 ==
    $(document).on('click', '.tagBtn', function () {
        var tagId = $(this).data('tagid');
        tagSrcPage(tagId);
    });


    //=== 下方標籤開闔 － 舊的 ===
    var $moreMapTagBtn = $('#moreMapTagBtn');
    var $MapTagBox = $('#allMapTagBox');

    var tagOpen = false;
    $moreMapTagBtn.on('click', function () {
        var tabBtnTxt;
        if (tagOpen) {
            $MapTagBox.removeClass('open');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '展開' : tabBtnTxt = 'More';
            $moreMapTagBtn.find('span').html(tabBtnTxt);

            $('.map-all-tag').mCustomScrollbar("destroy");
            tagOpen = false;
        } else {
            $MapTagBox.addClass('open');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '收合' : tabBtnTxt = 'Close';
            $moreMapTagBtn.find('span').html(tabBtnTxt);

            $('.map-all-tag').mCustomScrollbar();
            tagOpen = true;
        }
    });

    $(document).on('click', '.tag', function () {
        var indEvent = $(this).data('tagid');
        var $eventTagBox = $('#eventsTag' + indEvent);
        var eventTagBoxShow = $eventTagBox.is(':visible');
        if (eventTagBoxShow) {
            $('.tag').removeClass('act');
            $('.map-event').hide();
            $('#mapEventBox').hide();
        } else {
            $('.tag').removeClass('act');
            $(this).addClass('act');
            if ($eventTagBox.length) {
                $('.map-event').hide();
                $('#mapEventBox').show();
                $eventTagBox.show();
            } else {
                $('.map-event').hide();
                $('#mapEventBox').hide();
            }
        }
    });
    //=== 下方標籤開闔 － 舊的 END ===

    //== 事件標籤 飛飛 ==
    $(document).on('click', '.t-even', function () {
        //== 固定不要移動 ==
        // bgFix();
        var eventIdAct = $(this).data('eventid');
        eventGoTo(eventIdAct);
    });

    //== 先關起來 ==
    // $('.them-btn-area,#mapEventSideArea').hide();
    //== 新事件點擊開關 新 ==
    $(document).on('click', '.map-event-side-name', function () {
        var $list = $(this).next('ul');
        var listOpen = $list.is(':visible');
        if (listOpen) {
            $list.slideUp();
            $(this).removeClass('act');
        } else {
            $list.slideDown();
            $(this).addClass('act');
        }
    });

    // $('footer').remove();
});
//=== 標籤群組事件前往 ===
function eventGoTo(eventId) {
    var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
    var dataListL = testMapRes._data.features.length;
    var eventInd = testMapRes._data.features.findIndex(function (item) {
        return item.properties.eventId == eventId;
    });
    actNum = eventInd;
    //== 如果還沒按開始，第一個'點選群組事件標籤時'和'用全站搜尋來的'要打開內容 ==
    //== 有多路線點選事件的時後，所以要找當前主題下 ==
    var goMapRes = map.getSource(actThem);
    var actThemInd = goMapRes.id.replace('routeLife', '');
    var $actBtn = $('#themBtn' + actThemInd);
    // var $actBtn = $('#themBtn1');
    openSideTxtCont($actBtn);
    //== 給上下則按鈕狀態更新 ==
    actNum == 0 ? $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#preBtn').removeAttr('style');
    actNum == dataListL - 1 ? $('#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#nextBtn').removeAttr('style');
    setTxtData(testMapRes);
    //== 如果還沒按開始，第一個點選標籤時要打開內容 ==
    // $('#storyCont').show();
    console.log(actNum);
}

//=== 讀取設置地圖 ===
function loadSetMapCn(apiLink) {
    var mapDataList = [];
    $.get(apiLink, function (data) {
        var resData = JSON.parse(data);
        console.log(resData);
        //== 無資料返回首頁 ==
        noDataToHome(resData);
        $.each(resData, function (ind, val) {
            // var pointDataListTxt = val.data;
            var { key, keyName, data } = val;
            var pointDataList = JSON.parse(data);
            console.log(keyName)
            console.log(pointDataList);
            //
            var mapReData = [];//路徑和點主要資料
            var mapRepath = [];//路線線條資料
            $.each(pointDataList, function (ind, val) {
                var { Eventid, Name_tw, Content_tw, longitude, latitude, pic_tw, med_tw, data, EventgidArray } = val;
                //= # 標籤 =
                var tagListData = JSON.parse(data.TagidArray);
                var tagList = [];
                $.each(tagListData, function (ind, val) {
                    var { Tagid, Name_tw } = val;
                    tagList.push({ 'tagId': Tagid, 'tagName': Name_tw });
                });
                //= 事件群組標籤 =
                var eventGroup = JSON.parse(EventgidArray);
                mapReData.push({
                    'type': 'Feature',
                    'properties': { 'eventId': Eventid, 'title': Name_tw, 'description': Content_tw, 'pic': pic_tw, 'med': med_tw, 'tagList': tagList, 'eventgidList': eventGroup },
                    'geometry': { 'type': 'Point', 'coordinates': [Number(longitude), Number(latitude)] }
                });
                mapRepath.push([Number(longitude), Number(latitude)]);
            });
            // console.log('=== 喵喵SEE ===');
            // console.log(mapReData);
            // console.log(mapRepath);
            mapDataList.push({ 'mapKey': key, 'mapName': keyName, 'mapData': mapReData, 'pathData': mapRepath });
            //== 帶入第一個主題名稱 ==
            // actThem = 'routeLife1';
            // defaultThemeName = mapDataList[0].mapName;
        });
    });
    console.log(mapDataList);
    //=== 開始讀取地圖 ===
    mapStart(mapDataList);
    //=== 增加開始隱藏按鈕 ===
    // $('#menu').html('');
    // $('#storyCover').html('');
    $('#themBtnBox').html('');
    startColorBtn = [];
    $.each(mapDataList, function (ind, val) {
        console.log(ind);
        var num = ind + 1;
        var routeId = 'route' + num;
        var markId = 'routeMark' + num;
        var { mapKey, mapName } = val;

        //=== 增加隱藏按鈕 ===
        // var link = document.createElement('a');
        // link.id = routeId;
        // //toggMarkLayerIds : link.textContent = '隱藏' + routeId;
        // link.textContent = mapName;//按鈕名稱
        // link.markContent = markId;
        // link.className = 'active';

        // link.style.background = pathColorList[ind];

        // $('#menu').append(link);

        // link.onclick = function (e) {
        //     var clickedLayer = this.id;//一個關路徑
        //     var markLayer = this.markContent;//一個關標記mark
        //     console.log(this.textContent);
        //     console.log(this.markContent);
        //     console.log('111', markLayer);
        //     console.log('clickedLayer', clickedLayer);
        //     console.log('markLayer', markLayer);
        //     e.preventDefault();//js停止事件的默認行為，例如a的href，會阻擋觸發
        //     e.stopPropagation();//js中阻止冒泡事件
        //     var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

        //     // Toggle layer visibility by changing the layout object's visibility property.
        //     if (visibility === 'visible') {
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'none');
        //         map.setLayoutProperty(markLayer, 'visibility', 'none');
        //         this.className = '';
        //         console.log('markLayer點點')
        //     } else {
        //         this.className = 'active';
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
        //         map.setLayoutProperty(markLayer, 'visibility', 'visible');
        //     }
        // };
        //== 加入開始按鈕 ==
        // $('#storyCover').append('<div class="story-start-btn story-start-btn' + (ind + 1) + '" data-mapkey="' + mapKey + '" id="storyStartBtn' + (ind + 1) + '">開始《' + mapName + '》</div>');
        $('#themBtnBox').append('<div class="them-btn" data-mapkey="' + mapKey + '" id="themBtn' + (ind + 1) + '">' + mapName + '</div>');
        startColorBtn.push({ 'btnClass': 'story-start-btn' + (ind + 1), 'color': pathColorList[ind], 'legendName': mapName });
    });
    //== 增加切換主題備註 ==
    $('#themBtnBox').append('<span>*可切換主題查看事紀行跡</span>');
    //== 增加圖例 ==
    addMapLegend(startColorBtn);
}
function loadSetMapEn(apiLink) {
    var mapDataList = [];
    $.get(apiLink, function (data) {
        var resData = JSON.parse(data);
        console.log(resData);
        //== 無資料返回首頁 ==
        noDataToHome(resData);
        $.each(resData, function (ind, val) {
            // var pointDataListTxt = val.data;
            var { key, keyName, data } = val;
            var pointDataList = JSON.parse(data);
            console.log(keyName)
            console.log(pointDataList);
            //
            var mapReData = [];//路徑和點主要資料
            var mapRepath = [];//路線線條資料
            $.each(pointDataList, function (ind, val) {
                var { Eventid, Name_en, Content_en, longitude, latitude, pic_en, med_en, data, EventgidArray } = val;
                //= # 標籤 =
                var tagListData = JSON.parse(data.TagidArray);
                var tagList = [];
                $.each(tagListData, function (ind, val) {
                    var { Tagid, Name_en } = val;
                    tagList.push({ 'tagId': Tagid, 'tagName': Name_en });
                });
                //= 事件群組標籤 =
                var eventGroup = JSON.parse(EventgidArray);
                mapReData.push({
                    'type': 'Feature',
                    'properties': { 'eventId': Eventid, 'title': Name_en, 'description': Content_en, 'pic': pic_en, 'med': med_en, 'tagList': tagList, 'eventgidList': eventGroup },
                    'geometry': { 'type': 'Point', 'coordinates': [Number(longitude), Number(latitude)] }
                });
                mapRepath.push([Number(longitude), Number(latitude)]);
            });
            // console.log('=== 喵喵SEE ===');
            // console.log(mapReData);
            // console.log(mapRepath);
            mapDataList.push({ 'mapKey': key, 'mapName': keyName, 'mapData': mapReData, 'pathData': mapRepath });
            //== 帶入第一個主題名稱 ==
            // defaultThemeName = mapDataList[0].mapName;
        });
    });
    console.log(mapDataList);
    //=== 開始讀取地圖 ===
    mapStart(mapDataList);
    //=== 增加開始隱藏按鈕 ===
    // $('#menu').html('');
    // $('#storyCover').html('');
    $('#themBtnBox').html('');
    startColorBtn = [];
    $.each(mapDataList, function (ind, val) {
        console.log(ind);
        var num = ind + 1;
        var routeId = 'route' + num;
        var markId = 'routeMark' + num;
        var { mapKey, mapName } = val;

        // var link = document.createElement('a');
        // link.id = routeId;
        // //toggMarkLayerIds : link.textContent = '隱藏' + routeId;
        // link.textContent = mapName;//按鈕名稱
        // link.markContent = markId;
        // link.className = 'active';

        // link.style.background = pathColorList[ind];

        // $('#menu').append(link);

        // link.onclick = function (e) {
        //     var clickedLayer = this.id;//一個關路徑
        //     var markLayer = this.markContent;//一個關標記mark
        //     console.log(this.textContent);
        //     console.log(this.markContent);
        //     console.log('111', markLayer);
        //     console.log('clickedLayer', clickedLayer);
        //     console.log('markLayer', markLayer);
        //     e.preventDefault();//js停止事件的默認行為，例如a的href，會阻擋觸發
        //     e.stopPropagation();//js中阻止冒泡事件
        //     var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

        //     // Toggle layer visibility by changing the layout object's visibility property.
        //     if (visibility === 'visible') {
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'none');
        //         map.setLayoutProperty(markLayer, 'visibility', 'none');
        //         this.className = '';
        //         console.log('markLayer點點')
        //     } else {
        //         this.className = 'active';
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
        //         map.setLayoutProperty(markLayer, 'visibility', 'visible');
        //     }
        // };
        //== 加入開始按鈕 ==
        // $('#storyCover').append('<div class="story-start-btn story-start-btn' + (ind + 1) + '" data-mapkey="' + mapKey + '" id="storyStartBtn' + (ind + 1) + '">START《' + mapName + '》</div>');
        $('#themBtnBox').append('<div class="them-btn" data-mapkey="' + mapKey + '" id="themBtn' + (ind + 1) + '">' + mapName + '</div>');
        startColorBtn.push({ 'btnClass': 'story-start-btn' + (ind + 1), 'color': pathColorList[ind], 'legendName': mapName });
    });
    //== 增加切換主題備註 ==
    $('#themBtnBox').append('<span>You can click different topic buttons below to check.</span>');
    //== 增加圖例 ==
    addMapLegend(startColorBtn);
}
//=== 如果沒有資料導回首頁 ===
function noDataToHome(data) {
    var actLang = CookiesLang;
    // var actLang = getCookie('CacheLang');
    var nameTxt = data[0].keyName;
    if (nameTxt == '') {
        window.location.href = "/" + actLang + "/Home/PageNotFound";
    }
}
//=== 增加地圖圖例 ===
function addMapLegend(list) {
    var mapLegendAreaL = $('#mapLegendArea').length;
    if (mapLegendAreaL == 0) {
        $('.map-box').append('<div class="map-legend-area" id="mapLegendArea"></div>');
    }
    $('#mapLegendArea').html('');
    $.each(list, function (ind, val) {
        //= 開始按鈕顏色 =
        //var $statrBtn = $('.' + val.btnClass);
        //$statrBtn.css({ 'background': val.color });
        //= 增加圖例 =
        $('#mapLegendArea').append('<div class="map-legend-box map-legend-box' + (ind + 1) + '"><span class="legend-circle"></span><span class="legend-name">' + val.legendName + '</span></span></div>');
        var $legendCircle = $('.map-legend-box' + (ind + 1)).find('.legend-circle');
        $legendCircle.css({ 'background': val.color });
    });
}
//=== 地圖設置 ===
function mapStart(mapDataList) {
    map = new mapboxgl.Map({//初始化
        container: 'map',
        // text-field: 'system-ui',
        // style: 'mapbox://styles/mapbox/streets-v11', //自訂樣式藍色系
        style: 'mapbox://styles/truesun/ckq76o3dp0g8f17rzdwwp5rcp',
        // style: 'mapbox://styles/mapbox/light-v10',
        //藍色系：mapbox://styles/truesun/ckq76o3dp0g8f17rzdwwp5rcp
        // 一般樣式 style: 'mapbox://styles/mapbox/streets-v11',
        //- 預設樣式： mapbox://styles/mapbox/streets-v11
        //- 暗黑模式：mapbox://styles/mapbox/dark-v10
        center: [120.587043762207, 23.9626407623291],//預設地圖中心點
        zoom: 6, // 8 預設地圖縮放比例，可以到小數點兩位
        maxZoom: 25,//defalut 15
        pitch: 0, //可以用俯視角度看地圖（上下角度）
        bearing: 0, // 可以用俯視角度看地圖（左右角度）,
        localIdeographFontFamily: " system-ui,'Noto Sans', 'Noto Sans CJK SC', sans-serif "//可以設定字體，但是應用了自訂的map style所以會被設為false
    });

    map.on('load', function () {
        console.log('123');
        //=== 路徑圖片設置 ===
        // https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png
        // map.loadImage('https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',//img/icon_location.png
        // 	// Add an image to use as a custom marker
        // 	function (error, image) {
        // 		if (error) throw error;
        // 		map.addImage('custom-marker', image);
        // 		$.each(mapDataList, function (ind, val) {
        // 			var idNum = ind + 1;
        // 			var routeId = 'routeLife' + idNum;//總資料
        // 			var markId = 'routeMark' + idNum;//總點名稱ID
        // 			var { mapName, mapData } = val;
        // 			//routeLife1,'大事記',路徑資料,routeMark1
        // 			setMainRouteData(routeId, mapName, mapData, markId);
        // 		});
        // 	});
        //=== 加路徑線條 資料 ===
        $.each(mapDataList, function (ind, val) {
            var idNum = ind + 1;
            var routeLifeId = 'routeLife' + idNum;//總資料
            var markId = 'routeMark' + idNum;//總點名稱ID
            var { mapKey, mapName, mapData } = val;

            var routePathId = 'routePath' + idNum;//路線資料定義
            var routeId = 'route' + idNum;//總路線名稱ID
            var { pathData } = val;
            //routePath1,路徑資料,route1
            setMainRoutePathData(routePathId, pathData, routeId, pathColorList[ind]);
            //routeLife1,主題ID,主題名稱,主題主資料,mark點ID(路徑和mark點設置)
            setMainRouteData2(routeLifeId, mapKey, mapName, mapData, markId, pathColorList[ind]);
        });
        //=== 前往搜尋近來得事件 ===
        var mapEvent = getLinkVal('Eventid');
        if (mapEvent && srcBtnClick == false) {//點過搜尋就不會啟動網址初始情況
            //== 設置群組事件 ==
            changeEventGroup(actMapKeyId);
            //== 需要開啟同步事件 所以要先跑事件 ==
            eventGoTo(mapEvent);
        }
    });

    //=== 增加一些地圖功能 ===
    // Center the map on the coordinates of any clicked symbol from the 'symbols' layer.
    /**** 滑鼠移過出現popup  ****/
    // Create a popup, but don't add it to the map yet.
    popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });
    //=== 點擊座標居中 ===
    //routeMark1對應的是lyaer id
    $.each(mapDataList, function (ind, val) {
        var idNum = ind + 1;
        var markId = 'routeMark' + idNum;//總點名稱ID
        //routeMark1
        setMakerFun(markId);
    });

    //=== 增加控制地圖縮放 ===
    map.addControl(new mapboxgl.NavigationControl());
    //=== 地圖設置語系 (預設全地圖語系)===
    var mapboxLanguage = new MapboxLanguage({
        defaultLanguage: mapLanguage //'en'
    });
    map.addControl(mapboxLanguage);
    //=== 地圖飛完後 因為會有太遠好像會上下跳來跳去 所以先鎖定畫面windows後再解開===
    map.on('moveend', function (e) {
        //== 解開固定 ==
        // bgUnFix();
        // if (flying) {
        //     var tooltip = new mapboxgl.Popup()
        //         .setLngLat(map.getCenter())
        //         .setHTML('<h1>Hello World!</h1>')
        //         .addTo(map);
        //     map.fire('flyend');
        // }
    });
}
//=== 地圖設置語系(無,這是跑員資料後替換標籤內的語系) ===
// function setMapLang(lang) {
//     map.setLayoutProperty('country-label', 'text-field', [
//         'get',
//         `name_${lang}`
//     ]);
// }
//=== 設置主要路線資料和打點 ===
//routeLife1,1,葉石濤大事紀,sourceData(mapDataList.mapData),routeMark1(img打點)
function setMainRouteData2(sourceId, mapThemeKey, themeName, sourceData, layerMarkId, circleColor) {
    //== 路徑(點)總資料 ==
    map.addSource(sourceId, {//routeLife1
        'type': 'geojson',
        'data': {
            'type': 'FeatureCollection',
            'themeId': mapThemeKey,
            'themeName': themeName,//葉石濤大事紀
            'features': sourceData
        }
    });
    //== 打點資料 ==
    map.addLayer({
        'id': layerMarkId,
        'type': 'circle',
        'source': sourceId,//點資料來源
        'paint': {
            'circle-color': circleColor,//#4264fb
            'circle-radius': 6,//圈圈大小
            'circle-stroke-width': 2,
            'circle-stroke-color': '#ffffff'
        }
    });
}
//=== 路線路徑資料與設定 ===
//routePath1,pathSourceData(mapDataList.pathData),route1,#CB5050
function setMainRoutePathData(pathSourceId, pathSourceData, layerRouteId, pathColor) {
    //== 路徑(路線)資料 ==
    map.addSource(pathSourceId, {// 路徑資料ID routePath1
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': pathSourceData//路線線條array
            }
        }
    });
    //== 路徑設定 ==
    map.addLayer({
        'id': layerRouteId,//路徑線條群組設定ID route1
        'type': 'line',
        'source': pathSourceId,//路徑資料來源
        'layout': {
            'line-join': 'round',
            'line-cap': 'round',
            'visibility': 'visible'
        },
        'paint': {
            'line-color': pathColor,//#CB5050 or "rgba(255, 255, 0, 1)"
            'line-width': 2,
            'line-opacity': 0.5,
            // 'line-dasharray': [1, 2],//可以做虛線 [線段,線距]
        }
    });
}
//=== 座標點點擊設定 ===
//routeMark1
function setMakerFun(layerMarkId) {
    //=== 點擊座標居中 ===
    map.on('click', layerMarkId, function (e) { //要aabd是layer的id
        console.log('=== wwwwww ===');
        console.log(e.features[0]);
        //== 帶入當前資料並打開側邊文字欄，要在飛行前打開，位置才會在中間 ==
        var actThemInd = e.features[0].source.replace('routeLife', '');
        var $actBtn = $('#themBtn' + actThemInd);
        openSideTxtCont($actBtn);

        map.flyTo({
            center: e.features[0].geometry.coordinates, //取得物件的座標
            //- center: [ //這邊是墾丁的座標
            //- 	120.74 + (Math.random() - 0.5) * 10,
            //- 	21.99 + (Math.random() - 0.5) * 10
            //- ],
            speed: 0.5,//移動過去的速度 數字越大越快
            essential: true // 啟用物理動畫
            //這裡不zoom因為pop是出現在點擊的點上，放大會歪掉
        });
        //== 點擊後設置資料 ==
        markPointSet(e);
    });
    //== hover popup ==
    // map.on('mouseenter', layerMarkId, function (e) {//places要對應 map.addSource的參數
    //     // Change the cursor style as a UI indicator.
    //     map.getCanvas().style.cursor = 'pointer';

    //     var coordinates = e.features[0].geometry.coordinates.slice();
    //     var description = e.features[0].properties.description;

    //     // Ensure that if the map is zoomed out such that multiple
    //     // copies of the feature are visible, the popup appears
    //     // over the copy being pointed to.
    //     while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
    //         coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    //     }

    //     // Populate the popup and set its coordinates
    //     // based on the feature found.
    //     popup.setLngLat(coordinates).setHTML(description).addTo(map);
    // });

    // map.on('mouseleave', layerMarkId, function (e) { //places要對應 map.addSource的參數
    //     map.getCanvas().style.cursor = '';
    //     popup.remove();
    // });
    //== click popup ==
    map.on('click', layerMarkId, function (e) {//'routeMark2'
        var coordinates = e.features[0].geometry.coordinates.slice();
        var description = e.features[0].properties.description;
        // var pic = e.features[0].properties.pic;
        var tit = e.features[0].properties.title;
        //== popup加圖片 ==
        var popupCont = popImgDom(description, tit);

        //- Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        //- over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML(popupCont)//description
            .addTo(map);
    });
    //- Change the cursor to a pointer when the mouse is over the places layer.
    map.on('mouseenter', layerMarkId, function () {//'routeMark2'
        map.getCanvas().style.cursor = 'pointer';
    });
    //- Change it back to a pointer when it leaves.
    map.on('mouseleave', layerMarkId, function () {//'routeMark2'
        map.getCanvas().style.cursor = '';
    });
}
//=== 上下則資料帶入 ===
function setTxtData(testMapRes) {
    //== 加入資料顯示動畫 ==
    changeDataAni();

    //== 資料顯示內容 ==(點擊上下則不會改變主要主題);
    var { title, description, pic, med, tagList } = testMapRes._data.features[actNum].properties;
    // var title = testMapRes._data.features[actNum].properties.title;
    // var cont = testMapRes._data.features[actNum].properties.description;
    $('#themTit').html(title);
    $('#themCont').html(description);
    pic ? $('#themeImg').show() : $('#themeImg').hide();
    $('.story-theme-img img').attr('src', pic);
    $('.story-theme-img-note').html(med);
    // var tagList = testMapRes._data.features[actNum].properties.tagList;
    // console.log(tagList);
    var tagListL = tagList.length;
    if (tagListL != 0) {
        $('#themTag').html('');
        $.each(tagList, function (ind, val) {
            $('#themTag').append('<div data-tagid="' + val.tagId + '" class="tagBtn">#' + val.tagName + '</div>');
        });
    } else {
        $('#themTag').html('');
    }
    //== 前往位置 ==
    var goPos = testMapRes._data.features[actNum].geometry.coordinates;
    // console.log(goPos);

    mapFlyTo(goPos, testMapRes);
    console.log(actNum);
    //== 開啟pop ==
    // = 發現因為pop如果在移動先跳出 不知道為什麼overflow:hidden沒有擋住 所以改在飛行結束後跳pop =
    // var popPoint = testMapRes._data.features[actNum];
    // movePopup(popPoint);
    //== 連動事件群組 ==
    setEventSideTagAct(actNum);
}
//===  地圖 - 飛向位置 ===
function mapFlyTo(target, testMapRes) {
    $('.mapboxgl-popup').remove();
    // bgFix();
    map.flyTo({
        center: target,
        essential: true, // this animation is considered essential with respect to prefers-reduced-motion
        speed: 1.5,
        curve: 2,
        zoom: 15//15
    });

    //當飛行完全結束後，才跳出pop
    map.once('idle', function () {
        // console.log('= 我已經冷靜下來了 =');
        var popPoint = testMapRes._data.features[actNum];
        movePopup(popPoint);
        // 地圖已經靜止，可以顯示 popup
        // popup.addTo(map);
    });

    // map.on('moveend', function (e) {
    //     bgUnFix();
    // });
}
//=== Mark點擊設置資料 ===
//== 因為setTxtData需要 flyto，也不會改變當前主題，也可能跳點而更新資料事件順序，所以分開寫 ==
function markPointSet(point) {
    console.log(point.features[0].source);
    console.log('= 點擊Id =');
    console.log(point.features[0].properties.eventId);
    //== 加入資料顯示動畫 ==
    changeDataAni();

    //== 帶入點擊point的當前主題 ==
    actThem = point.features[0].source;
    console.log('actThem', actThem);
    var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
    // var themeName = testMapRes._data.themeName;
    var { themeId, themeName } = testMapRes._data;
    //== 帶入當前主題狀態 ==
    // $('#them span').html(themeName);
    // $('#them span').html(actThem + ':' + themeName);
    //== 帶入點擊point的當前點資料 ==
    // $('#storyCont').show();
    var { title, description, pic, med, tagList } = point.features[0].properties;
    $('#themTit').html(title);
    $('#themCont').html(description);
    pic ? $('#themeImg').show() : $('#themeImg').hide();
    $('.story-theme-img img').attr('src', pic);
    $('.story-theme-img-note').html(med);
    var tagList = JSON.parse(tagList);
    var tagListL = tagList.length;
    if (tagListL != 0) {
        $('#themTag').html('');
        $.each(tagList, function (ind, val) {
            $('#themTag').append('<div data-tagid="' + val.tagId + '" class="tagBtn">#' + val.tagName + '</div>');
        });
    } else {
        $('#themTag').html('');
    }
    //== 新標籤群組 ==(如果當前不同時在更新)
    //== 事件群組 ==
    // var eventAreaShow = $('.map-tag-box').is(':visible');
    var eventAreaShow = $('#mapEventSideArea').is(':visible');
    if (actMapKeyId != themeId) {
        actMapKeyId = themeId;
        changeEventGroup(actMapKeyId);
    } else if (eventAreaShow == false) {
        //= 如果第一次沒有出現時設置 =
        changeEventGroup(actMapKeyId);
    }
    //== 更新當前的mark為第幾筆 ==
    var actPoint = point.features[0].properties.eventId;
    var dataList = testMapRes._data.features;
    var dataListL = testMapRes._data.features.length;
    console.log(dataList);
    var indNum = dataList.findIndex(function (item) {
        return item.properties.eventId == actPoint; // 0
    });
    console.log('= 點擊Id在第幾筆 =');
    console.log(indNum);
    actNum = indNum;
    console.log('actNum', actNum);
    //== 給上下則按鈕狀態更新 ==
    actNum == 0 ? $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#preBtn').removeAttr('style');
    actNum == dataListL - 1 ? $('#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#nextBtn').removeAttr('style');
    //== popup跳出(如果沒有再出現)==
    var popL = $('.mapboxgl-popup-content').length;
    if (popL == 0) {
        $('.mapboxgl-popup-content').remove();
        movePopup(dataList[actNum]);
    }
    //== 連動事件群組 ==
    setEventSideTagAct(actNum);
}
//=== 移動到當前點時顯示pop資料 move popup ===
function movePopup(popupData) {
    var coordinates = popupData.geometry.coordinates.slice();
    var description = popupData.properties.description;
    // var pic = popupData.properties.pic;
    var tit = popupData.properties.title;
    console.log(popupData);
    //== popup加圖片 ==
    var popupCont = popImgDom(description, tit);

    //= 需要清除pop他是新增上去並不是取代 所以要刪除只顯示當前的 =
    $('.mapboxgl-popup').remove();
    //- Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    //- over the copy being pointed to.
    //= 點擊範圍計算，這裡不需要，因為是直接出現這個點pop 不會有點擊範圍問題 =
    // while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
    //     coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    // }

    new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(popupCont)//description
        .addTo(map);
}
//== popup加圖片 ==
function popImgDom(description, tit) {
    var popupCont;
    if (tit == '') {
        popupCont = description;
    } else {
        // var imgDom = '<img src=' + pic + ' class="popupimg">';
        var titDom = '<p class="popuptit">' + tit + '</p>';
        // popupCont = titDom + description;
        popupCont = '<p class="popuptit">' + tit + '</p>';
    }
    return popupCont;
}
//=== 打開側邊文字欄 ===
function openSideTxtCont($actBtn) {
    $('.them-btn').removeClass('act');
    $actBtn.addClass('act');
    $('.map-box').addClass('map-open-box');
    // $('.map-box').css({ 'width': '70%' });
    map.resize();
    $('#storyContBoxBg').show();
}

//=== 側邊資料顯示動畫 ===
function changeDataAni() {
    var element = document.querySelector('.story-cont');
    // element.classList.add('aaa', 'bbb');
    element.classList.add('change-data');
    element.addEventListener('animationend', () => {
        // do something
        $('.story-cont').removeClass('change-data');
    });
}


//=== 地圖事件群組 - 舊 ===
function getGroupEventsCn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        var tagData = JSON.parse(data);
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $('#mapEventBox').hide();
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_tw + '</div>');
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_tw + '</div>');
                });
            }
        });
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').hide();
        } else {
            $('.map-tag-box').show();
            $('.map-event').mCustomScrollbar();
        }
        tagBtnMoreShow();
    });
}

function getGroupEventsEn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        console.log(JSON.parse(data));
        var tagData = JSON.parse(data);
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $('#mapEventBox').hide();
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_en + '</div>');
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_en + '</div>');
                });
            }
        });
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').hide();
        } else {
            $('.map-tag-box').show();
            $('.map-event').mCustomScrollbar();
        }
        tagBtnMoreShow();
    });
}

function tagBtnMoreShow() {
    var tagsH = $('#allMapTagInnerBox').outerHeight();
    if (tagsH < 45) {
        $('#moreMapTagBtn').hide();
    } else {
        $('#moreMapTagBtn').show();
    }
}
//=== 地圖事件群組 - 舊 END ===

//=== 新 - 地圖事件群組 ===
function getGroupSideEventsCn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        var tagData = JSON.parse(data);
        // $('.map-event-side-box').html('');
        //== 為了解決切換時的滾軸問題 ==
        $('.map-event-side-box').remove();
        $('#mapEventSideArea').append('<div class="map-event-side-box"></div>');
        // $('.map-event-side-box').mCustomScrollbar("destroy");

        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('.map-event-side-box').append('<div class="map-event-side-name" id="eventsTagName' + val.eventgid + '"><span>' + val.title_tw + '</span><i class="far fa-chevron-down"></i></div>');
                $('.map-event-side-box').append('<ul class="map-event-side-list" id="eventsTagList' + val.eventgid + '"></ul>');
                var $eventsBox = $('#eventsTagList' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<li class="t-even" data-eventid="' + val.eventid + '">' + val.Name_tw + '</li>');
                });
            }
        });

        var groupEventL = $('.map-event-side-box').find('li').length;
        if (!groupEventL) {
            $('#mapEventSideArea').hide();
        } else {
            $('#mapEventSideArea').show();
            $('.map-event-side-box').mCustomScrollbar();
        }
    });
}

function getGroupSideEventsEn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        var tagData = JSON.parse(data);
        // $('.map-event-side-box').html('');
        //== 為了解決切換時的滾軸問題 ==
        $('.map-event-side-box').remove();
        $('#mapEventSideArea').append('<div class="map-event-side-box"></div>');

        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('.map-event-side-box').append('<div class="map-event-side-name" id="eventsTagName' + val.eventgid + '"><span>' + val.title_en + '</span><i class="far fa-chevron-down"></i></div>');
                $('.map-event-side-box').append('<ul class="map-event-side-list" id="eventsTagList' + val.eventgid + '"></ul>');
                var $eventsBox = $('#eventsTagList' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<li class="t-even" data-eventid="' + val.eventid + '">' + val.Name_en + '</li>');
                });
            }
        });

        var groupEventL = $('.map-event-side-box').find('li').length;
        if (!groupEventL) {
            $('#mapEventSideArea').hide();
        } else {
            $('#mapEventSideArea').show();
            $('.map-event-side-box').mCustomScrollbar();
        }
    });
}
//=== 移動當下事件連動事件群組 ===
//== actNum 總資料當前 第幾筆 事件 ==
function setEventSideTagAct(actNum) {
    var goMapRes = map.getSource(actThem);
    var actEvent = goMapRes._data.features[actNum];
    var { eventId, eventgidList } = actEvent.properties;
    console.log('=== 連動起來的資料 ===');
    console.log('事件', eventId);
    console.log('事件群組', eventgidList);
    var eventSideTagBoxShow = $('#mapEventSideArea').is(':visible');
    if (eventSideTagBoxShow == true) {
        $('.map-event-side-name,.t-even').removeClass('act');
        $('.map-event-side-list').hide();
    }
    if (eventgidList.length > 0 && eventSideTagBoxShow == true) {
        //== 擁有的事件如果是打開的就不動 把其他的關起來 ==
        $.each(eventgidList, function (ind, val) {
            $('#eventsTagName' + val).addClass('act');
            $('#eventsTagList' + val).show();
            $('#eventsTagList' + val).find('li[data-eventid="' + eventId + '"]').addClass('act');
        });
    }
}
//=== 新 - 地圖事件群組 END ===

//=== 只更新事件群組 ===
function changeEventGroup(mapKeyId) {
    console.log('=== 更新事件群組 ===');
    //== 新標籤群組 ==
    var lang = CookiesLang;
    // var lang = getCookie('CacheLang');
    if (lang == 'zh') {
        var GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + mapKeyId;
        // getGroupEventsCn(GroupApiLink);
        $('.map-event-side-tit').html('事件群組');
        getGroupSideEventsCn(GroupApiLink);
    } else {
        var GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + mapKeyId;
        // getGroupEventsEn(GroupApiLink);
        $('.map-event-side-tit').html('Event Group');
        getGroupSideEventsEn(GroupApiLink);
    }
}
//=== ＃號標籤搜尋 ===
function tagSrcPage(tagId) {
    var lang = CookiesLang;
    // var lang = getCookie('CacheLang');
    var protocol = window.location.protocol;
    var host = window.location.host;
    var domain = protocol + '//' + host;
    var linkPage = window.location.pathname;
    console.log(domain);

    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("target", "_blank");
    console.log(linkPage);
    if (linkPage == '/en/Writer/WriterCont') {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterSrcTagList?Tagid=" + tagId);
    }
    else if (linkPage == '/zh/Writer/WriterCont') {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterSrcTagList?Tagid=" + tagId);
    }
    else {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterStorySrcTagList?Tagid=" + tagId);
    }

    document.body.appendChild(form);
    form.submit();
}

//========== 滑動時 背景固定 ==========
var scTop;
function bgFix() {
    scTop = $(document).scrollTop();
    $('body').css({ 'top': -scTop, 'position': 'fixed', 'width': '100%' })
}
function bgUnFix() {
    $('body').removeAttr('style');
    $('html,body').animate({ scrollTop: scTop }, 0);
}