const tagCanvas = document.querySelector("#wordCloudBox");
let fontSize, rotate;
var tagArry = [
    // ['政治事件', 5, 'link'],
    // ['女性文學', 4, 'link'],
    // ['美術', 3, 'link'],
    // ['小說', 3, 'link'],
    // ['現實主義', 3, 'link'],
    // ['羅馬字', 3, 'link'],
    // ['移工', 3, 'link'],
    // ['政治小說', 3, 'link'],
    // ['政治文學', 3, 'link'],
    // ['後殖民', 2, 'link/test'],
    // ['政治小說', 2, 'link'],
    // ['留學生文學', 2, 'link'],
    // ['馬華文學', 2, 'link'],
    // ['高雄縣文學貢獻獎', 2, 'link'],
    // ['動物小說', 2, 'link'],
    // ['動畫', 2, 'link'],
    // ['新電影', 2, 'link'],
    // ['文學館', 2, 'link'],
    // ['日文小說', 2, 'link'],
    // ['洪建全兒童文學獎', 1, 'link'],
    // ['皇民化時期文學', 1, 'link'],
    // ['兒童文學獎童話獎', 1, 'link'],
    // ['府城文學貢獻獎', 1, 'link'],
    // ['客家文學', 1, 'link'],
    // ['後設', 1, 'link'],
    // ['後設小說', 1, 'link'],
    // ['報章雜誌', 1, 'link'],
    // ['散文', 1, 'link'],
    // ['離散文學', 1, 'link'],
    // ['短篇小說', 1, 'link'],
    // ['筆匯月刊', 1, 'link'],
    // ['白話文', 1, 'link'],
    // ['同志文學', 1, 'link'],
    // ['後殖民', 1, 'link'],
    // ['科幻小說', 1, 'link'],
    // ['純文學月刊', 1, 'link'],
    // ['高雄市文藝獎', 1, 'link'],
    // ['女性作家', 1, 'link'],
    // ['文學史', 1, 'link'],
    // ['兒童文學', 1, 'link'],
    // ['後現代', 1, 'link'],
    // ['混語文學', 1, 'link'],
    // ['現代文學', 1, 'link'],
    // ['現代主義', 1, 'link'],
    // ['現代詩', 1, 'link'],
    // ['現代戲劇', 1, 'link'],
    // ['華文文學', 1, 'link'],
    // ['評審委員推薦獎', 1, 'link'],
    // ['話劇金鼎獎', 1, 'link'],
    // ['榮譽文藝獎章', 1, 'link'],
];
const enTagArry = [
    // ['Transnational', 5, 'link'],
    // ['Books publication', 4, 'link'],
    // ['Literature activity', 3, 'link'],
    // ['Political incident', 3, 'link'],
    // ['Classical poem', 3, 'link'],
    // ['Novel', 3, 'link'],
    // ['Regional literature', 3, 'link'],
    // ['Gender novel', 3, 'link'],
    // ['Mixes the philology', 3, 'link'],
    // ['Latter colonizes', 2, 'link'],
    // ['Latter modern age', 2, 'link'],
    // ['Modern poem', 2, 'link'],
    // ['Latter supposes', 2, 'link'],
    // ['Taiwanese literature', 2, 'link'],
    // ['Prose', 2, 'link'],
    // ['Periodical magazine', 2, 'link'],
    // ['Being homesick', 2, 'link'],
    // ['Prize in literature', 2, 'link'],
    // ['Moves the labor', 2, 'link'],
    // ['Luo Mazi', 1, 'link'],
    // ['Vernacular literature', 1, 'link'],
    // ['Japanese creation', 1, 'link'],
    // ['Literature event', 1, 'link'],
    // ['Left wing literature', 1, 'link'],
    // ['Folk literature', 1, 'link'],
    // ['Literature collection', 1, 'link'],
    // ['Play', 1, 'link'],
    // ['Science fiction', 1, 'link'],
    // ['Realism', 1, 'link'],
    // ['Fine arts', 1, 'link'],
    // ['Political novel', 1, 'link'],
    // ['Memorial hall', 1, 'link'],
    // ['Nature writing', 1, 'link'],
    // ['Reports the literature', 1, 'link'],
    // ['228 literatures', 1, 'link'],
    // ['New movie', 1, 'link'],
    // ['Deductive', 1, 'link'],
    // ['Network literature', 1, 'link'],
    // ['River novel', 1, 'link'],
    // ['Cruel literature', 1, 'link'],
    // ['Literature anthology', 1, 'link'],
    // ['Youth novel', 1, 'link'],
    // ['Child literature', 1, 'link'],
    // ['Ballad', 1, 'link'],
    // ['Short story', 1, 'link'],
    // ['Yilan literature', 1, 'link'],
    // ['5 Animation', 1, 'link'],
    // ['Japanese novel', 1, 'link'],
    // ['Novel', 1, 'link'],
    // ['Translation', 1, 'link'],
];
var colors = ['#4297ab', '#40c9e0', '#f35b5b', '#98ce44', '#ffae48'];
// let option = {
//     list: tagArry,
//     classes: "tag-cloud-item",
//     gridSize: Math.round(5 * $('#wordCloudBox').width() / 1200),
//     weightFactor: fontSize,
//     // weightFactor: function (size) {
//     //     return Math.pow(size, 2.4) * $('#canvas').width() / 1200;
//     // },
//     drawOutOfBound: false,//不允許字詞超出寬高
//     shrinkToFit: false,
//     fontFamily: '"Noto Sans TC", sans-serif',
//     color: function (word, weight) {
//         //4
//         if (weight === 1) {
//             console.log('===', colors[0]);
//             return colors[0];
//         } else if (weight === 2) {
//             return colors[1];
//         } else if (weight === 3) {
//             return colors[2];
//         } else if (weight === 4) {
//             return colors[3];
//         } else if (weight === 5) {
//             return colors[4];
//         }
//     },
//     rotateRatio: 0.5,
//     rotationSteps: 2,
//     backgroundColor: 'transparent',
//     shuffle: false,//隨機渲染
//     clearCanvas: false,
//     click: (item) => { clickItem(item); }
// };
// $(document).on("load", function () {
$(document).ready(function () {
    // var protocol = window.location.protocol;
    // var host = window.location.host;
    var linkLang = window.location.pathname;
    // var lang = getCookie('CacheLang');
    // var lang = '@(Request.Cookies["CacheLang"].Value)';
    var lang = CookiesLang;
    if (lang == '' && linkLang == '/') {
        lang = 'zh';
    } else if (lang == '' && linkLang == '/zh') {
        lang = 'zh';
    } else if (lang == '' && linkLang == '/en') {
        lang = 'en';
    }
    console.log(linkLang);
    // console.log(lang);
    var apiLink;
    lang == 'zh' ? $('#wordCloudArea').addClass('cn') : $('#wordCloudArea').addClass('en');
    lang == 'zh' ? rotate = 0 : rotate = 0;//0.5是直橫 0只有橫
    lang == 'zh' ? apiLink = '/Api/WordArt?Type=1' : apiLink = '/Api/WordArt?Type=2';
    $.get(apiLink, function (data) {
        var dataTxt = data.split('],[');
        //=== 文字列表 ===
        var dataList = dataTxt[0] + ']';
        console.log(JSON.parse(dataList));
        var wordList = JSON.parse(dataList);
        tagArry = [];
        $.each(wordList, function (ind, val) {
            var { name, Atotal, Link } = val;
            if (name != '') {
                tagArry.push([name, Number(Atotal), Link]);
            }
        });
        //=== 顏色列表 ===
        var colorsList = '[' + dataTxt[1];
        colors = JSON.parse(colorsList);
        console.log(colors);
    });
    const winw = document.body.offsetWidth;

    console.log(lang);
    if (winw >= 481 && lang === 'zh') {
        fontSize = 20;
    } else if (winw < 480 && lang === 'zh') {
        fontSize = 12;
    } else if (winw >= 481 && lang === 'en') {
        fontSize = 16;
    } else if (winw < 481 && lang === 'en') {
        fontSize = 10;
    }

    //== 重新設置字體大小 ==
    // option.weightFactor = fontSize;
    //== 啟動文字雲 ==
    tagArry.length != 0 ? setWordCloud(tagArry, colors, fontSize, rotate) : $('#wordCloudArea').remove();
});
let resizeTimer = null;
$(window).resize(function () {
    const winw = document.body.offsetWidth;
    // console.log(lang);
    if (winw >= 481 && lang === 'zh') {
        fontSize = 20;
    } else if (winw < 480 && lang === 'zh') {
        fontSize = 12;
    } else if (winw >= 481 && lang === 'en') {
        fontSize = 16;
    } else if (winw < 481 && lang === 'en') {
        fontSize = 10;
    }
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        //== 重新設置字體大小 ==
        // option.weightFactor = fontSize;
        //== 啟動文字雲 ==
        tagArry.length != 0 ? setWordCloud(tagArry, colors, fontSize, rotate) : $('#wordCloudArea').remove();
        // setWordCloud(tagArry, colors, fontSize);
    }, 500);
});
//== 啟動文字雲 ==
function setWordCloud(tagArry, colors, fontSize, rotate) {
    var option = {
        list: tagArry,
        classes: "tag-cloud-item",
        gridSize: Math.round(5 * $('#wordCloudBox').width() / 1200) + 3,
        weightFactor: fontSize,
        // weightFactor: function (size) {
        //     return Math.pow(size, 2.4) * $('#canvas').width() / 1200;
        // },
        drawOutOfBound: false,//不允許字詞超出寬高
        shrinkToFit: false,
        fontFamily: '"Noto Sans TC", sans-serif',
        color: function (word, weight) {
            //4
            if (weight === 1) {
                return colors[0];
            } else if (weight === 2) {
                return colors[1];
            } else if (weight === 3) {
                return colors[2];
            } else if (weight === 4) {
                return colors[3];
            } else if (weight === 5) {
                return colors[4];
            }
        },
        rotateRatio: rotate,
        rotationSteps: 2,
        backgroundColor: 'transparent',
        shuffle: false,//隨機渲染
        clearCanvas: false,
        click: (item) => {
            clickItem(item);
        }
    };
    WordCloud(tagCanvas, option);
    tagCanvas.addEventListener('wordcloudstop', function (e) {
        // loop over all added elements (by class name)
        document.querySelectorAll('.tag-cloud-item').forEach(function (element) {
            const text = element.innerHTML;
            element.innerHTML = `<a target="_blank" style="color: inherit;">${text}</a>`;
        });
        $('.tag-cloud-item').find('a').each(function (ind, val) {
            var valTxt = $(val).text();
            if (valTxt == '') {
                $(val).remove();
            }
        });
    });
}
function clickItem(item) {
    // console.log(item[2]);
    window.open(item[2], '_blank');
    // location.href = item[2];
}