//== dom ==
//- var actCountry = 1;//給RWD 手機時顯示使用當前的地區館舍
$(document).ready(function () {
    //=== 官舍sw ===
    $('.museum-sw-box').each(function (ind, val) {
        var museumNum = $(val).data('num');
        //- console.log(museumNum);
        if (museumNum != undefined) {
            museumSw(museumNum);
        }
    });

    //=== 讀取的館舍狀態 ===
    //== dom ==
    //- var windowW = $(window).width();
    //- if (windowW < 768) {
    //-     reMbMuseum(actCountry);
    //- }
    //=== 手機切換地區 ===
    //== dom ==
    //- $('#museumCountryBox').on('click',function(){
    //-     var $mItem = $('#museumCountryItem');
    //-     var mItemShow = $mItem.is(':visible');
    //-     mItemShow ? $mItem.slideUp() : $mItem.slideDown();
    //- });
    //- $('.museumCountryName').on('click',function(){
    //-     var countryNum = $(this).data('num');
    //-     var countryTxt = $(this).html();
    //-     actCountry = countryNum;
    //-     $('#museumCountry').html(countryTxt);
    //-     reMbMuseum(actCountry);
    //- });
    //== select ==
    $('#museumPlace').on('change', function () {
        reMbMuseum();
        //- museumSw(actVal);
    });
});
//== select ==
$(window).on('load', function () {
    reMbMuseum();
});
$(window).resize(function () {
    var windowW = $(window).width();
    if (windowW < 768) {
        //== dom ==
        //- reMbMuseum(actCountry);
        //== select ==
        reMbMuseum();
    } else {
        $('.travel-family-museum-box').show();
        $('.travel-family-museum-box').removeClass('open');
    }
});

//=== Ｒ至手機時選擇的項目和sw開啟 ===
//== dom ==
//- function reMbMuseum(num){
//-     $('.travel-family-museum-box').removeClass('open');
//-     $('.museumArea'+num).addClass('open');
//- }
//== select ==
function reMbMuseum() {
    var actVal = $('#museumPlace').val();
    $('.travel-family-museum-box').removeClass('open');
    $('.museumArea' + actVal).addClass('open');
    //- console.log(actVal);
    //- $('.travel-family-museum-box').hide();
    //- $('.museumArea'+actVal).show();
}

function museumSw(num) {
    new Swiper('#travelFamilySw' + num, {
        spaceBetween: 0,
        navigation: {
            nextEl: '#familySwNext' + num,
            prevEl: '#familySwPrev' + num,
        },
    });
}