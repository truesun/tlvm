$(document).ready(function () {
    loadApp();
    //- yepnope({
    //- 	test: Modernizr.csstransforms,
    //- 	yep: ['js/book/turn.js'],
    //- 	nope: ['js/book/turn.html4.min.js'],
    //- 	both: ['js/book/zoom.min.js', 'js/book/magazine.js', 'js/book/magazine.css'],
    //- 	complete: loadApp
    //- });
    //=== 鍵盤翻頁和放大關閉 ===
    // Using arrow keys to turn the page
    $(document).keydown(function (e) {

        var previous = 37, next = 39, esc = 27;

        switch (e.keyCode) {
            case previous:

                // left arrow
                $('.magazine').turn('previous');
                e.preventDefault();

                break;
            case next:

                //right arrow
                $('.magazine').turn('next');
                e.preventDefault();

                break;
            case esc:

                $('.magazine-viewport').zoom('zoomOut');
                e.preventDefault();

                break;
        }
    });

    //=== 網址頁數 ===
    // URIs - Format #/page/1 
    Hash.on('^page\/([0-9]*)$', {
        yep: function (path, parts) {
            var page = parts[1];

            if (page !== undefined) {
                if ($('.magazine').turn('is'))
                    $('.magazine').turn('page', page);
            }

        },
        nop: function (path) {

            if ($('.magazine').turn('is'))
                $('.magazine').turn('page', 1);
        }
    });

    //=== 小圖頁碼 ===
    // Events for thumbnails
    $('.thumbnails').click(function (event) {

        var page;

        if (event.target && (page = /page-([0-9]+)/.exec($(event.target).attr('class')))) {

            $('.magazine').turn('page', page[1]);
        }
    });

    $('.thumbnails li').
        bind($.mouseEvents.over, function () {

            $(this).addClass('thumb-hover');

        }).bind($.mouseEvents.out, function () {

            $(this).removeClass('thumb-hover');

        });

    if ($.isTouch) {

        $('.thumbnails').
            addClass('thumbanils-touch').
            bind($.mouseEvents.move, function (event) {
                event.preventDefault();
            });

    } else {

        $('.thumbnails ul').mouseover(function () {

            $('.thumbnails').addClass('thumbnails-hover');

        }).mousedown(function () {

            return false;

        }).mouseout(function () {

            $('.thumbnails').removeClass('thumbnails-hover');

        });

    }

    // Regions
    if ($.isTouch) {
        $('.magazine').bind('touchstart', regionClick);
    } else {
        $('.magazine').click(regionClick);
    }

    // Events for the next button(下一頁按鈕)
    $('#nextBtn').on('click', (e) => {
        $('.magazine').turn('next');
    });

    // Events for the next button(上一頁按鈕)
    $('#preBtn').on('click', (e) => {
        $('.magazine').turn('previous');
    });

    //=== 放大縮小 ===
    // Zoom icon
    let zoomSwitch = true;
    $('#zoomBtn').on('click', (e) => {
        if (zoomSwitch) {
            $('.magazine-viewport').zoom('zoomIn');
            zoomSwitch = false;
        } else {
            $('.magazine-viewport').zoom('zoomOut');
            zoomSwitch = true;
        }
    });

    //- $('#canvas').hide();
});
//=== resize ===
$(window).resize(function () {
    resizeViewport();
}).bind('orientationchange', function () {
    resizeViewport();
});
//=== 主要功能 ===
function loadApp() {
    //- window.page_image_path = 'https://literature.sds.com.tw/storage/ReadingTranslation/pages/487/%page'
    //= 這個路徑會在 magazine.js 裡面使用到，原本會切換圖片大小張，但改為統一一張大圖就好 =
    window.page_image_path = "img/";

    $('#canvas').fadeIn(1000);

    const imgW = $('#fristImg').find('img').width();
    const imgH = $('#fristImg').find('img').height();
    //- console.log(imgW, imgH);
    var flipbook = $('.magazine');

    // Check if the CSS was already loaded

    if (flipbook.width() == 0 || flipbook.height() == 0) {
        setTimeout(loadApp, 10);
        return;
    }

    // Create the flipbook

    flipbook.turn({
        direction: "ltr",

        // Magazine width

        width: imgW * 2,//2400,

        // Magazine height

        height: imgH,//1695,

        // Duration in millisecond

        duration: 1000,

        // Hardware acceleration

        acceleration: !isChrome(),

        // Enables gradients

        gradients: true,

        // Auto center this flipbook

        autoCenter: true,

        // Elevation from the edge of the flipbook when turning a page

        elevation: 50,

        // The number of pages

        // pages: 6,//頁數本來在這，在 missing 功能，利用頁數去讀圖片

        // Events

        when: {
            turning: function (event, page, view) {

                var book = $(this),
                    currentPage = book.turn('page'),
                    pages = book.turn('pages');

                // Update the current URI

                Hash.go('page/' + page).update();

                // Show and hide navigation buttons

                disableControls(page);


                $('.thumbnails .page-' + currentPage).
                    parent().
                    removeClass('current');

                $('.thumbnails .page-' + page).
                    parent().
                    addClass('current');
            },

            turned: function (event, page, view) {

                disableControls(page);

                $(this).turn('center');

                if (page == 1) {
                    $(this).turn('peel', 'br');
                }

            },

            missing: function (event, pages) {

                // Add pages that aren't in the magazine
                //不需要動態加入，直接放在dom上
                // for (var i = 0; i < pages.length; i++)
                //   addPage(pages[i], $(this));

            }
        }

    });

    // Zoom.js
    $('.magazine-viewport').zoom({
        flipbook: $('.magazine'),

        max: function () {

            return largeMagazineWidth() / $('.magazine').width();

        },

        when: {

            swipeLeft: function () {

                $(this).zoom('flipbook').turn('next');

            },

            swipeRight: function () {

                $(this).zoom('flipbook').turn('previous');

            },

            resize: function (event, scale, page, pageElement) {

                if (scale == 1)
                    loadSmallPage(page, pageElement);
                else
                    loadLargePage(page, pageElement);

            },

            zoomIn: function () {

                $('.thumbnails').hide();
                $('.made').hide();
                $('.magazine').removeClass('animated').addClass('zoom-in');
                //- $('.zoom-icon').removeClass('zoom-icon-in').addClass('zoom-icon-out');
                $('#zoomBtn').html('<i class="fas fa-compress-alt"></i> 縮小');

                if (!window.escTip && !$.isTouch) {
                    escTip = true;

                    $('<div />', { 'class': 'exit-message' }).
                        html('<div>點擊 ESC 離開</div>').
                        appendTo($('body')).
                        delay(2000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });
                }
            },

            zoomOut: function () {

                $('.exit-message').hide();
                $('.thumbnails').fadeIn();
                $('.made').fadeIn();
                //- $('.zoom-icon').removeClass('zoom-icon-out').addClass('zoom-icon-in');
                $('#zoomBtn').html('<i class="fas fa-expand-alt"></i> 放大');

                setTimeout(function () {
                    $('.magazine').addClass('animated').removeClass('zoom-in');
                    resizeViewport();
                }, 0);

            }
        }
    });

    // Zoom event(本藏)

    // if ($.isTouch)
    //   $('.magazine-viewport').bind('zoom.doubleTap', zoomTo);
    // else
    //   $('.magazine-viewport').bind('zoom.tap', zoomTo);


    resizeViewport();

    $('.magazine').addClass('animated');
}