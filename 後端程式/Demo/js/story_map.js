mapboxgl.accessToken = 'pk.eyJ1IjoidHJ1ZXN1biIsImEiOiJja25nMDBzbmUwamZpMnhtZXlzNXM2YnFqIn0.F2UM5EyLSGmv_lbwkO_4LA';
var map, popup, mapLanguage;//因為設置為function啟動 需要宣告在最外層才可以共同使用
var actThem = 'routeLife1';//== 當前主題 == // routeLife1
var actNum = 0;//== 當前point資料位置 ==
var defaultThemeName = '';// 葉石濤大事記
var actMapKeyId = '';//當前主題key 事件使用
// var mapDataList = [];
var pathColorList = ['#CB5050', '#f1b43e', '#4ab93d'];//#f1b43e,#f1d53e//'#50A4CB', '#ca83cb'

$(document).ready(function () {
    $('#goTop').remove();
    var apiLink, GroupApiLink, apiThemeLink;
    //=== 作家行跡或地圖故事 ===
    var pageLink = window.location.pathname;
    var pageLinkList = pageLink.split('/');
    var pageActInd = pageLinkList.length - 1;
    var pageAct = pageLinkList[pageActInd];//WriterCont Or WriterStoryCont
    var themeId;
    if (pageAct == 'WriterCont') {
        themeId = 1;
    } else if (pageAct == 'WriterStoryCont') {
        themeId = 2;
    }

    var lang = getCookie('CacheLang');
    var mapKey = getLinkVal('Wid') || getCookie('Key');
    actMapKeyId = mapKey;
    lang == 'zh' ? mapLanguage = "zh-Hant" : mapLanguage = "en";
    if (lang == 'zh') {
        apiLink = '/Api/WebServiceMapCn?Type=1&key=' + mapKey;
        // apiLink = '/WebServiceMapCn.aspx?Type=1&key=' + mapKey;
        loadSetMapCn(apiLink);
        // apiLink = '/Api/WebServiceMapCn?Type=1&key=' + mapKey;
        // getMapCn(apiLink, mapEvent);
        GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + mapKey;
        getGroupEventsCn(GroupApiLink);
        //== 所有主題 ==
        apiThemeLink = '/Api/WebServiceMapCn?Type=4&key=' + themeId;
        // apiThemeLink = '/WebServiceMapCn.aspx?Type=4&key=' + themeId;
    } else {
        apiLink = '/Api/WebServiceMapEn?Type=1&key=' + mapKey;
        // apiLink = '/WebServiceMapEn.aspx?Type=1&key=' + mapKey;
        loadSetMapEn(apiLink);
        // apiLink = '/Api/WebServiceMapEn?Type=1&key=' + mapKey;
        // getMapEn(apiLink, mapEvent);
        GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + mapKey;
        getGroupEventsEn(GroupApiLink);
        //== 所有主題 ==
        apiThemeLink = '/Api/WebServiceMapEn?Type=4&key=' + themeId;
        // apiThemeLink = '/WebServiceMapEn.aspx?Type=4&key=' + themeId;
    }

    //=== 主題選擇 ===
    // apiThemeLink = '/WebServiceMapCn.aspx?Type=4&key=1';
    // var apiThemeLink = '/WebServiceMapEn.aspx?Type=4&key=1';//英
    var storyNameBtn = 'storyNameBtn';
    var buildInfo = '<div class="error-page">' +
        '<div class="build-cont">' +
        '<div class="error-info">' +
        '<div class="error-txt build-txt">Sorry！網站建置中</div>' +
        '<div class="error-en">Coming Soon...</div>' +
        '<a class="btn" href="/">回到首頁 / Back Home</a>' +
        '</div>' +
        '</div>' +
        '</div>';
    //=== 置入路線主題 ===
    $.get(apiThemeLink, function (data) {
        var resData = JSON.parse(data);
        console.log(resData);
        var titCont = resData.filter(function (item) {
            return item.Wid == mapKey;
        });
        if (titCont[0].SubjectName == '') {
            console.log('顯示建置中');
            $('.page-wrap').html(buildInfo);
        } else {
            $.each(resData, function (ind, val) {
                var { Wid, SubjectName, Pic } = val;
                if (SubjectName !== '') {
                    $('#mapThemeBox').append('<div class="map-theme-box" id="' + storyNameBtn + Wid + '">' +
                        '<div class="img"><img src="' + Pic + '" alt="" /></div>' +
                        '<div class="name">' + SubjectName + '</div>' +
                        '</div>');
                }
            });
            var sendTxt;
            lang == 'zh' ? sendTxt = '送出' : sendTxt = 'submit';
            $('#mapThemeBox').append('<div class="map-theme-btn" id="mapThemeBtn"><i class="fas fa-check-circle"></i> ' + sendTxt + '</div>');
            $('#storyNameBtn' + mapKey).addClass('act');
        }
    });
    //=== 最多選擇三筆 ===
    $(document).on('click', '.map-theme-box', function () {
        var actL = $('#mapThemeBox').find('.act').length;
        var hasAct = $(this).hasClass('act');
        if (actL == 3 && hasAct == false) {
            alert('最多選擇三個主題。');
        } else if (actL !== 3 && hasAct == false) {
            $(this).addClass('act');
        }
    });
    $(document).on('click', '.map-theme-box.act', function () {
        // var actL = $('#mapThemeBox').find('.act').length;
        $(this).removeClass('act');
    });
    //=== 送出選擇主題 ===
    $('#mapThemeBtn').on('click', function () {
        var choseTheme = [];
        $('.map-theme-box.act').each(function (param) {
            var choseIdTxt = $(this).attr('id');
            var choseId = choseIdTxt.replace(storyNameBtn, '');
            choseTheme.push(choseId);
        });
        if (choseTheme.length) {
            var apiIdList = choseTheme.join(',');
            console.log(apiIdList);
            actMapKeyId = choseTheme[0];
            $('#storyCover').show();
            $('#storyCont').hide();
            $('#preBtn,#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
            if (lang == 'zh') {
                //== 設置新地圖 ==
                // apiLink = '/WebServiceMapCn.aspx?Type=1&key=' + apiIdList;
                apiLink = '/Api/WebServiceMapCn?Type=1&key=' + apiIdList;
                loadSetMapCn(apiLink);
                //== 新標籤群組 ==
                GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + actMapKeyId;
                getGroupEventsCn(GroupApiLink);
            } else {
                //== 設置新地圖 ==
                // apiLink = '/WebServiceMapEn.aspx?Type=1&key=' + apiIdList;
                apiLink = '/Api/WebServiceMapEn?Type=1&key=' + apiIdList;
                loadSetMapEn(apiLink);
                //== 新標籤群組 ==
                GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + actMapKeyId;
                getGroupEventsEn(GroupApiLink);
            }
        } else {
            if (lang == 'zh') {
                alert('請至少選擇一個主題。');
            } else {
                alert('Please Chose One Theme.');
            }
        }
    });
    //=== 第一筆資料開合 ===
    $('#storyCoverBtn').on('click', function () {
        var isShow = $('#storyCover').is(':visible');
        isShow ? $('#storyCover').slideUp() : $('#storyCover').slideDown();
    });
    //=== 重置按鈕 ===
    // $('#storyCont').hide();
    $('#storyContBox').mCustomScrollbar();//側邊內容滾輪
    $('#preBtn,#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
    //=== 選擇第一筆資料前往 ===
    $(document).on('click', '.story-start-btn', function () {
        var btnId = $(this).attr('id').replace('storyStartBtn', '');
        var mapKeyId = $(this).data('mapkey');
        actThem = 'routeLife' + btnId;
        actNum = 0;//回到第一筆
        var goMapRes = map.getSource(actThem);
        console.log('=== 前往選擇資料 ===');
        console.log(goMapRes);
        setTxtData(goMapRes);
        defaultThemeName = goMapRes._data.themeName;
        //== 放入當前主題 ==
        // $('#them span').html(actThem + ':' + defaultThemeName);
        $('#them span').html(defaultThemeName);
        // $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
        $('#storyCont').show();
        $('#nextBtn').removeAttr('style');
        //== 新標籤群組 ==(如果當前不同時在更新)
        if (actMapKeyId != mapKeyId) {
            actMapKeyId = mapKeyId;
            changeEventGroup(actMapKeyId);
        }
    });
    //=== 上下則 ===
    // $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
    $('#preBtn').on('click', function () {
        // var testMapRes = map.getSource('routeLife1');
        var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
        var pointL = testMapRes._data.features.length;
        console.log(pointL);
        actNum = actNum - 1;
        if (actNum == 0) {
            $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
            if (pointL != 1) {
                $('#nextBtn').removeAttr('style');
            }
        } else {
            $('#nextBtn').removeAttr('style');
        }
        //== 設置詳細資訊 ==
        setTxtData(testMapRes);
    });
    $('#nextBtn').on('click', function () {
        // var testMapRes = map.getSource('routeLife1');
        var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
        var pointL = testMapRes._data.features.length;
        console.log(testMapRes._data);
        console.log(pointL);
        actNum = actNum + 1;
        if (actNum == pointL - 1) {
            $('#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' });
            $('#preBtn').removeAttr('style');
        } else {
            if (actNum !== 0) {
                $('#preBtn').removeAttr('style');
            }
        }
        //== 設置詳細資訊 ==
        setTxtData(testMapRes);
    });

    //== ＃號標籤收尋 ==
    $(document).on('click', '.tagBtn', function () {
        var tagId = $(this).data('tagid');
        tagSrcPage(tagId);
    });

    //=== 下方標籤開闔 ===
    var $moreMapTagBtn = $('#moreMapTagBtn');
    var $MapTagBox = $('#allMapTagBox');

    var tagOpen = false;
    $moreMapTagBtn.on('click', function () {
        var tabBtnTxt;
        if (tagOpen) {
            $MapTagBox.removeClass('open');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '展開' : tabBtnTxt = 'More';
            $moreMapTagBtn.find('span').html(tabBtnTxt);

            $('.map-all-tag').mCustomScrollbar("destroy");
            tagOpen = false;
        } else {
            $MapTagBox.addClass('open');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '收合' : tabBtnTxt = 'Close';
            $moreMapTagBtn.find('span').html(tabBtnTxt);

            $('.map-all-tag').mCustomScrollbar();
            tagOpen = true;
        }
    });

    $(document).on('click', '.tag', function () {
        var indEvent = $(this).data('tagid');
        var $eventTagBox = $('#eventsTag' + indEvent);
        var eventTagBoxShow = $eventTagBox.is(':visible');
        if (eventTagBoxShow) {
            $('.tag').removeClass('act');
            $('.map-event').hide();
            $('#mapEventBox').hide();
        } else {
            $('.tag').removeClass('act');
            $(this).addClass('act');
            if ($eventTagBox.length) {
                $('.map-event').hide();
                $('#mapEventBox').show();
                $eventTagBox.show();
            } else {
                $('.map-event').hide();
                $('#mapEventBox').hide();
            }
        }
    });
    //== 事件標籤 ==
    $(document).on('click', '.t-even', function () {
        var eventIdAct = $(this).data('eventid');
        eventGoTo(eventIdAct);
    });
});
//=== 標籤群組事件前往 ===
function eventGoTo(eventId) {
    var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
    var dataListL = testMapRes._data.features.length;
    var eventInd = testMapRes._data.features.findIndex(function (item) {
        return item.properties.eventId == eventId;
    });
    actNum = eventInd;
    //== 給上下則按鈕狀態更新 ==
    actNum == 0 ? $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#preBtn').removeAttr('style');
    actNum == dataListL - 1 ? $('#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#nextBtn').removeAttr('style');
    setTxtData(testMapRes);
    //== 如果還沒按開始，第一個點選標籤時要打開內容 ==
    $('#storyCont').show();
    console.log(actNum);
}

//=== 讀取設置地圖 ===
function loadSetMapCn(apiLink) {
    var mapDataList = [];
    $.get(apiLink, function (data) {
        var resData = JSON.parse(data);
        console.log(resData);
        $.each(resData, function (ind, val) {
            // var pointDataListTxt = val.data;
            var { key, keyName, data } = val;
            var pointDataList = JSON.parse(data);
            console.log(keyName)
            console.log(pointDataList);
            //
            var mapReData = [];//路徑和點主要資料
            var mapRepath = [];//路線線條資料
            $.each(pointDataList, function (ind, val) {
                var { Eventid, Name_tw, Content_tw, longitude, latitude, pic_tw, med_tw, data } = val;
                var tagListData = JSON.parse(data.TagidArray);
                var tagList = [];
                $.each(tagListData, function (ind, val) {
                    var { Tagid, Name_tw } = val;
                    tagList.push({ 'tagId': Tagid, 'tagName': Name_tw });
                });
                mapReData.push({
                    'type': 'Feature',
                    'properties': { 'eventId': Eventid, 'title': Name_tw, 'description': Content_tw, 'pic': pic_tw, 'med': med_tw, 'tagList': tagList },
                    'geometry': { 'type': 'Point', 'coordinates': [Number(longitude), Number(latitude)] }
                });
                mapRepath.push([Number(longitude), Number(latitude)]);
            });
            // console.log('=== 喵喵SEE ===');
            // console.log(mapReData);
            // console.log(mapRepath);
            mapDataList.push({ 'mapKey': key, 'mapName': keyName, 'mapData': mapReData, 'pathData': mapRepath });
            //== 帶入第一個主題名稱 ==
            // actThem = 'routeLife1';
            // defaultThemeName = mapDataList[0].mapName;
        });
    });
    console.log(mapDataList);
    //=== 開始讀取地圖 ===
    mapStart(mapDataList);
    //=== 增加隱藏按鈕 ===
    // $('#menu').html('');
    $('#storyCover').html('');
    $.each(mapDataList, function (ind, val) {
        console.log(ind);
        var num = ind + 1;
        var routeId = 'route' + num;
        var markId = 'routeMark' + num;
        var { mapKey, mapName } = val;

        // var link = document.createElement('a');
        // link.id = routeId;
        // //toggMarkLayerIds : link.textContent = '隱藏' + routeId;
        // link.textContent = mapName;//按鈕名稱
        // link.markContent = markId;
        // link.className = 'active';

        // link.style.background = pathColorList[ind];

        // $('#menu').append(link);

        // link.onclick = function (e) {
        //     var clickedLayer = this.id;//一個關路徑
        //     var markLayer = this.markContent;//一個關標記mark
        //     console.log(this.textContent);
        //     console.log(this.markContent);
        //     console.log('111', markLayer);
        //     console.log('clickedLayer', clickedLayer);
        //     console.log('markLayer', markLayer);
        //     e.preventDefault();//js停止事件的默認行為，例如a的href，會阻擋觸發
        //     e.stopPropagation();//js中阻止冒泡事件
        //     var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

        //     // Toggle layer visibility by changing the layout object's visibility property.
        //     if (visibility === 'visible') {
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'none');
        //         map.setLayoutProperty(markLayer, 'visibility', 'none');
        //         this.className = '';
        //         console.log('markLayer點點')
        //     } else {
        //         this.className = 'active';
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
        //         map.setLayoutProperty(markLayer, 'visibility', 'visible');
        //     }
        // };
        //== 加入開始按鈕 ==
        $('#storyCover').append('<div class="story-start-btn" data-mapkey="' + mapKey + '" id="storyStartBtn' + (ind + 1) + '">開始《' + mapName + '》</div>');
    });
}
function loadSetMapEn(apiLink) {
    var mapDataList = [];
    $.get(apiLink, function (data) {
        var resData = JSON.parse(data);
        console.log(resData);
        $.each(resData, function (ind, val) {
            // var pointDataListTxt = val.data;
            var { key, keyName, data } = val;
            var pointDataList = JSON.parse(data);
            console.log(keyName)
            console.log(pointDataList);
            //
            var mapReData = [];//路徑和點主要資料
            var mapRepath = [];//路線線條資料
            $.each(pointDataList, function (ind, val) {
                var { Eventid, Name_en, Content_en, longitude, latitude, pic_en, med_en, data } = val;
                var tagListData = JSON.parse(data.TagidArray);
                var tagList = [];
                $.each(tagListData, function (ind, val) {
                    var { Tagid, Name_en } = val;
                    tagList.push({ 'tagId': Tagid, 'tagName': Name_en });
                });
                mapReData.push({
                    'type': 'Feature',
                    'properties': { 'eventId': Eventid, 'title': Name_en, 'description': Content_en, 'pic': pic_en, 'med': med_en, 'tagList': tagList },
                    'geometry': { 'type': 'Point', 'coordinates': [Number(longitude), Number(latitude)] }
                });
                mapRepath.push([Number(longitude), Number(latitude)]);
            });
            // console.log('=== 喵喵SEE ===');
            // console.log(mapReData);
            // console.log(mapRepath);
            mapDataList.push({ 'mapKey': key, 'mapName': keyName, 'mapData': mapReData, 'pathData': mapRepath });
            //== 帶入第一個主題名稱 ==
            // defaultThemeName = mapDataList[0].mapName;
        });
    });
    console.log(mapDataList);
    //=== 開始讀取地圖 ===
    mapStart(mapDataList);
    //=== 增加隱藏按鈕 ===
    // $('#menu').html('');
    $('#storyCover').html('');
    $.each(mapDataList, function (ind, val) {
        console.log(ind);
        var num = ind + 1;
        var routeId = 'route' + num;
        var markId = 'routeMark' + num;
        var { mapKey, mapName } = val;

        // var link = document.createElement('a');
        // link.id = routeId;
        // //toggMarkLayerIds : link.textContent = '隱藏' + routeId;
        // link.textContent = mapName;//按鈕名稱
        // link.markContent = markId;
        // link.className = 'active';

        // link.style.background = pathColorList[ind];

        // $('#menu').append(link);

        // link.onclick = function (e) {
        //     var clickedLayer = this.id;//一個關路徑
        //     var markLayer = this.markContent;//一個關標記mark
        //     console.log(this.textContent);
        //     console.log(this.markContent);
        //     console.log('111', markLayer);
        //     console.log('clickedLayer', clickedLayer);
        //     console.log('markLayer', markLayer);
        //     e.preventDefault();//js停止事件的默認行為，例如a的href，會阻擋觸發
        //     e.stopPropagation();//js中阻止冒泡事件
        //     var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

        //     // Toggle layer visibility by changing the layout object's visibility property.
        //     if (visibility === 'visible') {
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'none');
        //         map.setLayoutProperty(markLayer, 'visibility', 'none');
        //         this.className = '';
        //         console.log('markLayer點點')
        //     } else {
        //         this.className = 'active';
        //         map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
        //         map.setLayoutProperty(markLayer, 'visibility', 'visible');
        //     }
        // };
        //== 加入開始按鈕 ==
        $('#storyCover').append('<div class="story-start-btn" data-mapkey="' + mapKey + '" id="storyStartBtn' + (ind + 1) + '">START《' + mapName + '》</div>');
    });
}
//=== 地圖設置 ===
function mapStart(mapDataList) {
    map = new mapboxgl.Map({//初始化
        container: 'map',
        // text-field: 'system-ui',
        // style: 'mapbox://styles/mapbox/streets-v11', //自訂樣式藍色系
        style: 'mapbox://styles/truesun/ckq76o3dp0g8f17rzdwwp5rcp',
        // style: 'mapbox://styles/mapbox/light-v10',
        //藍色系：mapbox://styles/truesun/ckq76o3dp0g8f17rzdwwp5rcp
        // 一般樣式 style: 'mapbox://styles/mapbox/streets-v11',
        //- 預設樣式： mapbox://styles/mapbox/streets-v11
        //- 暗黑模式：mapbox://styles/mapbox/dark-v10
        center: [120.587043762207, 23.9626407623291],//預設地圖中心點
        zoom: 6, // 8 預設地圖縮放比例，可以到小數點兩位
        maxZoom: 25,//defalut 15
        pitch: 0, //可以用俯視角度看地圖（上下角度）
        bearing: 0, // 可以用俯視角度看地圖（左右角度）,
        localIdeographFontFamily: " system-ui,'Noto Sans', 'Noto Sans CJK SC', sans-serif "//可以設定字體，但是應用了自訂的map style所以會被設為false
    });

    map.on('load', function () {
        console.log('123');
        //=== 路徑圖片設置 ===
        // https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png
        // map.loadImage('https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',//img/icon_location.png
        // 	// Add an image to use as a custom marker
        // 	function (error, image) {
        // 		if (error) throw error;
        // 		map.addImage('custom-marker', image);
        // 		$.each(mapDataList, function (ind, val) {
        // 			var idNum = ind + 1;
        // 			var routeId = 'routeLife' + idNum;//總資料
        // 			var markId = 'routeMark' + idNum;//總點名稱ID
        // 			var { mapName, mapData } = val;
        // 			//routeLife1,'大事記',路徑資料,routeMark1
        // 			setMainRouteData(routeId, mapName, mapData, markId);
        // 		});
        // 	});
        //=== 加路徑線條 資料 ===
        $.each(mapDataList, function (ind, val) {
            var idNum = ind + 1;
            var routeLifeId = 'routeLife' + idNum;//總資料
            var markId = 'routeMark' + idNum;//總點名稱ID
            var { mapKey, mapName, mapData } = val;

            var routePathId = 'routePath' + idNum;//路線資料定義
            var routeId = 'route' + idNum;//總路線名稱ID
            var { pathData } = val;
            //routePath1,路徑資料,route1
            setMainRoutePathData(routePathId, pathData, routeId, pathColorList[ind]);
            //routeLife1,主題ID,主題名稱,主題主資料,mark點ID(路徑和mark點設置)
            setMainRouteData2(routeLifeId, mapKey, mapName, mapData, markId, pathColorList[ind]);
        });
        //=== 前往搜尋近來得事件 ===
        var mapEvent = getLinkVal('Eventid');
        if (mapEvent) {
            eventGoTo(mapEvent);
        }
    });
    // Center the map on the coordinates of any clicked symbol from the 'symbols' layer.
    /**** 滑鼠移過出現popup  ****/
    // Create a popup, but don't add it to the map yet.
    popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });
    //=== 點擊座標居中 ===
    //routeMark1對應的是lyaer id
    $.each(mapDataList, function (ind, val) {
        var idNum = ind + 1;
        var markId = 'routeMark' + idNum;//總點名稱ID
        //routeMark1
        setMakerFun(markId);
    });

    //=== 增加控制地圖縮放 ===
    map.addControl(new mapboxgl.NavigationControl());
    //=== 地圖設置語系 (預設全地圖語系)===
    var mapboxLanguage = new MapboxLanguage({
        defaultLanguage: mapLanguage //'en'
    });
    map.addControl(mapboxLanguage);
}
//=== 地圖設置語系(無,這是跑員資料後替換標籤內的語系) ===
// function setMapLang(lang) {
//     map.setLayoutProperty('country-label', 'text-field', [
//         'get',
//         `name_${lang}`
//     ]);
// }
//=== 設置主要路線資料和打點 ===
//routeLife1,1,葉石濤大事紀,sourceData(mapDataList.mapData),routeMark1(img打點)
function setMainRouteData2(sourceId, mapThemeKey, themeName, sourceData, layerMarkId, circleColor) {
    //== 路徑資料 ==
    map.addSource(sourceId, {//routeLife1
        'type': 'geojson',
        'data': {
            'type': 'FeatureCollection',
            'themeId': mapThemeKey,
            'themeName': themeName,//葉石濤大事紀
            'features': sourceData
        }
    });
    //== 打點資料 ==
    map.addLayer({
        'id': layerMarkId,
        'type': 'circle',
        'source': sourceId,
        'paint': {
            'circle-color': circleColor,//#4264fb
            'circle-radius': 6,
            'circle-stroke-width': 2,
            'circle-stroke-color': '#ffffff'
        }
    });
}
//=== 路線路徑資料與設定 ===
//routePath1,pathSourceData(mapDataList.pathData),route1,#CB5050
function setMainRoutePathData(pathSourceId, pathSourceData, layerRouteId, pathColor) {
    //== 路徑(路線)資料 ==
    map.addSource(pathSourceId, {// 路徑資料ID routePath1
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': pathSourceData//路線線條array
            }
        }
    });
    //== 路徑設定 ==
    map.addLayer({
        'id': layerRouteId,//路徑線條群組設定ID route1
        'type': 'line',
        'source': pathSourceId,//路徑資料來源
        'layout': {
            'line-join': 'round',
            'line-cap': 'round',
            'visibility': 'visible'
        },
        'paint': {
            'line-color': pathColor,//#CB5050
            'line-width': 3,
            // 'line-dasharray': [1, 2],//可以做虛線 [線段,線距]
        }
    });
}
//=== 座標點點擊設定 ===
//routeMark1
function setMakerFun(layerMarkId) {
    //=== 點擊座標居中 ===
    map.on('click', layerMarkId, function (e) { //要aabd是layer的id
        console.log('=== wwwwww ===');
        console.log(e.features[0]);
        map.flyTo({
            center: e.features[0].geometry.coordinates, //取得物件的座標
            //- center: [ //這邊是墾丁的座標
            //- 	120.74 + (Math.random() - 0.5) * 10,
            //- 	21.99 + (Math.random() - 0.5) * 10
            //- ],
            speed: 0.5,//移動過去的速度 數字越大越快
            essential: true // 啟用物理動畫
        });
        //== 點擊後設置資料 ==
        markPointSet(e);
    });
    //== hover popup ==
    map.on('mouseenter', layerMarkId, function (e) {//places要對應 map.addSource的參數
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        var coordinates = e.features[0].geometry.coordinates.slice();
        var description = e.features[0].properties.description;

        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(coordinates).setHTML(description).addTo(map);
    });

    map.on('mouseleave', layerMarkId, function (e) { //places要對應 map.addSource的參數
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
    //== click popup ==
    // map.on('click', 'routeMark2', function (e) {
    // 	var coordinates = e.features[0].geometry.coordinates.slice();
    // 	var description = e.features[0].properties.description;

    // 	//- Ensure that if the map is zoomed out such that multiple
    // 	// copies of the feature are visible, the popup appears
    // 	//- over the copy being pointed to.
    // 	while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
    // 		coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    // 	}

    // 	new mapboxgl.Popup()
    // 		.setLngLat(coordinates)
    // 		.setHTML(description)
    // 		.addTo(map);
    // });
    // //- Change the cursor to a pointer when the mouse is over the places layer.
    // map.on('mouseenter', 'routeMark2', function () {
    // 	map.getCanvas().style.cursor = 'pointer';
    // });
    // //- Change it back to a pointer when it leaves.
    // map.on('mouseleave', 'routeMark2', function () {
    // 	map.getCanvas().style.cursor = '';
    // });
}
//=== 上下則資料帶入 ===
function setTxtData(testMapRes) {
    //== 加入資料顯示動畫 ==
    changeDataAni();

    //== 資料顯示內容 ==(點擊上下則不會改變主要主題);
    var { title, description, pic, med, tagList } = testMapRes._data.features[actNum].properties;
    // var title = testMapRes._data.features[actNum].properties.title;
    // var cont = testMapRes._data.features[actNum].properties.description;
    $('#themTit').html(title);
    $('#themCont').html(description);
    pic ? $('#themeImg').show() : $('#themeImg').hide();
    $('.story-theme-img img').attr('src', pic);
    $('.story-theme-img-note').html(med);
    // var tagList = testMapRes._data.features[actNum].properties.tagList;
    // console.log(tagList);
    var tagListL = tagList.length;
    if (tagListL != 0) {
        $('#themTag').html('');
        $.each(tagList, function (ind, val) {
            $('#themTag').append('<div data-tagid="' + val.tagId + '" class="tagBtn">#' + val.tagName + '</div>');
        });
    } else {
        $('#themTag').html('');
    }
    //== 前往位置 ==
    var goPos = testMapRes._data.features[actNum].geometry.coordinates;
    // console.log(goPos);
    mapFlyTo(goPos);
    console.log(actNum);
}
//===  地圖 - 飛向位置 ===
function mapFlyTo(target) {
    map.flyTo({
        center: target,
        essential: true, // this animation is considered essential with respect to prefers-reduced-motion
        speed: 1,
        curve: 2,
        zoom: 15//15
    });
}
//=== Mark點擊設置資料 ===
function markPointSet(point) {
    console.log(point.features[0].source);
    console.log('= 點擊Id =');
    console.log(point.features[0].properties.eventId);
    //== 加入資料顯示動畫 ==
    changeDataAni();

    //== 帶入點擊point的當前主題 ==
    actThem = point.features[0].source;
    console.log('actThem', actThem);
    var testMapRes = map.getSource(actThem);//取得當前主題資料，可以取的點資訊
    // var themeName = testMapRes._data.themeName;
    var { themeId, themeName } = testMapRes._data;
    $('#them span').html(themeName);
    // $('#them span').html(actThem + ':' + themeName);
    //== 帶入點擊point的當前點資料 ==
    $('#storyCont').show();
    var { title, description, pic, med, tagList } = point.features[0].properties;
    $('#themTit').html(title);
    $('#themCont').html(description);
    pic ? $('#themeImg').show() : $('#themeImg').hide();
    $('.story-theme-img img').attr('src', pic);
    $('.story-theme-img-note').html(med);
    var tagList = JSON.parse(tagList);
    var tagListL = tagList.length;
    if (tagListL != 0) {
        $('#themTag').html('');
        $.each(tagList, function (ind, val) {
            $('#themTag').append('<div data-tagid="' + val.tagId + '" class="tagBtn">#' + val.tagName + '</div>');
        });
    } else {
        $('#themTag').html('');
    }
    //== 新標籤群組 ==(如果當前不同時在更新)
    if (actMapKeyId != themeId) {
        actMapKeyId = themeId;
        changeEventGroup(actMapKeyId);
    }
    //== 更新當前的mark為第幾筆 ==
    var actPoint = point.features[0].properties.eventId;
    var dataList = testMapRes._data.features;
    var dataListL = testMapRes._data.features.length;
    console.log(dataList);
    var indNum = dataList.findIndex(function (item) {
        return item.properties.eventId == actPoint; // 0
    });
    console.log('= 點擊Id在第幾筆 =');
    console.log(indNum);
    actNum = indNum;
    console.log('actNum', actNum);
    //== 給上下則按鈕狀態更新 ==
    actNum == 0 ? $('#preBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#preBtn').removeAttr('style');
    actNum == dataListL - 1 ? $('#nextBtn').css({ 'opacity': '0.35', 'cursor': 'auto', 'pointer-events': 'none' }) : $('#nextBtn').removeAttr('style');
}
//=== 側邊資料顯示動畫 ===
function changeDataAni() {
    var element = document.querySelector('.story-cont');
    // element.classList.add('aaa', 'bbb');
    element.classList.add('change-data');
    element.addEventListener('animationend', () => {
        // do something
        $('.story-cont').removeClass('change-data');
    });
}


//=== 地圖事件群組 ===
function getGroupEventsCn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        var tagData = JSON.parse(data);
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $('#mapEventBox').hide();
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_tw + '</div>');
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_tw + '</div>');
                });
            }
        });
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').hide();
        } else {
            $('.map-tag-box').show();
            $('.map-event').mCustomScrollbar();
        }
        tagBtnMoreShow();
    });
}

function getGroupEventsEn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        console.log(JSON.parse(data));
        var tagData = JSON.parse(data);
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $('#mapEventBox').hide();
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_en + '</div>');
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_en + '</div>');
                });
            }
        });
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').hide();
        } else {
            $('.map-tag-box').show();
            $('.map-event').mCustomScrollbar();
        }
        tagBtnMoreShow();
    });
}

function tagBtnMoreShow() {
    var tagsH = $('#allMapTagInnerBox').outerHeight();
    if (tagsH < 45) {
        $('#moreMapTagBtn').hide();
    } else {
        $('#moreMapTagBtn').show();
    }
}

//=== 只更新事件群組 ===
function changeEventGroup(mapKeyId) {
    console.log('=== 更新事件群組 ===');
    //== 新標籤群組 ==
    var lang = getCookie('CacheLang');
    if (lang == 'zh') {
        var GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + mapKeyId;
        getGroupEventsCn(GroupApiLink);
    } else {
        var GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + mapKeyId;
        getGroupEventsEn(GroupApiLink);
    }
}
//=== ＃號標籤搜尋 ===
function tagSrcPage(tagId) {
    var lang = getCookie('CacheLang');
    var protocol = window.location.protocol;
    var host = window.location.host;
    var domain = protocol + '//' + host;
    var linkPage = window.location.pathname;
    console.log(domain);

    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("target", "_blank");
    console.log(linkPage);
    if (linkPage == '/en/Writer/WriterCont') {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterSrcTagList?Tagid=" + tagId);
    }
    else if (linkPage == '/zh/Writer/WriterCont') {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterSrcTagList?Tagid=" + tagId);
    }
    else {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterStorySrcTagList?Tagid=" + tagId);
    }

    document.body.appendChild(form);
    form.submit();
}