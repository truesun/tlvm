
//=== domain更換 ===
// var domain = "https://whitenight.work";
//==================
var storymap;
var storymap_data = {
    storymap: {
        slides: []
    }
}
$(document).ready(function () {
    //=== 地圖移除goTop ===
    $('#goTop').remove();
    //=== 地圖足跡事件 ===
    //一些JSON設定
    // $.ajaxSetup({
    //     async: false,//異步設定(false是會按照頁面JS執行順序，true是不會同步執行會先跑JS 在AJAX) 
    //     crossDomain: true,
    // });
    var apiLink, GroupApiLink;
    var lang = getCookie('CacheLang');//語系
    var mapKey = getLinkVal('Wid') || getCookie('Key');//足跡主題ID
    var mapEvent = getLinkVal('Eventid');//事件eventid
    if (!mapEvent) {//如果沒有mapEvent 就直接顯示第一筆
        mapEvent = 0;
    }
    if (lang == 'zh') {
        apiLink = '/Api/WebServiceMapCn?Type=1&key=' + mapKey;
        // apiLink = domain + '/WebServiceMapCn.aspx?Type=1&key=1&Tagid=0';
        getMapCn(apiLink, mapEvent);
        //群組事件
        GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + mapKey;;
        getGroupEventsCn(GroupApiLink);
    } else {
        apiLink = '/Api/WebServiceMapEn?Type=1&key=' + mapKey;
        getMapEn(apiLink, mapEvent);
        //群組事件
        GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + mapKey;;
        getGroupEventsEn(GroupApiLink);
    }
    //地圖第一則 goto
    $(document).on('click', '#mapGo', function () {
        storymap.goTo(1);
    });
    //點選事件前往
    $(document).on('click', '.t-even', function () {
        var eventIdAct = $(this).data('eventid');
        var eventInd = storymap_data.storymap.slides.findIndex(function (item) {
            return item.eventId == eventIdAct;
        });
        storymap.goTo(eventInd);
    });

    //Tag點擊 至 搜尋頁
    $(document).on('click', '.tagBtn', function () {
        var tagId = $(this).data('tagid');
        tagSrcPage(tagId);
    });

    //- 標籤開合
    var $moreMapTagBtn = $('#moreMapTagBtn');
    var $MapTagBox = $('#allMapTagBox');
    //預設關起來 標籤收合
    var tagOpen = false;
    $moreMapTagBtn.on('click', function () {
        var tabBtnTxt;
        // var tagOpen = $MapTagBox.hasClass('open');
        // fa-arrow-to-top
        if (tagOpen) {
            $MapTagBox.removeClass('open');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '展開' : tabBtnTxt = 'More';
            $moreMapTagBtn.find('span').html(tabBtnTxt);

            //scroll
            $('.map-all-tag').mCustomScrollbar("destroy");
            tagOpen = false;
        } else {
            $MapTagBox.addClass('open');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '收合' : tabBtnTxt = 'Close';
            $moreMapTagBtn.find('span').html(tabBtnTxt);
            //scroll
            $('.map-all-tag').mCustomScrollbar();
            tagOpen = true;
        }
    });
    //標籤打開事件
    $(document).on('click', '.tag', function () {
        var indEvent = $(this).data('tagid');
        var $eventTagBox = $('#eventsTag' + indEvent);
        var eventTagBoxShow = $eventTagBox.is(':visible');
        if (eventTagBoxShow) {
            $('.tag').removeClass('act');
            $('.map-event').hide();
            $('#mapEventBox').hide();
        } else {
            //標籤ACT
            $('.tag').removeClass('act');
            $(this).addClass('act');
            //事件開起(不一定都有事件??如果沒有要關起來)
            if ($eventTagBox.length) {
                $('.map-event').hide();
                $('#mapEventBox').show();
                $eventTagBox.show();
            } else {
                $('.map-event').hide();
                $('#mapEventBox').hide();
            }
        }
    });
    //地圖事件
    // var storymap_data = {
    //     storymap: {
    //         slides: [
    //             {
    //                 "type": "overview",//第一則導覽才需要加
    //                 "date": "1790-2010",
    //                 "background": {//可以單獨加背景
    //                     "color": "#005C9A",
    //                 },
    //                 "text": {
    //                     "headline": "ORPHAN OF ASIA",
    //                     "text": "<p>吳濁流著，原名《胡志明》，以日文書寫。<br />「胡志明」原本是主角的名稱，後來因為越南戰後的革命領袖也叫胡志明，為避免被誤會，主角改為胡太明，書名改為《亞細亞的孤兒》。</p><div id='mapGo' class='map-go-btn'>ORPHAN OF ASIA<div>"
    //                 },
    //                 "media": {
    //                     "url": "img/story01.jpg",
    //                     "credit": "資料來源：國立臺灣文學館館藏",
    //                     //- caption: "Mean US_Mean_Center_of_Population_1790-2010"
    //                 }
    //             },
    //             {// required; array of slide objects (see below)
    //                 //=== 官網增加 ===
    //                 background: {//可以單獨加背景
    //                     color: "#005C9A",
    //                     //- opacity: 0.8,
    //                     //- url:''
    //                 },
    //                 data: '',
    //                 //=== 官網增加 END===
    //                 location: {// required for all slides except "overview" slide
    //                     //=== 官網增加 ===
    //                     //- custom_icon: true,
    //                     //- icon:"/Demo/Content/images/Storymap/residencelamontagne-hi.png",
    //                     //- icon: "http://maps.gstatic.com/intl/en_us/mapfiles/ms/micons/blue-pushpin.png",
    //                     icon: "/img/icon_map.png",
    //                     acticon: "/img/icon_map_act.png",
    //                     iconSize: [35, 35],
    //                     zoom: 17,
    //                     lat: 25.033493,// latitude of point on map
    //                     lon: 121.564101,// longitude of point on map
    //                     line: true
    //                 },
    //                 text: {// optional if media present
    //                     date: "2020/12/12",
    //                     headline: "ORPHAN OF ASIA",//string
    //                     text: '<p>吳濁流著，原名《胡志明》，以日文書寫。<br />「胡志明」原本是主角的名稱，後來因為越南戰後的革命領袖也叫胡志明，為避免被誤會，主角改為胡太明，書名改為《亞細亞的孤兒》。</p>', //string may contain HTML markup
    //                     maptxt: '吳濁流著，原名《胡志明》，以日文書寫。<br />「胡志明」原本是主角的名稱，後來因為越南戰後的革命領袖也叫胡志明，為避免被誤會，主角改為胡太明，書名改為《亞細亞的孤兒》。',
    //                     id: 1073,
    //                 },
    //                 media: {// optional if text present
    //                     url: "img/his05.jpg",// url for featured media
    //                     //- caption: "看我ㄓㄜ這 我是哪裡？",// string optional; brief explanation of media content
    //                     credit: "資料來源：國立臺灣文學館館藏",// optional; creator of media content
    //                 }
    //             },
    //             {// required; array of slide objects (see below)
    //                 background: {//可以單獨加背景
    //                     color: "#005C9A",
    //                     opacity: 0.8,
    //                     url: ''
    //                 },
    //                 location: {// required for all slides except "overview" slide
    //                     //=== 官網增加 ===
    //                     //- icon:"/Demo/Content/images/Storymap/residencelamontagne-hi.png",
    //                     name: "?????????",
    //                     icon: "/img/icon_map.png",
    //                     acticon: "/img/icon_map_act.png",
    //                     iconSize: [35, 35],
    //                     zoom: 17,
    //                     //=== 官網增加 END===
    //                     lat: 25.033493,// latitude of point on map
    //                     lon: 121.570101, // longitude of point on map
    //                     line: true
    //                 },
    //                 text: {// optional if media present
    //                     headline: "1925年出生",//string
    //                     text: '<p>吳濁流著，原名《胡志明》，以日文書寫。<br />「胡志明」原本是主角的名稱，後來因為越南戰後的革命領袖也叫胡志明，為避免被誤會，主角改為胡太明，書名改為《亞細亞的孤兒》。</p><div class="his-timeline-tag-box"><a href="###">#臺灣文學</a><a href="###">#文化</a><a href="###">#臺灣</a></div>', //string may contain HTML markup
    //                     maptxt: '吳濁流著，原名《胡志明》，以日文書寫。<br />「胡志明」原本是主角的名稱，後來因為越南戰後的革命領袖也叫胡志明，為避免被誤會，主角改為胡太明，書名改為《亞細亞的孤兒》。',
    //                     id: 1073,
    //                 },
    //                 media: {// optional if text present
    //                     url: "img/his05.jpg",// url for featured media
    //                     //- caption: "看我ㄓㄜ這 我是哪裡？",// string optional; brief explanation of media content
    //                     credit: "資料來源：國立臺灣文學館館藏",// optional; creator of media content
    //                 }
    //             },
    //             {// required; array of slide objects (see below)
    //                 //=== 官網增加 ===
    //                 background: {//可以單獨加背景
    //                     color: "#005C9A",
    //                     opacity: 0.8,
    //                     url: ''
    //                 },
    //                 location: {// required for all slides except "overview" slide
    //                     icon: "/img/icon_map.png",
    //                     acticon: "/img/icon_map_act.png",
    //                     iconSize: [35, 35],
    //                     zoom: 17,
    //                     //=== 官網增加 END===
    //                     lat: 22.692992,// latitude of point on map
    //                     lon: 121.018111, // longitude of point on map
    //                     line: true
    //                 },
    //                 text: {// optional if media present
    //                     headline: "知本洗溫泉",//string
    //                     text: '<p>吳濁流著，原名《胡志明》，以日文書寫。<br />「胡志明」原本是主角的名稱，後來因為越南戰後的革命領袖也叫胡志明，為避免被誤會，主角改為胡太明，書名改為《亞細亞的孤兒》。</p><div class="his-timeline-tag-box"><a href="###">#臺灣文學</a><a href="###">#文化</a><a href="###">#臺灣</a></div>', //string may contain HTML markup
    //                     maptxt: '吳濁流著，原名《胡志明》，以日文書寫。<br />「胡志明」原本是主角的名稱，後來因為越南戰後的革命領袖也叫胡志明，為避免被誤會，主角改為胡太明，書名改為《亞細亞的孤兒》。',
    //                 },
    //                 media: {// optional if text present
    //                     url: "img/his05.jpg",// url for featured media
    //                     //- caption: "看我ㄓㄜ這 我是哪裡？",// string optional; brief explanation of media content
    //                     credit: "資料來源：國立臺灣文學館館藏",// optional; creator of media content
    //                 }
    //             }
    //         ]
    //     }
    // }
    // var storymap_options = {
    //     map_type: "osm:standard",// required
    //     language: "zh-tw",
    //     map_as_image: false,// required
    //     map_subdomains: "",// optional
    //     use_custom_markers: true,//是否使用自定義圖片 如果使用 VCO.MapMarker.Leaflet.js 可以詳細設定
    //     map_popup: true,//開啟地圖 popup
    //     //開啟地圖 popup 請至 VCO.MapMarker.Leaflet.js 裡面加入使用
    //     calculate_zoom: false//預設會是true 會自動幫忙縮放 詳細解釋下面：
    //     //calculate_zoom是可選的。StoryMapJS將為您的每張幻燈片計算最佳縮放級別，以使上一個和下一個地圖點均可見。
    //     //如果要控制每個幻燈片的縮放級別，請將calculate_zoom選項設置為false，然後確保zoom為每個幻燈片設置屬性。
    //     // zoom 設定在每個資料中的 location 裡
    //     //- "line_color": "#005C9A", //"#DA0000" OK
    // };

    // storymap = new VCO.StoryMap('MapArea', storymap_data, storymap_options);

    // window.onresize = function (event) {
    //     storymap.updateDisplay(); // this isn't automatic
    // }
});
$(window).resize(function () {
    //標籤判斷一行時沒有展開按鈕
    tagBtnMoreShow();
});
//=== 地圖 API 中文===
function getMapCn(apiLink, mapEvent) {
    $.get(apiLink, function (data) {
        console.log(JSON.parse(data));
        var mapData = JSON.parse(data);
        var firstMap = mapData.splice(0, 1);
        firstMap = firstMap[0];
        var firstTagListTxt;
        //=== 是否有標籤 ===
        var firstTagListL = JSON.parse(firstMap.data.TagidArray);
        if (firstTagListL.length) {
            var firstTagList = [];
            $.each(firstTagListL, function (ind, val) {
                firstTagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_tw + '</a>');
            });
            firstTagListTxt = '<div class="his-timeline-tag-box">' + firstTagList.join() + '</div>';
        } else {
            firstTagListTxt = '';
        }

        var firstMapData = {
            "type": "overview",//第一則導覽才需要加
            "eventId": firstMap.Eventid,
            "background": {
                "color": "#005C9A",
            },
            "text": {
                "headline": firstMap.Name_tw,
                "text": '<p>' + firstMap.Content_tw + '</p>' + firstTagListTxt + '<div id="mapGo" class="map-go-btn">' + firstMap.Name_tw + '<div></div></div>'
            },
            "media": {
                "url": firstMap.pic_tw,
                "credit": firstMap.med_tw,
            }
        };
        storymap_data.storymap.slides.push(firstMapData);
        //第二筆之後
        $.each(mapData, function (ind, val) {
            var mapListData, tagListL, tagListTxt;
            //=== 是否有標籤 ===
            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_tw + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }
            mapListData = {
                "eventId": val.Eventid,
                "background": {
                    "color": "#005C9A",
                },
                "location": {
                    "icon": "/img/icon_map.png",
                    "acticon": "/img/icon_map_act.png",
                    "iconSize": [35, 35],
                    "zoom": 17,
                    "lat": Number(val.latitude),
                    "lon": Number(val.longitude),
                },
                "text": {// optional if media present
                    "headline": val.Name_tw,//string
                    "text": '<p>' + val.Content_tw + '</p>' + tagListTxt, //string may contain HTML markup
                    "maptxt": val.Content_tw,
                },
                "media": {// optional if text present
                    "url": val.pic_tw,// url for featured media
                    "credit": val.med_tw,// optional; creator of media content
                }
            }
            storymap_data.storymap.slides.push(mapListData);
        });
    });
    //如果有搜尋事件進來 會需要找陣列位置
    var slideInd = storymap_data.storymap.slides.findIndex(function (item) {
        return item.eventId == mapEvent;
    });
    if (slideInd == '-1') {//如果搜不到 就會直接回到第一個
        slideInd = 0;
    }

    var storymap_options = {
        map_type: "osm:standard",// required
        language: "zh-tw",
        map_as_image: false,// required
        map_subdomains: "",// optional
        use_custom_markers: true,//是否使用自定義圖片 如果使用 VCO.MapMarker.Leaflet.js 可以詳細設定
        map_popup: true,//開啟地圖 popup
        start_at_slide: slideInd,//slide 起始位置
        map_mini: false,//控制地圖放大縮小按鈕 false 是打開
        //開啟地圖 popup 請至 VCO.MapMarker.Leaflet.js 裡面加入使用
        calculate_zoom: false//預設會是true 會自動幫忙縮放 詳細解釋下面：
        //calculate_zoom是可選的。StoryMapJS將為您的每張幻燈片計算最佳縮放級別，以使上一個和下一個地圖點均可見。
        //如果要控制每個幻燈片的縮放級別，請將calculate_zoom選項設置為false，然後確保zoom為每個幻燈片設置屬性。
        // zoom 設定在每個資料中的 location 裡
        //- "line_color": "#005C9A", //"#DA0000" OK
    };

    storymap = new VCO.StoryMap('MapArea', storymap_data, storymap_options);

    window.onresize = function (event) {
        storymap.updateDisplay(); // this isn't automatic
    }
}

//=== 地圖 API 英文 ===
function getMapEn(apiLink, mapEvent) {
    $.get(apiLink, function (data) {
        console.log(JSON.parse(data));
        var mapData = JSON.parse(data);
        var firstMap = mapData.splice(0, 1);
        firstMap = firstMap[0];
        var firstTagListTxt;
        //=== 是否有標籤 ===
        var firstTagListL = JSON.parse(firstMap.data.TagidArray);
        if (firstTagListL.length) {
            var firstTagList = [];
            $.each(firstTagListL, function (ind, val) {
                firstTagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_en + '</a>');
            });
            firstTagListTxt = '<div class="his-timeline-tag-box">' + firstTagList.join() + '</div>';
        } else {
            firstTagListTxt = '';
        }

        var firstMapData = {
            "type": "overview",//第一則導覽才需要加
            "eventId": firstMap.Eventid,
            "background": {
                "color": "#005C9A",
            },
            "text": {
                "headline": firstMap.Name_en,
                "text": '<p>' + firstMap.Content_en + '</p>' + firstTagListTxt + '<div id="mapGo" class="map-go-btn">' + firstMap.Name_en + '<div></div></div>'
            },
            "media": {
                "url": firstMap.pic_en,
                "credit": firstMap.med_en,
            }
        };
        storymap_data.storymap.slides.push(firstMapData);

        //第二筆之後
        $.each(mapData, function (ind, val) {
            var mapListData, tagListL, tagListTxt;
            //=== 是否有標籤 ===
            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_en + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }
            mapListData = {
                "eventId": val.Eventid,
                "background": {
                    "color": "#005C9A",
                },
                "location": {
                    "icon": "/img/icon_map.png",
                    "acticon": "/img/icon_map_act.png",
                    "iconSize": [35, 35],
                    "zoom": 17,
                    "lat": Number(val.latitude),
                    "lon": Number(val.longitude),
                },
                "text": {// optional if media present
                    "headline": val.Name_en,//string
                    "text": '<p>' + val.Content_en + '</p>' + tagListTxt, //string may contain HTML markup
                    "maptxt": val.Content_en,
                },
                "media": {// optional if text present
                    "url": val.pic_en,// url for featured media
                    "credit": val.med_en,// optional; creator of media content
                }
            }
            storymap_data.storymap.slides.push(mapListData);
        });
    });

    //如果有搜尋事件進來 會需要找陣列位置
    var slideInd = storymap_data.storymap.slides.findIndex(function (item) {
        return item.eventId == mapEvent;
    });
    if (slideInd == '-1') {//如果搜不到 就會直接回到第一個
        slideInd = 0;
    }

    var storymap_options = {
        map_type: "osm:standard",// required
        language: "en",
        map_as_image: false,// required
        map_subdomains: "",// optional
        use_custom_markers: true,//是否使用自定義圖片 如果使用 VCO.MapMarker.Leaflet.js 可以詳細設定
        map_popup: true,//開啟地圖 popup
        start_at_slide: slideInd,//slide 起始位置
        map_mini: false,//控制地圖放大縮小按鈕 false 是打開
        //開啟地圖 popup 請至 VCO.MapMarker.Leaflet.js 裡面加入使用
        calculate_zoom: false//預設會是true 會自動幫忙縮放 詳細解釋下面：
        //calculate_zoom是可選的。StoryMapJS將為您的每張幻燈片計算最佳縮放級別，以使上一個和下一個地圖點均可見。
        //如果要控制每個幻燈片的縮放級別，請將calculate_zoom選項設置為false，然後確保zoom為每個幻燈片設置屬性。
        // zoom 設定在每個資料中的 location 裡
        //- "line_color": "#005C9A", //"#DA0000" OK
    };

    storymap = new VCO.StoryMap('MapArea', storymap_data, storymap_options);

    window.onresize = function (event) {
        storymap.updateDisplay(); // this isn't automatic
    }
}
//=== 地圖群組事件 中文===
function getGroupEventsCn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        // console.log(data);
        console.log(JSON.parse(data));
        var tagData = JSON.parse(data);
        //後台有資料先清空
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                //如果群組內有事件才會列出
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_tw + '</div>');
                //如果群組內事件的列表BOX
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                //放入事件
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_tw + '</div>');
                });
            }
        });
        //如果都群組內都沒有事件的話
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').remove();
        } else {
            $('.map-event').mCustomScrollbar();
        }
        //標籤判斷一行時沒有展開按鈕
        tagBtnMoreShow();
    });
}

//=== 地圖群組事件 英文===
function getGroupEventsEn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        // console.log(data);
        console.log(JSON.parse(data));
        var tagData = JSON.parse(data);
        //後台有資料先清空
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                //如果群組內有事件才會列出
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_en + '</div>');
                //如果群組內事件的列表BOX
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                //放入事件
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_en + '</div>');
                });
            }
        });
        //如果都群組內都沒有事件的話
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').remove();
        } else {
            $('.map-event').mCustomScrollbar();
        }
        //標籤判斷一行時沒有展開按鈕
        tagBtnMoreShow();
    });
}

//標籤判斷一行時沒有展開按鈕
function tagBtnMoreShow() {
    var tagsH = $('#allMapTagInnerBox').outerHeight();
    if (tagsH < 45) {
        $('#moreMapTagBtn').hide();
    } else {
        $('#moreMapTagBtn').show();
    }
}

//Tag Src - Tag標籤
function tagSrcPage(tagId) {
    var protocol = window.location.protocol;
    var host = window.location.host;
    var domain = protocol + '//' + host;
    var linkPage = window.location.pathname;
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("target", "_blank");//另開新頁
    if (linkPage == '/Writer/WriterCont') {
        form.setAttribute("action", domain + "/Writer/WriterSrcTagList?Tagid=" + tagId);
    } else {
        form.setAttribute("action", domain + "/Writer/WriterStorySrcTagList?Tagid=" + tagId);
    }

    document.body.appendChild(form);    // Not entirely sure if this is necessary
    form.submit();
}