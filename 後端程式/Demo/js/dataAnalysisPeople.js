var columnTable, pie01, pie02, pie03, pie04;
$(function () {
    var validator = $("#form").validate({
        submitHandler: function (form) {
            alert("完成");
            form.submit();
        }
    });

    //時間選擇器
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    console.log(yesterday);
    //=== 日期格式 ===dateRange
    $("#dateRange").datepicker({
        dateFormat: "yyyy/mm/dd",
        autoClose: true,
        position: "bottom right",
        language: 'en',
        range: true,
        multipleDatesSeparator: ' ~ ',
        maxDate: yesterday,
        // minDate: threeMonth,
        onSelect: (fd, d, picker) => {
            //- this.date = this.$("#date").val();
        },
    });
    // var currentDate = new Date();
    $("#dateRange").data('datepicker').selectDate(yesterday);
    // $('.datepicker-here').datepicker({
    //   language: "en",
    //   position: "bottom right",
    //   //minView: 'months',
    //   //dateFormat: "yyyy/mm",
    //   autoClose: true
    // });
    //取消因為滾動不會消失的日期選取器
    $('.main').scroll(function () {
        $('.datepickers-container').find('.datepicker').removeClass('active');
        $('.datepickers-container').find('.datepicker').css({ 'left': '-100000px' });
        $('#dateRange').blur();
    });


    //=== 取得API ===
    //== 取的攝影機列表 ==
    // $('#type').html('');
    var apiLink2 = '/api/overview?Type=2'
    // var cameraList = [];
    $.get(apiLink2, function (data) {
        var aipData = JSON.parse(data);
        // aipData.shift();
        //== 可能多加攝影機而改變順序 所以用id判斷 ==
        $.each(aipData, function (ind, val) {
            if (val.CameraUID == "75effc8acfc74397bc694b537b0829f5") {
                var { CameraUID, CameraName } = val;
                $('#type').append('<option value="' + CameraUID + '">' + CameraName + '</option>');
            }
        });
        // var oneCam = aipData.splice(1, 1);
        // console.log(aipData);
        // var { CameraUID, CameraName } = oneCam[0];
        //== 只留客情 ==
        // $('#type').html('');
        // $('#type').append('<option value="' + CameraUID + '">' + CameraName + '</option>');
        //== 多個 ==
        // $.each(aipData, function (ind, val) {
        //     var { CameraUID, CameraName } = val;
        //     // console.log(CameraUID);
        //     // console.log(CameraName);
        //     $('#type').append('<option value="' + CameraUID + '">' + CameraName + '</option>');
        //     //== 複數 ==
        //     // cameraList.push({ CameraUID, CameraName });
        // });
    });

    //== 取的客群資料 ==
    srcChartStart();
    $('#srcBtn').on('click', function () {
        //= 搜尋圖表 =
        srcChartStart();
    });

    //=== Export button ===
    $('#exportBtn').click(function () {
        Highcharts.exportCharts([pie01]);
        Highcharts.exportCharts([pie02]);
        Highcharts.exportCharts([pie03]);
        Highcharts.exportCharts([pie04]);
        Highcharts.exportCharts([columnTable]);

        // pie01.exportChart();
        // setTimeout(function () {
        //     pie02.exportChart();
        // }, 5000);
        // setTimeout(function () {
        //     pie03.exportChart();
        // }, 10000);
        // setTimeout(function () {
        //     pie04.exportChart();
        // }, 15000);
        // setTimeout(function () {
        //     columnTable.exportChart();
        // }, 20000);

        // Highcharts.charts.forEach(function (chart, i) {
        //     console.log(chart);
        //     // Highcharts.exportCharts([chart]);
        //     if (i === 1) {
        //         setTimeout(function () {
        //             chart.exportChart();
        //         }, 5000);
        //         // console.log(chart);
        //         // console.log(i);
        //     } else if (i === 2) {
        //         setTimeout(function () {
        //             chart.exportChart();
        //         }, 10000);
        //     } else if (i === 3) {
        //         setTimeout(function () {
        //             chart.exportChart();
        //         }, 15000);
        //     } else if (i === 4) {
        //         setTimeout(function () {
        //             chart.exportChart();
        //         }, 20000);
        //     } else {
        //         chart.exportChart();
        //     }
        // });
    });
    //=== csvDownload botton ===
    $('#csvBtn').click(function () {
        pie01.downloadCSV();
        pie02.downloadCSV();
        pie03.downloadCSV();
        pie04.downloadCSV();
        columnTable.downloadCSV();

        // setTimeout(function () {
        //     pie02.downloadCSV();
        // }, 5000);
        // setTimeout(function () {
        //     pie03.downloadCSV();
        // }, 10000);
        // setTimeout(function () {
        //     pie04.downloadCSV();
        // }, 15000);
        // setTimeout(function () {
        //     columnTable.downloadCSV();
        // }, 20000);

        // Highcharts.charts.forEach(function (chart, i) {
        //     if (i === 1) {
        //         setTimeout(function () {
        //             chart.downloadCSV();
        //         }, 5000);
        //         // console.log(chart);
        //         // console.log(i);
        //     } else if (i === 2) {
        //         setTimeout(function () {
        //             chart.downloadCSV();
        //         }, 10000);
        //     } else if (i === 3) {
        //         setTimeout(function () {
        //             chart.downloadCSV();
        //         }, 15000);
        //     } else if (i === 4) {
        //         setTimeout(function () {
        //             chart.downloadCSV();
        //         }, 20000);
        //     } else {
        //         chart.downloadCSV();
        //     }
        // });
    });
    // chart.downloadCSV()
});
//=== 啟動搜尋 ===
function srcChartStart() {
    // var type = $('#type').val();//原為攝影機ID
    var type = $('#range').val();//改為統計單位下拉單
    var dateRange = $('#dateRange').val();
    var sday, eday, srcDayTxt;
    var dateRangeL = dateRange.split(' ~ ');
    sday = dateRangeL[0];
    dateRangeL.length > 1 ? eday = dateRangeL[1] : eday = dateRangeL[0];
    sday = sday.split('/').join('-');
    eday = eday.split('/').join('-');
    console.log('type:', type);
    console.log('dateRange:', sday, eday);
    dateRangeL.length > 1 ? srcDayTxt = dateRangeL[0] + ' - ' + dateRangeL[1] : srcDayTxt = dateRangeL[0];
    srcChart(type, sday, eday, srcDayTxt);
}
//=== 取得圖表API 資料 ===
// Highcharts.setOptions({
//     lang: {
//         thousandsSep: ','
//     }
// });
function srcChart(type, sday, eday, srcDayTxt) {
    var apiLink3 = '/api/DataAnalysispeople?Type=' + type + '&Sday=' + sday + '&Eday=' + eday;
    $.get(apiLink3, function (data) {
        var aipData = JSON.parse(data);
        console.log(aipData);
        var { F, FTotal, M, MTotal } = aipData[0];
        var MFcolor = ['#88cff9', '#f98888'];
        var MFdata = [];
        MFdata.push({ name: "男性", y: Number(Number(M).toFixed(2)), num: Number(MTotal), numTxt: FormatNumber(Number(MTotal)) });
        MFdata.push({ name: "女性", y: Number(Number(F).toFixed(2)), num: Number(FTotal), numTxt: FormatNumber(Number(FTotal)) });
        pieChart('container5', 'allTotal', MFcolor, '客群性別分佈佔比', srcDayTxt, MFdata, 1);
        var { MA, MATotal, MB, MBTotal, MC, MCTotal, MD, MDTotal, ME, METotal, MF, MFTotal, MG, MGTotal } = aipData[0];
        var Mcolor = ['#60C4FF', '#88CFF9', '#99D9FF', '#A4DDFF', '#B0E2FF', '#C6EAFF', '#D8F0FF'];
        var Mdata = [];
        Mdata.push({ name: "(0-17歲)", y: Number(Number(MA).toFixed(2)), num: Number(MATotal), numTxt: FormatNumber(Number(MATotal)) });
        Mdata.push({ name: "(18-24歲)", y: Number(Number(MB).toFixed(2)), num: Number(MBTotal), numTxt: FormatNumber(Number(MBTotal)) });
        Mdata.push({ name: "(25-34歲)", y: Number(Number(MC).toFixed(2)), num: Number(MCTotal), numTxt: FormatNumber(Number(MCTotal)) });
        Mdata.push({ name: "(35-44歲)", y: Number(Number(MD).toFixed(2)), num: Number(MDTotal), numTxt: FormatNumber(Number(MDTotal)) });
        Mdata.push({ name: "(45-54歲)", y: Number(Number(ME).toFixed(2)), num: Number(METotal), numTxt: FormatNumber(Number(METotal)) });
        Mdata.push({ name: "(55-64歲)", y: Number(Number(MF).toFixed(2)), num: Number(MFTotal), numTxt: FormatNumber(Number(MFTotal)) });
        Mdata.push({ name: "(65+歲)", y: Number(Number(MG).toFixed(2)), num: Number(MGTotal), numTxt: FormatNumber(Number(MGTotal)) });
        pieChart('container', 'maleTotal', Mcolor, '男性客群年齡分佈佔比', srcDayTxt, Mdata, 2);
        var { FA, FATotal, FB, FBTotal, FC, FCTotal, FD, FDTotal, FE, FETotal, FF, FFTotal, FG, FGTotal } = aipData[0];
        var Fcolor = ['#FF7676', '#F98888', '#F79C9C', '#FFB3B3', '#FFC0C0', '#FFD4D4', '#FFE3E3'];
        var Fdata = [];
        Fdata.push({ name: "(0-17歲)", y: Number(Number(FA).toFixed(2)), num: Number(FATotal), numTxt: FormatNumber(Number(FATotal)) });
        Fdata.push({ name: "(18-24歲)", y: Number(Number(FB).toFixed(2)), num: Number(FBTotal), numTxt: FormatNumber(Number(FBTotal)) });
        Fdata.push({ name: "(25-34歲)", y: Number(Number(FC).toFixed(2)), num: Number(FCTotal), numTxt: FormatNumber(Number(FCTotal)) });
        Fdata.push({ name: "(35-44歲)", y: Number(Number(FD).toFixed(2)), num: Number(FDTotal), numTxt: FormatNumber(Number(FDTotal)) });
        Fdata.push({ name: "(45-54歲)", y: Number(Number(FE).toFixed(2)), num: Number(FETotal), numTxt: FormatNumber(Number(FETotal)) });
        Fdata.push({ name: "(55-64歲)", y: Number(Number(FF).toFixed(2)), num: Number(FFTotal), numTxt: FormatNumber(Number(FFTotal)) });
        Fdata.push({ name: "(65+歲)", y: Number(Number(FG).toFixed(2)), num: Number(FGTotal), numTxt: FormatNumber(Number(FGTotal)) });
        pieChart('container2', 'femaleTotal', Fcolor, '女性客群年齡分佈佔比', srcDayTxt, Fdata, 3);
        var { TA, TATotal, TB, TBTotal, TC, TCTotal, TD, TDTotal, TE, TETotal, TF, TFTotal, TG, TGTotal } = aipData[0];
        var Tcolor = ['#FFAD88', '#B978FA', '#F88D8D', '#FFB3B3', '#9ABCEF', '#F3E15D', '#B2C15A'];
        var Tdata = [];
        Tdata.push({ name: "(0-17歲)", y: Number(Number(TA).toFixed(2)), num: Number(TATotal), numTxt: FormatNumber(Number(TATotal)) });
        Tdata.push({ name: "(18-24歲)", y: Number(Number(TB).toFixed(2)), num: Number(TBTotal), numTxt: FormatNumber(Number(TBTotal)) });
        Tdata.push({ name: "(25-34歲)", y: Number(Number(TC).toFixed(2)), num: Number(TCTotal), numTxt: FormatNumber(Number(TCTotal)) });
        Tdata.push({ name: "(35-44歲)", y: Number(Number(TD).toFixed(2)), num: Number(TDTotal), numTxt: FormatNumber(Number(TDTotal)) });
        Tdata.push({ name: "(45-54歲)", y: Number(Number(TE).toFixed(2)), num: Number(TETotal), numTxt: FormatNumber(Number(TETotal)) });
        Tdata.push({ name: "(55-64歲)", y: Number(Number(TF).toFixed(2)), num: Number(TFTotal), numTxt: FormatNumber(Number(TFTotal)) });
        Tdata.push({ name: "(65+歲)", y: Number(Number(TG).toFixed(2)), num: Number(TGTotal), numTxt: FormatNumber(Number(TGTotal)) });
        pieChart('container4', 'ageTotal', Tcolor, '客群年齡分佈佔比', srcDayTxt, Tdata, 4);
        //直條圖
        var columnData = JSON.parse(aipData[0].data.Array);
        console.log(columnData);
        var columnXdata = [];//日期資料
        var columnYdata = [];//Ｙ軸資料
        var columnMYdata = [];//男生日資料
        var columnFYdata = [];//女生日資料
        $.each(columnData, function (ind, val) {
            var { timestamp, CountM, CountF, CountTotal } = val;
            var timestampTxt = timestamp.split('-').join('/');
            columnXdata.push(timestampTxt);
            columnMYdata.push(Number(CountM));
            columnFYdata.push(Number(CountF));
        });
        columnYdata.push({ name: '男性', data: columnMYdata });
        columnYdata.push({ name: '女性', data: columnFYdata })
        columnChart('container3', '男女人數分佈統計', srcDayTxt, columnXdata, columnYdata);
    });
}

//=== 千分位 ===
function FormatNumber(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
    return arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
}
//=== 圓餅圖表 ===
function pieChart(box, totalId, colors, tit, time, data, number) {
    // Highcharts.setOptions({
    //     lang: {
    //         thousandsSep: ','
    //     }
    // });
    var cont = Highcharts.chart(box, {
        chart: {
            type: 'pie',
            //= 可以顯示在圖表上總人口數 =
            events: {
                load: function (event) {
                    // console.log('===RRR===');
                    // console.log(event);
                    var total = 0;
                    for (var i = 0, len = this.series[0].userOptions.data.length; i < len; i++) {
                        total += this.series[0].userOptions.data[i].num;
                    }
                    var text = this.renderer.text(
                        '總人數: ' + total,
                        this.plotLeft,
                        this.plotTop - 20
                    ).attr({
                        zIndex: 5,
                        id: totalId
                    }).add() // write it to the upper left hand corner
                },
                //== 點選點點更新 ==
                // render: function (event) {
                //     console.log('===喵喵喵===');
                //     // console.log(event);
                //     console.log(this);
                //     console.log(this.legend.allItems);
                //     var legendTotal = 0;
                //     $.each(this.legend.allItems, function (ind, val) {
                //         console.log(val.visible);
                //         if (val.visible) {
                //             legendTotal += val.num;
                //         }
                //     });
                //     $('#' + totalId).html('總人數: ' + FormatNumber(legendTotal));
                // }
            }
        },
        colors: colors,
        title: {
            text: tit,
        },
        subtitle: {
            text: time,
        },

        accessibility: {
            announceNewData: {
                enabled: true
            },
            point: {
                valueSuffix: '%'
            }
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                //= 顯示區間定義選則點點 =
                showInLegend: true
            },
            //= 圖表外顯示數據 =
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.percentage:.2f}%({point.num}人)'
                }
            },
        },
        //== 右下網址顯示 ==
        credits: {
            enabled: false
        },
        //== 指過去顯示的詳細資料 ==
        tooltip: {
            headerFormat: '',
            // headerFormat: '<span style="font-size:11px;color:{point.color};">{series.name}</span><br>',
            // headerFormat: '<span style="font-size:11px">{series.name}</span><br>',//標題
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.percentage:.2f}%({point.num}人)</b>'//內文
        },
        //右上角功能按鈕
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"],
                }
            },
            //輸出圖表檔案名稱
            filename: tit,
        },

        series: [
            {
                name: "佔比(%)",//這裡要下名稱 下載csv時才有標題爛位 佔比(%)
                colorByPoint: true,
                data: data,
            }
        ]
    });
    //== 為了要匯出圖片和csv 需要帶入一個宣告 ==
    if (number == 1) {
        pie01 = cont;
    } else if (number == 2) {
        pie02 = cont;
    } else if (number == 3) {
        pie03 = cont;
    } else if (number == 4) {
        pie04 = cont;
    }
}

// Create the chart
// Highcharts.setOptions({
//     colors: ['#60C4FF', '#88CFF9', '#99D9FF', '#A4DDFF', '#B0E2FF', '#C6EAFF', '#D8F0FF']
// });
// Highcharts.chart('container', {
//     chart: {
//         type: 'pie'
//     },
//     title: {
//         text: '男性客群年齡分佈佔比'
//     },
//     subtitle: {
//         text: '2021-05-10 - 2021/05/14'
//     },

//     accessibility: {
//         announceNewData: {
//             enabled: true
//         },
//         point: {
//             valueSuffix: '%'
//         }
//     },

//     plotOptions: {
//         series: {
//             dataLabels: {
//                 enabled: true,
//                 format: '{point.name}: {point.y:.1f}%'
//             }
//         }
//     },
//     credits: {
//         enabled: false
//     },

//     tooltip: {
//         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b>'
//     },

//     series: [
//         {
//             name: "",
//             colorByPoint: true,
//             data: [
//                 {
//                     name: "(35-44歲)",
//                     y: 32.56,
//                     drilldown: "Chrome"
//                 },
//                 {
//                     name: "(45-54歲)",
//                     y: 27.57,
//                     drilldown: "Firefox"
//                 },
//                 {
//                     name: "(25-34歲)",
//                     y: 12.23,
//                     drilldown: "Internet Explorer"
//                 },
//                 {
//                     name: "(18-24歲)",
//                     y: 8.58,
//                     drilldown: "Safari"
//                 },
//                 {
//                     name: "(55-64歲)",
//                     y: 9.02,
//                     drilldown: "Edge"
//                 },
//                 {
//                     name: "(65+歲)",
//                     y: 2.42,
//                     drilldown: "Opera"
//                 },
//                 {
//                     name: "(0-17歲)",
//                     y: 7.62,
//                     drilldown: null
//                 }
//             ]
//         }
//     ]
// });
//女性
// Create the chart
// Highcharts.setOptions({
//     colors: ['#FF7676', '#F98888', '#F79C9C', '#FFB3B3', '#FFC0C0', '#FFD4D4', '#FFE3E3']
// });
// //== 點選圖表的圖示（不是點點） ==
// // Highcharts.addEvent(Highcharts.Point, 'click', function () {
// //     console.log(this)
// //     console.log(this.a11yProxyElement.ariaHidden)//aria-hidden
// //     // console.log(this.a11yProxyElement.aria('hidden'))
// // });
// Highcharts.chart('container2', {
//     chart: {
//         type: 'pie',
//         //= 可以顯示在圖表上總人口數 =
//         // events: {
//         //     load: function (event) {
//         //         // console.log('===RRR===');
//         //         // console.log(event);
//         //         var total = 0;
//         //         for (var i = 0, len = this.series[0].userOptions.data.length; i < len; i++) {
//         //             total += this.series[0].userOptions.data[i].num;
//         //         }
//         //         var text = this.renderer.text(
//         //             '總人數: ' + total,
//         //             this.plotLeft,
//         //             this.plotTop - 20
//         //         ).attr({
//         //             zIndex: 5,
//         //             id: 'femaleTotal'
//         //         }).add() // write it to the upper left hand corner
//         //     },
//         //     //== 點選點點更新 ==
//         //     render: function (event) {
//         //         console.log('===喵喵喵===');
//         //         // console.log(event);
//         //         console.log(this);
//         //         console.log(this.legend.allItems);
//         //         var legendTotal = 0;
//         //         $.each(this.legend.allItems, function (ind, val) {
//         //             console.log(val.visible);
//         //             if (val.visible) {
//         //                 legendTotal += val.num;
//         //             }
//         //         });
//         //         $('#femaleTotal').html('總人數: ' + legendTotal);
//         //     }
//         // }
//     },
//     title: {
//         text: '女性客群年齡分佈佔比'
//     },
//     subtitle: {
//         text: '2021-05-10 - 2021/05/14'
//     },

//     accessibility: {
//         announceNewData: {
//             enabled: true
//         },
//         point: {
//             valueSuffix: '%'
//         }
//     },
//     //= 一樣可以設定區塊定義位置 =
//     // legend: {
//     //     // layout: 'vertical',//水平對齊
//     //     align: 'right',// 圖表點點資料左右
//     //     verticalAlign: 'top',
//     // },

//     plotOptions: {
//         pie: {
//             allowPointSelect: true,
//             cursor: 'pointer',
//             // //圖表上顯示數據
//             // dataLabels: {
//             //     enabled: true,
//             //     format: '<b>{point.name}</b><br>{point.percentage:.1f} % ({point.num}人)',
//             //     distance: -50,
//             //     filter: {
//             //         property: 'percentage',
//             //         operator: '>',
//             //         value: 4
//             //     }
//             // },
//             //= 顯示區間定義選則點點 =
//             showInLegend: true
//         },
//         //= 圖表外顯示數據 =
//         series: {
//             dataLabels: {
//                 enabled: true,
//                 format: '{point.name}: {point.percentage:.2f}%({point.num}人)'
//                 //{point.percentage:.1f}
//                 // format: '{point.name}: {point.y:.1f}%({point.num}人)'
//             }
//         },
//     },
//     //== 右下網址顯示 ==
//     credits: {
//         enabled: false
//     },
//     //== 指過去顯示的詳細資料 ==
//     tooltip: {
//         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.percentage:.2f}%({point.num}人)</b>'
//         // pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%({point.num}人)</b>'
//     },

//     series: [
//         {
//             name: "",
//             colorByPoint: true,
//             data: [
//                 {
//                     name: "(35-44歲)",
//                     y: 44.12,
//                     num: 10,
//                     drilldown: "Chrome",
//                     // sliced: true,
//                     // selected: true
//                 },
//                 {
//                     name: "(45-54歲)",
//                     y: 12.32,
//                     num: 20,
//                     drilldown: "Firefox"
//                 },
//                 {
//                     name: "(25-34歲)",
//                     y: 22.45,
//                     num: 30,
//                     drilldown: "Internet Explorer"
//                 },
//                 {
//                     name: "(18-24歲)",
//                     y: 5.18,
//                     num: 40,
//                     drilldown: "Safari"
//                 },
//                 {
//                     name: "(55-64歲)",
//                     y: 13.43,
//                     num: 50,
//                     drilldown: "Edge"
//                 },
//                 {
//                     name: "(65+歲)",
//                     y: 1.02,
//                     num: 60,
//                     drilldown: "Opera"
//                 },
//                 {
//                     name: "(0-17歲)",
//                     y: 2.12,
//                     num: 70,
//                     drilldown: null
//                 }
//             ]
//         }
//     ]
// });
// var a = $('#container2').find('.highcharts-legend-item').length;
// console.log(a);

//男女性
function columnChart(box, tit, time, xData, yData) {
    // Highcharts.setOptions({
    //     lang: {
    //         thousandsSep: ','
    //     }
    // });
    var cont = Highcharts.chart(box, {
        chart: {
            type: 'column'
        },
        colors: ['#88cff9', '#f98888'],
        title: {
            text: tit
        },
        subtitle: {
            text: time,
        },
        xAxis: {
            categories: xData
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '人數'
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>總共: <b>{point.stackTotal}</b>',
            // shared: true,
            // formatter: function () {
            //     return this.x + '<br/>' +
            //         '<sapn style="color:' + this.series.color + '">' + this.series.name + '</sapn>: <b>' + FormatNumber(this.y) + '</b><br/>' +
            //         '總共:<b>' + FormatNumber(this.point.stackTotal) + '</b>';
            // },
            // formatter: function () {
            //     return '<b>' + this.x + '</b><br/>' +
            //         this.series.name + ': ' + this.y + '<br/>' +
            //         '總共：' + this.point.stackTotal;
            // }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        credits: {
            enabled: false
        },
        //右上角功能按鈕
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"],
                }
            },
            //輸出圖表檔案名稱
            filename: tit,
        },
        series: yData
        // series: [{
        //     name: '男',
        //     data: [291, 762, 398, 692, 599],
        //     stack: 'male'
        // }, {
        //     name: '女',
        //     data: [583, 384, 736, 198, 873],
        //     stack: 'male'
        // }]
    });
    columnTable = cont;
}
// Highcharts.setOptions({
//     colors: ['#88cff9', '#f98888']
// });
// Highcharts.chart('container3', {
//     chart: {
//         type: 'column'
//     },
//     colors: ['#88cff9', '#f98888'],
//     title: {
//         text: '男女人數分佈統計'
//     },
//     subtitle: {
//         text: '2021-05-10 12:00 - 2021/05/14 20:00'
//     },
//     xAxis: {
//         categories: ['5/10', '5/11', '5/12', '5/13', '5/14']
//     },

//     yAxis: {
//         allowDecimals: false,
//         min: 0,
//         title: {
//             text: '人數'
//         }
//     },

//     tooltip: {
//         formatter: function () {
//             return '<b>' + this.x + '</b><br/>' +
//                 this.series.name + ': ' + this.y + '<br/>' +
//                 '總共：' + this.point.stackTotal;
//         }
//     },

//     plotOptions: {
//         column: {
//             stacking: 'normal'
//         }
//     },
//     credits: {
//         enabled: false
//     },

//     series: [{
//         name: '男',
//         data: [291, 762, 398, 692, 599],
//         stack: 'male'
//     }, {
//         name: '女',
//         data: [583, 384, 736, 198, 873],
//         stack: 'male'
//     }]
// });
// Highcharts.setOptions({
//   colors: ['#88cff9', '#f98888']
// });
//男女比例圓餅圖
// Highcharts.chart('container5', {
//     chart: {
//         type: 'pie'
//     },
//     title: {
//         text: '客群性別分佈佔比'
//     },
//     subtitle: {
//         text: '2021-05-10 - 2021/05/14'
//     },

//     accessibility: {
//         announceNewData: {
//             enabled: true
//         },
//         point: {
//             valueSuffix: '%'
//         }
//     },

//     plotOptions: {
//         series: {
//             dataLabels: {
//                 enabled: true,
//                 format: '{point.name}: {point.y:.1f}%'
//             }
//         }
//     },
//     credits: {
//         enabled: false
//     },

//     tooltip: {
//         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b>'
//     },

//     series: [
//         {
//             name: "",
//             colorByPoint: true,
//             data: [
//                 {
//                     name: "男",
//                     y: 53.11,
//                 },
//                 {
//                     name: "女",
//                     y: 46.89,
//                 },
//             ]
//         }
//     ]
// });

//年齡分佈
// Highcharts.setOptions({
//     colors: ['#FFAD88', '#B978FA', '#F88D8D', '#FFB3B3', '#9ABCEF', '#F3E15D', '#B2C15A']
// });
// Highcharts.chart('container4', {
//     chart: {
//         type: 'pie'
//     },
//     title: {
//         text: '客群年齡分佈佔比'
//     },
//     subtitle: {
//         text: '2021-05-10 - 2021/05/14'
//     },

//     accessibility: {
//         announceNewData: {
//             enabled: true
//         },
//         point: {
//             valueSuffix: '%'
//         }
//     },

//     plotOptions: {
//         series: {
//             dataLabels: {
//                 enabled: true,
//                 format: '{point.name}: {point.y:.1f}%'
//             }
//         }
//     },
//     credits: {
//         enabled: false
//     },

//     tooltip: {
//         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b>'
//     },

//     series: [
//         {
//             name: "",
//             colorByPoint: true,
//             data: [
//                 {
//                     name: "(35-44歲)",
//                     y: 32.56,
//                     drilldown: "Chrome"
//                 },
//                 {
//                     name: "(45-54歲)",
//                     y: 27.57,
//                     drilldown: "Firefox"
//                 },
//                 {
//                     name: "(25-34歲)",
//                     y: 12.23,
//                     drilldown: "Internet Explorer"
//                 },
//                 {
//                     name: "(18-24歲)",
//                     y: 8.58,
//                     drilldown: "Safari"
//                 },
//                 {
//                     name: "(55-64歲)",
//                     y: 9.02,
//                     drilldown: "Edge"
//                 },
//                 {
//                     name: "(65+歲)",
//                     y: 2.42,
//                     drilldown: "Opera"
//                 },
//                 {
//                     name: "(0-17歲)",
//                     y: 7.62,
//                     drilldown: null
//                 }
//             ]
//         }
//     ]
// });
// Highcharts.chart('container4', {
//   chart: {
//     type: 'column'
//   },
//   title: {
//     text: '年齡分佈'
//   },
//   subtitle: {
//     text: '2021-05-10 12:00 - 2021/05/14 20:00'
//   },
//   xAxis: {
//     categories: ['0-17', '18-24', '25-34', '35-44', '45-54', '55-64', '65+'],
//   },
//   yAxis: {
//     allowDecimals: false,
//     min: 0,
//     title: {
//       text: '人數'
//     }
//   },
//   plotOptions: {
//     series: {
//       groupPadding: 0.1
//     }
//   },
//   credits: {
//     enabled: false
//   },
//   series: [{
//     name: '0-17',
//     data: [324]
//   }, {
//     name: '18-24',
//     data: [586]
//   }, {
//     name: '25-34',
//     data: [876]
//   }, {
//     name: '35-44',
//     data: [981]
//   }, {
//     name: '45-54',
//     data: [751]
//   }, {
//     name: '55-64',
//     data: [342]
//   }, {
//     name: '65+',
//     data: [112]
//   }]
// });

//=== 多圖一鍵下載 ===
/**
 * Create a global getSVG method that takes an array of charts as an argument. The SVG is returned as an argument in the callback.
 */
Highcharts.getSVG = function (charts, options, callback) {
    var svgArr = [],
        top = 0,
        width = 0,
        addSVG = function (svgres) {
            // Grab width/height from exported chart
            var svgWidth = +svgres.match(
                /^<svg[^>]*width\s*=\s*\"?(\d+)\"?[^>]*>/
            )[1],
                svgHeight = +svgres.match(
                    /^<svg[^>]*height\s*=\s*\"?(\d+)\"?[^>]*>/
                )[1],
                // Offset the position of this chart in the final SVG
                svg = svgres.replace('<svg', '<g transform="translate(0,' + top + ')" ');
            svg = svg.replace('</svg>', '</g>');
            top += svgHeight;
            width = Math.max(width, svgWidth);
            svgArr.push(svg);
        },
        exportChart = function (i) {
            // console.log('=== 1 ===');
            if (i === charts.length) {
                // console.log('=== 2 ===');
                return callback('<svg height="' + top + '" width="' + width +
                    '" version="1.1" xmlns="http://www.w3.org/2000/svg">' + svgArr.join('') + '</svg>');
            }
            charts[i].getSVGForLocalExport(options, {}, function () {
                console.log("Failed to get SVG");
            }, function (svg) {
                // console.log('=== 3 ===');
                // console.log(svg);//不知道為什麼多出div移掉後就成功了
                var toSvgL = svg.split('</div>');
                var toSvg = toSvgL[1];
                addSVG(toSvg);
                return exportChart(i + 1); // Export next only when this SVG is received
            });
        };
    exportChart(0);
};

/**
 * Create a global exportCharts method that takes an array of charts as an argument,
 * and exporting options as the second argument
 */
Highcharts.exportCharts = function (charts, options) {
    options = Highcharts.merge(Highcharts.getOptions().exporting, options);

    // Get SVG asynchronously and then download the resulting SVG
    Highcharts.getSVG(charts, options, function (svg) {
        Highcharts.downloadSVGLocal(svg, options, function () {
            console.log("Failed to export on client side");
        });
    });
};

// Set global default options for all charts
// Highcharts.setOptions({
//     exporting: {
//         fallbackToExportServer: false // Ensure the export happens on the client side or not at all
//     }
// });

//=== csv下載欄位加上自定義欄位 ===
(function (H) {
    const pick = H.pick
    H.wrap(H.Chart.prototype, 'getCSV', function (p, useLocalDecimalPoint) {
        console.log(this);
        console.log(this.renderTo.id);
        // console.log(this.series[0].points[0].numTxt);
        if (this.renderTo.id !== 'container3') {
            const chartData = this.series[0].points;
            // console.log(chartData);

            let csv = '';
            const rows = this.getDataRows();
            const csvOptions = this.options.exporting.csv;
            const decimalPoint = pick(csvOptions.decimalPoint, csvOptions.itemDelimiter !== ',' && useLocalDecimalPoint ?
                (1.1).toLocaleString()[1] :
                '.');
            // use ';' for direct to Excel
            const itemDelimiter = pick(csvOptions.itemDelimiter, decimalPoint === ',' ? ';' : ',');
            // '\n' isn't working with the js csv data extraction
            const lineDelimiter = csvOptions.lineDelimiter;
            // Transform the rows to CSV
            // console.log(rows);
            // rows[0].push('人數');
            rows.forEach(function (row, i) {
                // console.log(i);
                // console.log(row);
                let val = ''
                let j = row.length + 1;
                while (j--) {
                    val = row[j];
                    // console.log(j);
                    if (typeof val === 'string') {
                        val = '"' + val + '"';
                    }
                    if (typeof val === 'number') {
                        if (decimalPoint !== '.') {
                            val = val.toString().replace('.', decimalPoint);
                        }
                    }
                    //== 新增欄位的標題 ==
                    if (typeof val === 'undefined' && i == 0) {
                        val = '人數';
                    }
                    //== 新增欄位的內容 ==
                    if (typeof val === 'undefined' && i !== 0) {
                        val = '"' + chartData[i - 1].numTxt + '"';
                        // val = '"' + i + 'test' + j + '"';
                    }
                    row[j] = val;
                }
                // Add the values
                csv += row.join(itemDelimiter);
                // Add the line delimiter
                if (i < rows.length - 1) {
                    csv += lineDelimiter;
                }
            });
            return csv;
        } else {// 因為有包含不需要新增欄位的圖表 所以不需要多欄位返回原本的圖表即可
            let csv = '';
            const rows = this.getDataRows();
            const csvOptions = this.options.exporting.csv;
            const decimalPoint = pick(csvOptions.decimalPoint, csvOptions.itemDelimiter !== ',' && useLocalDecimalPoint ?
                (1.1).toLocaleString()[1] :
                '.');
            // use ';' for direct to Excel
            const itemDelimiter = pick(csvOptions.itemDelimiter, decimalPoint === ',' ? ';' : ',');
            // '\n' isn't working with the js csv data extraction
            const lineDelimiter = csvOptions.lineDelimiter;
            // Transform the rows to CSV
            // console.log(rows);
            // rows[0].push('人數');
            rows.forEach(function (row, i) {
                // console.log(i);
                // console.log(row);
                let val = ''
                let j = row.length;//不需要新增欄位
                while (j--) {
                    val = row[j];
                    // console.log(j);
                    if (typeof val === 'string') {
                        val = '"' + val + '"';
                    }
                    if (typeof val === 'number') {
                        if (decimalPoint !== '.') {
                            val = val.toString().replace('.', decimalPoint);
                        }
                    }
                    row[j] = val;
                }
                // Add the values
                csv += row.join(itemDelimiter);
                // Add the line delimiter
                if (i < rows.length - 1) {
                    csv += lineDelimiter;
                }
            });
            return csv;
        }

    });
}(Highcharts));
