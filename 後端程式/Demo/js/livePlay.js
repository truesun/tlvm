const GO_BUTTON_START = "Play";
const GO_BUTTON_STOP = "Stop";

function livePlay(videoBox, tokenData, camData, liveNote) {
    var remoteVideo = null;
    var peerConnection = null;
    var peerConnectionConfig = { 'iceServers': [] };
    var localStream = null;

    // var token = '971073a15908a3d5d88879ff6db64255100114a40e148fd5d32a741b926f6b35';
    // var token = '677560b911a5bff1eb70588510da201b472d95bce5913593d9a5016439ddd5c0';
    var token = tokenData;
    var dd = '{Web}_{KW-External}_{12345678}';
    // var dd = '{Web}_{KW-External}_{12345678}';
    // var cam = {
    //     FinTimestamp: "2021-05-24T10:23:08.754+0000##0902-882",
    //     vcam_uid: "ea8e1f99fc4d47ca8e7fc3936a19e9de",
    //     ws_url: "wss://oregon-p1-stage-ss-origin-1-1.beseye.com:443/ea8e1f99fc4d47ca8e7fc3936a19e9de/webrtc-session.json",
    //     application_name: "live-origin-record",
    //     stream: "{enc_ea8e1f99}benIJa9G2DnMEyH9FLXkenLbHyphBDxSUHv7L_Ey_2lYv_eVAHRM6K85rOK5zO-j",
    //     stable_upstreaming: true
    // }
    var cam = camData;
    // var cam = {
    //     "FinTimestamp": "2021-06-10T07:32:24.232+0000##0902-882",
    //     "vcam_uid": "ea8e1f99fc4d47ca8e7fc3936a19e9de",
    //     "ws_url": "wss://oregon-p1-stage-ss-origin-1-1.beseye.com:443/ea8e1f99fc4d47ca8e7fc3936a19e9de/webrtc-session.json",
    //     "application_name": "live-origin-record",
    //     "stream": "{enc_ea8e1f99}benIJa9G2DnMEyH9FLXkenLbHyphBDxSUHv7L_Ey_2lYv_eVAHRM6K85rOK5zO-j",
    //     "stable_upstreaming": true
    // }

    var wsURL = `${cam.ws_url}?vci=${cam.vcam_uid}&token=${token}&dd=${dd}`
    var streamInfo = { "applicationName": `${cam.application_name}`, "streamName": `${cam.stream}` }

    var userData = {};
    var wsConnection = null;
    var repeaterRetryCount = 0;
    var doGetAvailableStreams = false;

    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
    window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

    $("#buttonGo").attr('value', GO_BUTTON_START);
    var $liveNote = $(liveNote); //$("#sdpDataTag")

    remoteVideo = document.getElementById(videoBox);
    // remoteVideo.addEventListener('loadstart', function () {
    //     console.error('on loadstart');
    // })
    // remoteVideo.addEventListener('loadedmetadata', function () {
    //     console.error('on loadedmetadata');
    // })
    // remoteVideo.addEventListener('loadeddata', function () {
    //     console.error('on loadeddata');
    // })
    // remoteVideo.addEventListener('canplay', function () {
    //     console.error('on canplay');
    // })
    // remoteVideo.addEventListener('canplaythrough', function () {
    //     console.error('on canplaythrough');
    // })
    // remoteVideo.addEventListener('play', function () {
    //     console.error('on play')
    // })

    start();

    // function pageReady() {

    //     // cookieWSURL = wsURL;
    //     // // $.cookie("webrtcPublishWSURL", cookieWSURL);
    //     // console.log('cookieWSURL: ' + cookieWSURL);

    //     // cookieApplicationName = streamInfo.applicationName;
    //     // // $.cookie("webrtcPublishApplicationName", cookieApplicationName);
    //     // console.log('cookieApplicationName: ' + cookieApplicationName);

    //     // cookieStreamName = streamInfo.streamName;
    //     // // $.cookie("webrtcPublishStreamName", cookieStreamName);
    //     // console.log('cookieStreamName: ' + cookieStreamName);

    //     // $('#sdpURL').val(cookieWSURL);
    //     // $('#applicationName').val(cookieApplicationName);
    //     // $('#streamName').val(cookieStreamName);

    //     $("#buttonGo").attr('value', GO_BUTTON_START);

    //     remoteVideo = document.getElementById('remoteVideo');
    //     remoteVideo.addEventListener('loadstart', function () {
    //         console.error('on loadstart');
    //     })
    //     remoteVideo.addEventListener('loadedmetadata', function () {
    //         console.error('on loadedmetadata');
    //     })
    //     remoteVideo.addEventListener('loadeddata', function () {
    //         console.error('on loadeddata');
    //     })
    //     remoteVideo.addEventListener('canplay', function () {
    //         console.error('on canplay');
    //     })
    //     remoteVideo.addEventListener('canplaythrough', function () {
    //         console.error('on canplaythrough');
    //     })
    //     remoteVideo.addEventListener('play', function () {
    //         console.error('on play')
    //     })
    // }

    function wsConnect(url) {
        wsConnection = new WebSocket(url);
        wsConnection.binaryType = 'arraybuffer';

        wsConnection.onopen = function () {
            // console.log("=== 1.wsConnection ===");
            console.log("wsConnection.onopen");

            peerConnection = new RTCPeerConnection(peerConnectionConfig);
            peerConnection.onicecandidate = gotIceCandidate;
            peerConnection.ontrack = gotRemoteTrack;
            peerConnection.oniceconnectionstatechange = gotIceConnectionStateChange;

            console.log("wsURL: " + wsURL);

            sendPlayGetOffer();
        }

        function sendPlayGetOffer() {
            // console.log("=== 2.sendPlayGetOffer ===");
            console.log("sendPlayGetOffer: " + JSON.stringify(streamInfo));
            console.log('{"direction":"play", "command":"getOffer", "streamInfo":' + JSON.stringify(streamInfo) + ', "userData":' + JSON.stringify(userData) + '}')
            wsConnection.send('{"direction":"play", "command":"getOffer", "streamInfo":' + JSON.stringify(streamInfo) + ', "userData":' + JSON.stringify(userData) + '}');
        }

        wsConnection.onmessage = function (evt) {
            // console.log("=== 3.onmessage ===");
            console.log("wsConnection.onmessage: " + evt.data);
            var msgJSON = JSON.parse(evt.data);

            var msgStatus = Number(msgJSON['status']);
            var msgCommand = msgJSON['command'];

            if (msgStatus == 514) // repeater stream not ready
            {
                repeaterRetryCount++;
                if (repeaterRetryCount < 10) {
                    setTimeout(sendGetOffer, 500);
                }
                else {
                    $liveNote.html('Live stream repeater timeout: ' + streamName);
                    // $("#sdpDataTag").html('Live stream repeater timeout: ' + streamName);
                    stopPlay();
                }
            }
            else if (msgStatus != 200) {
                $liveNote.html(msgJSON['statusDescription']);
                // $("#sdpDataTag").html(msgJSON['statusDescription']);
                stopPlay();
            }
            else {
                // console.log("=== GOGOPLAY ===");
                $liveNote.html("GOGOPLAY");
                // $("#sdpDataTag").html("");

                var streamInfoResponse = msgJSON['streamInfo'];
                if (streamInfoResponse !== undefined) {
                    streamInfo.sessionId = streamInfoResponse.sessionId;
                }

                var sdpData = msgJSON['sdp'];
                if (sdpData !== undefined) {
                    console.log('sdp: ' + JSON.stringify(msgJSON['sdp']));

                    // We mundge the SDP here, before creating an Answer
                    // If you can get the new MediaAPI to work this might
                    // not be needed.
                    msgJSON.sdp.sdp = enhanceSDP(msgJSON.sdp.sdp);

                    peerConnection.setRemoteDescription(new RTCSessionDescription(msgJSON.sdp), function () {
                        peerConnection.createAnswer(gotDescription, errorHandler);
                    }, errorHandler);
                }

                var iceCandidates = msgJSON['iceCandidates'];
                if (iceCandidates !== undefined) {
                    for (var index in iceCandidates) {
                        var iceCandidate = iceCandidates[index]
                        console.log('iceCandidates: ' + JSON.stringify(iceCandidate));
                        var candidateInfo = iceCandidate.candidate.split(' ')
                        if (candidateInfo[2] == 'TCP' || candidateInfo[2] == 'UDP' && typeof InstallTrigger != 'undefined') {
                            try {
                                peerConnection.addIceCandidate(new RTCIceCandidate(iceCandidate));
                            } catch (error) {
                                errorHandler(error);
                            }
                        }
                    }
                }
            }

            if ('sendResponse'.localeCompare(msgCommand) == 0) {
                if (wsConnection != null)
                    wsConnection.close();
                wsConnection = null;
            }
        }

        wsConnection.onclose = function (event) {
            console.log("wsConnection.onclose", event);
            if (event.code == 1003) {
                alert("Remote end hung up; dropping PeerConnection");
            }
        }

        wsConnection.onerror = function (evt) {
            console.log("wsConnection.onerror: " + JSON.stringify(evt));

            $liveNote.html('WebSocket connection failed: ' + wsURL);
            // $("#sdpDataTag").html('WebSocket connection failed: ' + wsURL);
        }
    }

    function getAvailableStreams() {
        doGetAvailableStreams = true;
        startPlay();
    }

    function startPlay() {
        repeaterRetryCount = 0;

        // $.cookie("webrtcPublishWSURL", wsURL, { expires: 365 });
        // $.cookie("webrtcPublishApplicationName", streamInfo.applicationName, { expires: 365 });
        // $.cookie("webrtcPublishStreamName", streamInfo.streamName, { expires: 365 });
        // console.log("=== 4.startPlay ===");
        console.log("startPlay: wsURL:" + wsURL + " streamInfo:" + JSON.stringify(streamInfo));

        wsConnect(wsURL);

        if (!doGetAvailableStreams) {
            $("#buttonGo").attr('value', GO_BUTTON_STOP);
        }
    }

    function stopPlay() {
        // console.log("=== 5.stopPlay ===");
        if (peerConnection != null)
            peerConnection.close();
        peerConnection = null;

        if (wsConnection != null)
            wsConnection.close();
        wsConnection = null;

        remoteVideo.removeAttribute('src');

        console.log("stopPlay");

        $("#buttonGo").attr('value', GO_BUTTON_START);
    }

    // start button clicked
    function start() {
        // console.log("=== 6.start ===");
        doGetAvailableStreams = false;

        if (peerConnection == null)
            startPlay();
        else
            stopPlay();
    }

    function gotIceCandidate(event) {
        // console.log("=== 7.gotIceCandidate ===");
        let candidate = event.candidate
        if (candidate != null) {
            console.log("Local Candidate: ", candidate);
        }
    }

    function enhanceSDP(sdpStr) {
        // console.log("=== 8.enhanceSDP ===");
        var sdpLines = sdpStr.split(/\r\n/);
        var sdpSection = 'header';
        var hitMID = false;
        var sdpStrRet = '';

        for (var sdpIndex in sdpLines) {
            var sdpLine = sdpLines[sdpIndex];

            if (sdpLine.length == 0)
                continue;

            if (sdpLine.includes("profile-level-id")) {
                console.log("found profile-id");
                // This profile seems to be correct for the stream publishing,
                // however will not allow Safari to play it back, so we swap
                // it for a baseline constrained one, which is declared when
                // Safari publishes in the SDP.
                if (sdpLine.includes("640029")) {
                    sdpLine = sdpLine.replace("640029", "42E01F");
                }
            }

            sdpStrRet += sdpLine;
            sdpStrRet += '\r\n';
        }

        console.log("Resulting SDP: " + sdpStrRet);
        return sdpStrRet;
    }

    function gotDescription(description) {
        // console.log("=== 9.gotDescription ===");
        console.log('gotDescription' + description.sdp);

        peerConnection.setLocalDescription(description, function () {
            console.log('sendAnswer');
            console.log('{"direction":"play", "command":"sendResponse", "streamInfo":' + JSON.stringify(streamInfo) + ', "sdp":' + JSON.stringify(description) + ', "userData":' + JSON.stringify(userData) + '}');
            wsConnection.send('{"direction":"play", "command":"sendResponse", "streamInfo":' + JSON.stringify(streamInfo) + ', "sdp":' + JSON.stringify(description) + ', "userData":' + JSON.stringify(userData) + '}');

        }, function () { console.error('set description error') });
    }

    function gotIceConnectionStateChange() {
        // console.log("=== 10.gotIceConnectionStateChange ===");
        console.log('ice state change: ' + peerConnection.iceConnectionState);
    }

    function gotRemoteTrack(event) {
        // console.log("=== 11.gotRemoteTrack ===");
        console.log('gotRemoteTrack: kind:' + event.track.kind + ' stream:' + event.streams[0]);

        remoteVideo.addEventListener('error', videoOnError)
        try {
            remoteVideo.srcObject = event.streams[0];
        } catch (error) {
            remoteVideo.src = window.URL.createObjectURL(event.streams[0]);
        }
    }

    function errorHandler(error) {
        console.error(error);
    }

    function videoOnError(error) {
        console.error(error);
    }
}
