
var storymap;
var storymap_data = {
    storymap: {
        slides: []
    }
}
$(document).ready(function () {
    $('#goTop').remove();
    var apiLink, GroupApiLink;
    var lang = getCookie('CacheLang');
    var mapKey = getLinkVal('Wid') || getCookie('Key');
    var mapEvent = getLinkVal('Eventid');
    if (!mapEvent) {
        mapEvent = 0;
    }
    if (lang == 'zh') {
        apiLink = '/Api/WebServiceMapCn?Type=1&key=' + mapKey;
        getMapCn(apiLink, mapEvent);
        GroupApiLink = '/Api/WebServiceMapCn?Type=3&key=' + mapKey;;
        getGroupEventsCn(GroupApiLink);
    } else {
        apiLink = '/Api/WebServiceMapEn?Type=1&key=' + mapKey;
        getMapEn(apiLink, mapEvent);
        GroupApiLink = '/Api/WebServiceMapEn?Type=3&key=' + mapKey;;
        getGroupEventsEn(GroupApiLink);
    }
    $(document).on('click', '#mapGo', function () {
        storymap.goTo(1);
    });
    $(document).on('click', '.t-even', function () {
        var eventIdAct = $(this).data('eventid');
        var eventInd = storymap_data.storymap.slides.findIndex(function (item) {
            return item.eventId == eventIdAct;
        });
        storymap.goTo(eventInd);
    });

    $(document).on('click', '.tagBtn', function () {
        var tagId = $(this).data('tagid');
        tagSrcPage(tagId);
    });

    var $moreMapTagBtn = $('#moreMapTagBtn');
    var $MapTagBox = $('#allMapTagBox');

    var tagOpen = false;
    $moreMapTagBtn.on('click', function () {
        var tabBtnTxt;
        if (tagOpen) {
            $MapTagBox.removeClass('open');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '展開' : tabBtnTxt = 'More';
            $moreMapTagBtn.find('span').html(tabBtnTxt);

            $('.map-all-tag').mCustomScrollbar("destroy");
            tagOpen = false;
        } else {
            $MapTagBox.addClass('open');
            $moreMapTagBtn.find('i').addClass('fa-arrow-to-bottom');
            $moreMapTagBtn.find('i').removeClass('fa-arrow-to-top');
            lang == 'zh' ? tabBtnTxt = '收合' : tabBtnTxt = 'Close';
            $moreMapTagBtn.find('span').html(tabBtnTxt);

            $('.map-all-tag').mCustomScrollbar();
            tagOpen = true;
        }
    });

    $(document).on('click', '.tag', function () {
        var indEvent = $(this).data('tagid');
        var $eventTagBox = $('#eventsTag' + indEvent);
        var eventTagBoxShow = $eventTagBox.is(':visible');
        if (eventTagBoxShow) {
            $('.tag').removeClass('act');
            $('.map-event').hide();
            $('#mapEventBox').hide();
        } else {
            $('.tag').removeClass('act');
            $(this).addClass('act');
            if ($eventTagBox.length) {
                $('.map-event').hide();
                $('#mapEventBox').show();
                $eventTagBox.show();
            } else {
                $('.map-event').hide();
                $('#mapEventBox').hide();
            }
        }
    });
});
$(window).resize(function () {
    tagBtnMoreShow();
});
function getMapCn(apiLink, mapEvent) {
    $.get(apiLink, function (data) {
        console.log(JSON.parse(data));
        var mapData = JSON.parse(data);
        var firstMap = mapData.splice(0, 1);
        firstMap = firstMap[0];
        var firstTagListTxt;
        var firstTagListL = JSON.parse(firstMap.data.TagidArray);
        if (firstTagListL.length) {
            var firstTagList = [];
            $.each(firstTagListL, function (ind, val) {
                firstTagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_tw + '</a>');
            });
            firstTagListTxt = '<div class="his-timeline-tag-box">' + firstTagList.join() + '</div>';
        } else {
            firstTagListTxt = '';
        }

        var firstMapData = {
            "type": "overview",
            "eventId": firstMap.Eventid,
            "background": {
                "color": "#005C9A",
            },
            "text": {
                "headline": firstMap.Name_tw,
                "text": '<p>' + firstMap.Content_tw + '</p>' + firstTagListTxt + '<div id="mapGo" class="map-go-btn">' + firstMap.Name_tw + '<div></div></div>'
            },
            "media": {
                "url": firstMap.pic_tw,
                "credit": firstMap.med_tw,
            }
        };
        storymap_data.storymap.slides.push(firstMapData);

        $.each(mapData, function (ind, val) {
            var mapListData, tagListL, tagListTxt;
            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_tw + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }
            mapListData = {
                "eventId": val.Eventid,
                "background": {
                    "color": "#005C9A",
                },
                "location": {
                    "icon": "/img/icon_map.png",
                    "acticon": "/img/icon_map_act.png",
                    "iconSize": [35, 35],
                    "zoom": 17,
                    "lat": Number(val.latitude),
                    "lon": Number(val.longitude),
                },
                "text": {
                    "headline": val.Name_tw,
                    "text": '<p>' + val.Content_tw + '</p>' + tagListTxt,
                    "maptxt": val.Content_tw,
                },
                "media": {
                    "url": val.pic_tw,
                    "credit": val.med_tw,
                }
            }
            storymap_data.storymap.slides.push(mapListData);
        });
    });

    var slideInd = storymap_data.storymap.slides.findIndex(function (item) {
        return item.eventId == mapEvent;
    });
    if (slideInd == '-1') {
        slideInd = 0;
    }

    var storymap_options = {
        map_type: "osm:standard",
        language: "zh-tw",
        map_as_image: false,
        map_subdomains: "",
        use_custom_markers: true,
        map_popup: true,
        start_at_slide: slideInd,
        map_mini: false,
        calculate_zoom: false
    };

    storymap = new VCO.StoryMap('MapArea', storymap_data, storymap_options);

    window.onresize = function (event) {
        storymap.updateDisplay();
    }
}

function getMapEn(apiLink, mapEvent) {
    $.get(apiLink, function (data) {
        console.log(JSON.parse(data));
        var mapData = JSON.parse(data);
        var firstMap = mapData.splice(0, 1);
        firstMap = firstMap[0];
        var firstTagListTxt;

        var firstTagListL = JSON.parse(firstMap.data.TagidArray);
        if (firstTagListL.length) {
            var firstTagList = [];
            $.each(firstTagListL, function (ind, val) {
                firstTagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_en + '</a>');
            });
            firstTagListTxt = '<div class="his-timeline-tag-box">' + firstTagList.join() + '</div>';
        } else {
            firstTagListTxt = '';
        }

        var firstMapData = {
            "type": "overview",
            "eventId": firstMap.Eventid,
            "background": {
                "color": "#005C9A",
            },
            "text": {
                "headline": firstMap.Name_en,
                "text": '<p>' + firstMap.Content_en + '</p>' + firstTagListTxt + '<div id="mapGo" class="map-go-btn">' + firstMap.Name_en + '<div></div></div>'
            },
            "media": {
                "url": firstMap.pic_en,
                "credit": firstMap.med_en,
            }
        };
        storymap_data.storymap.slides.push(firstMapData);

        $.each(mapData, function (ind, val) {
            var mapListData, tagListL, tagListTxt;

            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_en + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }
            mapListData = {
                "eventId": val.Eventid,
                "background": {
                    "color": "#005C9A",
                },
                "location": {
                    "icon": "/img/icon_map.png",
                    "acticon": "/img/icon_map_act.png",
                    "iconSize": [35, 35],
                    "zoom": 17,
                    "lat": Number(val.latitude),
                    "lon": Number(val.longitude),
                },
                "text": {
                    "headline": val.Name_en,
                    "text": '<p>' + val.Content_en + '</p>' + tagListTxt,
                    "maptxt": val.Content_en,
                },
                "media": {
                    "url": val.pic_en,
                    "credit": val.med_en,
                }
            }
            storymap_data.storymap.slides.push(mapListData);
        });
    });

    var slideInd = storymap_data.storymap.slides.findIndex(function (item) {
        return item.eventId == mapEvent;
    });
    if (slideInd == '-1') {
        slideInd = 0;
    }

    var storymap_options = {
        map_type: "osm:standard",
        language: "en",
        map_as_image: false,
        map_subdomains: "",
        use_custom_markers: true,
        map_popup: true,
        start_at_slide: slideInd,
        map_mini: false,
        calculate_zoom: false
    };

    storymap = new VCO.StoryMap('MapArea', storymap_data, storymap_options);

    window.onresize = function (event) {
        storymap.updateDisplay();
    }
}

function getGroupEventsCn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        var tagData = JSON.parse(data);
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_tw + '</div>');
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_tw + '</div>');
                });
            }
        });
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').remove();
        } else {
            $('.map-event').mCustomScrollbar();
        }
        tagBtnMoreShow();
    });
}

function getGroupEventsEn(GroupApiLink) {
    $.get(GroupApiLink, function (data) {
        console.log(JSON.parse(data));
        var tagData = JSON.parse(data);
        $('#allMapTagBox').html('<div id="allMapTagInnerBox"></div>');
        $('#mapEventBox').html('');
        $.each(tagData, function (ind, val) {
            var eventL = JSON.parse(val.dataGroup.TagidArray);
            if (eventL.length) {
                $('#allMapTagInnerBox').append('<div class="tag" data-tagid=' + val.eventgid + '>' + val.title_en + '</div>');
                $('#mapEventBox').append('<div class="map-event" id="eventsTag' + val.eventgid + '" style="display: none;"></div>');
                var $eventsBox = $('#eventsTag' + val.eventgid);
                $.each(eventL, function (ind, val) {
                    $eventsBox.append('<div class="t-even" data-eventid="' + val.eventid + '">' + val.Name_en + '</div>');
                });
            }
        });
        var groupEventL = $('#allMapTagInnerBox .tag').length;
        if (!groupEventL) {
            $('.map-tag-box').remove();
        } else {
            $('.map-event').mCustomScrollbar();
        }
        tagBtnMoreShow();
    });
}

function tagBtnMoreShow() {
    var tagsH = $('#allMapTagInnerBox').outerHeight();
    if (tagsH < 45) {
        $('#moreMapTagBtn').hide();
    } else {
        $('#moreMapTagBtn').show();
    }
}

function tagSrcPage(tagId) {
    var lang = getCookie('CacheLang');
    var protocol = window.location.protocol;
    var host = window.location.host;
    var domain = protocol + '//' + host;
    var linkPage = window.location.pathname;
    console.log(domain);

    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("target", "_blank");
    console.log(linkPage);
    if (linkPage == '/en/Writer/WriterCont') {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterSrcTagList?Tagid=" + tagId);
    }
    else if (linkPage == '/zh/Writer/WriterCont') {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterSrcTagList?Tagid=" + tagId);
    }
    else {
        form.setAttribute("action", domain + "/" + lang + "/Writer/WriterStorySrcTagList?Tagid=" + tagId);
    }

    document.body.appendChild(form);
    form.submit();
}
