$(function () {
    var validator = $("#form").validate({
        submitHandler: function (form) {
            alert("完成");
            form.submit();
        }
    });

    //時間選擇器
    //=== 日期格式 ===
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    yesterday.setHours(23);
    yesterday.setMinutes(30);
    // var yesterdayStart = new Date();
    // yesterdayStart.setDate(yesterdayStart.getDate() - 1);
    // yesterdayStart.setHours(0);
    // yesterdayStart.setMinutes(0);
    // console.log(yesterdayStart);

    // var today = new Date();
    // var currentDate = new Date();
    // // currentDate.setHours(0);
    // currentDate.setMinutes(0);
    $("#startTime").datepicker({
        dateFormat: "yyyy/mm/dd",
        autoClose: true,
        position: "top right",
        language: 'en',
        maxDate: yesterday,
        // startDate: currentDate,
        //// maxDate: today,
        timepicker: true,
        maxMinutes: 30,
        minutesStep: 30,
        // range: true,
        // multipleDatesSeparator: ' ~ ',
        // maxDate: new Date(),
        // minDate: threeMonth,
        onSelect: (fd, d, picker) => {
            //- this.date = this.$("#date").val();
        },
    });
    // var currentDate = new Date();
    // $("#dateRange").data('datepicker').selectDate(currentDate);

    //取消因為滾動不會消失的日期選取器
    $('.main').scroll(function () {
        $('.datepickers-container').find('.datepicker').removeClass('active');
        $('.datepickers-container').find('.datepicker').css({ 'left': '-100000px' });
        // $('.datepicker-here').blur();
        $('#startTime').blur();
    });

    //=== 取得API ===
    var clientIp = returnCitySN["cip"];
    //== 取的token ==
    var token;
    var apiLink1 = '/overview.aspx?Type=1'
    $.get(apiLink1, function (data) {
        console.log(data);
        var aipData = JSON.parse(data);
        token = aipData[0].access_token;
    });

    //== 取的攝影機列表 ==
    // $('#type').html('');
    var apiLink2 = '/overview.aspx?Type=2'
    // var cameraList = [];
    $.get(apiLink2, function (data) {
        console.log(data);
        console.log(token);
        var aipData = JSON.parse(data);
        // aipData.shift();
        console.log(aipData);
        $.each(aipData, function (ind, val) {
            var { CameraUID, CameraName } = val;
            // console.log(CameraUID);
            // console.log(CameraName);
            $('#type').append('<option value="' + CameraUID + '">' + CameraName + '</option>');
            //== 複數 ==
            // cameraList.push({ CameraUID, CameraName });
        });
    });
    // console.log(cameraList);

    //== 搜尋時間 ==
    $('#replaySrcBtn').on('click', function () {
        console.log('=== replaySrcBtn ===');
        var srcTime = $('#startTime').val();
        if (srcTime) {
            var srcTimeSet = new Date($('#startTime').val());
            var timeStr = srcTimeSet.getTime();
            console.log(timeStr);
            // var type = $('#type').val();

            //== 播放重置 ==
            $('#video1,#video2').remove();
            $('#videoReplayBox .video-replay').prepend('<video id="video2" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            $('#videoReplayBox .video-replay').prepend('<video id="video1" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            video1 = document.getElementById('video1');
            video2 = document.getElementById('video2');
            player1 = undefined
            player2 = undefined
            players = {}
            playlist = []
            isPlaying = false
            // onVideoEnded();

            var vcamId = $('#type').val();
            replayApi(vcamId, timeStr, token, clientIp);
            //== 複數 ==
            // if (type == 1) {
            //     $('#video1,#video2').remove();
            //     $('#videoReplayBox .video-replay').prepend('<video id="video2" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     $('#videoReplayBox .video-replay').prepend('<video id="video1" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     video1 = document.getElementById('video1');
            //     video2 = document.getElementById('video2');
            //     player1 = undefined
            //     player2 = undefined
            //     players = {}
            //     playlist = []
            //     isPlaying = false

            //     var vcamId1 = cameraList[2].CameraUID;
            //     $('#videoReplayTit').html(cameraList[2].CameraName);
            //     replayApi(vcamId1, timeStr, token, clientIp);
            //     //= 2 =
            //     // var ishasVideo2 = $('#video3').length;
            //     // if (ishasVideo2 == 0) {
            //     //     $('#videoReplayBox2 .video-replay').prepend('<video id="video4" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     //     $('#videoReplayBox2 .video-replay').prepend('<video id="video3" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     //     video3 = document.getElementById('video3');
            //     //     video4 = document.getElementById('video4');
            //     // }
            //     $('#video3,#video4').remove();
            //     $('#videoReplayBox2 .video-replay').prepend('<video id="video4" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     $('#videoReplayBox2 .video-replay').prepend('<video id="video3" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     video3 = document.getElementById('video3');
            //     video4 = document.getElementById('video4');
            //     player3 = undefined
            //     player4 = undefined
            //     players2 = {}
            //     playlist2 = []
            //     isPlaying2 = false
            //     var vcamId2 = cameraList[1].CameraUID;
            //     $('#videoReplayTit2').html(cameraList[1].CameraName);
            //     replayApiTwo(vcamId2, timeStr, token, clientIp);
            // }
            // document.addEventListener('DOMContentLoaded', initApp(mediaArray));
            //== 加入播放時綁定並播放 ==
            // var ishasVideo2 = $('#video3').length;
            // if (ishasVideo2 == 0) {
            //     $('#videoReplayBox2 .video-replay').prepend('<video id="video4" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     $('#videoReplayBox2 .video-replay').prepend('<video id="video3" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
            //     video3 = document.getElementById('video3');
            //     video4 = document.getElementById('video4');
            // }
            // player3 = undefined
            // player4 = undefined
            // players2 = {}
            // playlist2 = []
            // isPlaying2 = false
            // document.addEventListener('DOMContentLoaded', initAppTwo(mediaArray));
        } else {
            alert('請選擇時間。');
        }
    });
    // $('.note').on('click', function () {
    //     console.log('=== note ===');
    //     //== 移除時重置 ==
    //     $('#video3,#video4').remove();
    //     // player3 = undefined
    //     // player4 = undefined
    //     // players2 = {}
    //     // playlist2 = []
    //     // isPlaying2 = false
    // });

    // var vcamUid = 'ea8e1f99fc4d47ca8e7fc3936a19e9de';
    // var sTime = '1623301200000';//1622540407540
    // var apiLinkReplay = '/DVRMPDLive.aspx?Type=1&access_token=' + token + '&vcam_uid=' + vcamUid + '&client_ip=' + clientIp + '&start_time=' + sTime;
    // // var mediaArray = [];
    // $.get(apiLinkReplay, function (data) {
    //     // console.log(data);
    //     var apiData = JSON.parse(data);
    //     console.log('=== replay API ===');
    //     // console.log(apiData);
    //     var videoList = apiData.play_list;
    //     console.log(videoList);
    //     // $.each(videoList, function (ind, val) {
    //     //     var { server, stream, startTime, duration } = val;
    //     //     var hostL = server.split(':');
    //     //     var host = hostL[1].replace('//', '');
    //     //     console.log(host);
    //     //     var fUrl = server + '?host=' + host + '&token=' + token + '&dd={Web}_{KW-External}_{12345678}&vci=' + vcamUid + '&videoArray=' + stream;
    //     //     mediaArray.push({ 'server': server, 'stream': stream, 'startTime': startTime, 'duration': duration, 'manifestUri': fUrl });
    //     // });
    //     //== 啟動回放 ==
    //     // document.addEventListener('DOMContentLoaded', initApp(mediaArray));
    // });
    // $('#videoReplayBox .video-replay').prepend('<video id="video2" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
    // $('#videoReplayBox .video-replay').prepend('<video id="video1" width="760" poster="" controls autoplay muted>Your browser does not support the video tag.</video>');
    console.log('＝＝= 喵喵喵 汪汪 喵喵 咪咪 ＝＝＝')
    // const mediaArray = [
    //     {
    //         // "server": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist",
    //         // "stream": "{enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd",
    //         // "startTime": "1623301192136",
    //         // "duration": "10013",
    //         "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd",
    //     },
    //     {
    //         "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzHhtCYFS-mIEjurTYa0Vz3EHjI-xuDn5Oh7xIm6bdjV6B07he9g59xUVWxy_QxOx9g==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd",
    //     }
    //     // {
    //     //     "server": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist",
    //     //     "stream": "{enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd",
    //     //     "startTime": "1623301202114",
    //     //     "duration": "10035",
    //     //     "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzJp9QZwRDgPyJdBUPMYDyX6d3Ner01rNVmm-9ek8g-L03w0pzhb0qAxuQ1kBYbDpMw==/manifest.mpd",
    //     // },
    //     // {
    //     //     "server": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist",
    //     //     "stream": "{enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd",
    //     //     "startTime": "1623301212002",
    //     //     "duration": "10128",
    //     //     "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHDdA37OHOuJEzT8C5aUR57tFvsdmf3LyVAYxyoN6nNPzGoYvehweLVkw61AWXgdjnSyrO3yMUedQci-QyLCHETr4nCtwT3A3eePoRWtGR3ldA==/manifest.mpd",
    //     // },
    // ]
    // //== 啟動回放 ==
    // document.addEventListener('DOMContentLoaded', initApp(mediaArray));
    // document.addEventListener('DOMContentLoaded', initAppTwo(mediaArray));
});
function replayApi(vcamId, startTime, token, clientIp) {
    var vcamUid = vcamId;//'ea8e1f99fc4d47ca8e7fc3936a19e9de';
    var sTime = startTime;//'1623301200000';//1622540407540
    var apiLinkReplay = '/DVRMPDLive.aspx?Type=1&access_token=' + token + '&vcam_uid=' + vcamUid + '&client_ip=' + clientIp + '&start_time=' + sTime;
    var serverLink, host;
    var replayMedia = [];
    $.get(apiLinkReplay, function (data) {
        // console.log(data);
        var apiData = JSON.parse(data);
        console.log('=== replay API ===');
        // console.log(apiData);
        var videoList = apiData.play_list;
        console.log(videoList);
        var replayStream = [];
        $.each(videoList, function (ind, val) {
            var { server, stream, startTime, duration } = val;
            serverLink = server;
            var hostL = server.split(':');
            host = hostL[1].replace('//', '');
            console.log(host);
            replayStream.push(stream);
            // var fUrl = server + '?host=' + host + '&token=' + token + '&dd={Web}_{KW-External}_{12345678}&vci=' + vcamUid + '&videoArray=' + stream;
            // replayMedia.push({ 'server': server, 'stream': stream, 'startTime': startTime, 'duration': duration, 'manifestUri': fUrl });
        });
        //因為串接最多為12筆，所以依 12 分割看有幾次
        var pushNum = Math.ceil(replayStream.length / 12);
        //依分割次數去加入陣列 一筆12個資料
        var replayVideo = [];
        for (var i = 0; i < pushNum; i++) {
            console.log(i);
            var streamPart = replayStream.splice(0, 12);
            replayVideo.push({ streamList: streamPart });
        }
        // console.log('=== replayVideo ===');
        // console.log(replayVideo);
        //再用總陣列數去放串連影片
        $.each(replayVideo, function (ind, val) {
            var { streamList } = val;
            var streamListStr = streamList.join('&videoArray=');
            var fUrl = serverLink + '?host=' + host + '&token=' + token + '&dd={Web}_{KW-External}_{12345678}&vci=' + vcamUid + '&videoArray=' + streamListStr;
            replayMedia.push({ 'manifestUri': fUrl });
        });

        console.log('=== videoList ===');
        console.log(vcamUid);
        console.log(replayMedia.length);
        console.log(replayMedia[0]);
        //== 啟動回放 ==
        document.addEventListener('DOMContentLoaded', initApp(replayMedia));
    });
}

function replayApiTwo(vcamId, startTime, token, clientIp) {
    var vcamUid = vcamId;//'ea8e1f99fc4d47ca8e7fc3936a19e9de';
    var sTime = startTime;//'1623301200000';//1622540407540
    var apiLinkReplayTwo = '/DVRMPDLive.aspx?Type=1&access_token=' + token + '&vcam_uid=' + vcamUid + '&client_ip=' + clientIp + '&start_time=' + sTime;
    var serverLink, host;
    var replayMediaTwo = [];
    $.get(apiLinkReplayTwo, function (data) {
        // console.log(data);
        var apiData = JSON.parse(data);
        console.log('=== replay API ===');
        // console.log(apiData);
        var videoList = apiData.play_list;
        console.log(videoList);
        var replayStreamTwo = [];
        $.each(videoList, function (ind, val) {
            var { server, stream, startTime, duration } = val;
            serverLink = server;
            var hostL = server.split(':');
            host = hostL[1].replace('//', '');
            console.log(host);
            replayStreamTwo.push(stream);
            // var fUrl = server + '?host=' + host + '&token=' + token + '&dd={Web}_{KW-External}_{12345678}&vci=' + vcamUid + '&videoArray=' + stream;
            // replayMediaTwo.push({ 'server': server, 'stream': stream, 'startTime': startTime, 'duration': duration, 'manifestUri': fUrl });
        });
        //因為串接最多為12筆，所以依 12 分割看有幾次
        var pushNum = Math.ceil(replayStreamTwo.length / 12);
        //依分割次數去加入陣列 一筆12個資料
        var replayVideo = [];
        for (var i = 0; i < pushNum; i++) {
            console.log(i);
            var streamPart = replayStreamTwo.splice(0, 12);
            replayVideo.push({ streamList: streamPart });
        }
        // console.log('=== replayVideo ===');
        // console.log(replayVideo);
        //再用總陣列數去放串連影片
        $.each(replayVideo, function (ind, val) {
            var { streamList } = val;
            var streamListStr = streamList.join('&videoArray=');
            var fUrl = serverLink + '?host=' + host + '&token=' + token + '&dd={Web}_{KW-External}_{12345678}&vci=' + vcamUid + '&videoArray=' + streamListStr;
            replayMediaTwo.push({ 'manifestUri': fUrl });
        });

        console.log('=== videoList2 ===');
        console.log(vcamUid);
        console.log(replayMediaTwo.length);
        console.log(replayMediaTwo[0]);
        //== 啟動回放 ==
        document.addEventListener('DOMContentLoaded', initAppTwo(replayMediaTwo));
    });
}