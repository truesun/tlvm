//=== domain更換 ===
// var domain = "https://whitenight.work";
//==================
//時間軸資料
var timeline, lang;
var timelineDataCont = {
    "events": []
};
$(document).ready(function () {
    $('.history-timeline-tit-box').append('<div id="testInd"></div>');
    $('.history-timeline-tit-box').append('<div id="testData"></div>');
    //加上如果點擊標籤的話 顯示標籤
    $('.history-timeline-area').prepend('<div id="timeLineTag" class="time-line-tag-box"></div>');
    //加上搜尋無資料的話
    $('.history-timeline-area').append('<div id="timeLineNone" class="timeline-none"></div>');
    //一些JSON設定
    // $.ajaxSetup({
    //     async: false,//異步設定(false是會按照頁面JS執行順序，true是不會同步執行會先跑JS 在AJAX) 
    //     crossDomain: true,
    // });
    //api type1 = 時期，type2 = 標籤id，type3 = 關鍵字查詢
    var srcVal = $('#searchkey').val();
    console.log('搜尋條件');
    console.log(srcVal);
    //語系ＡＰＩ
    var apiLink, timeId;
    lang = getCookie('CacheLang');//語系
    // ?Id = 391 & Kind=1 & LIid=1 如果Kind1 id就是事件id LIid是時期id
    // ? Id = 1 & Kind=2 如果Kind2 id就是時期id
    var kind = getLinkVal('Kind');
    if (kind == '2') {
        timeId = getLinkVal('Id') || getCookie('Id');//時期id
    } else {
        timeId = getLinkVal('LIid') || getCookie('Id');
    }
    console.log('時期ID', timeId);
    // var timeId = getCookie('Id');//時期id
    if (lang == 'zh') {
        //如果搜尋有值
        if (srcVal) {
            apiLink = '/Api/WebServiceCn?Type=3&id=' + srcVal;
        } else {//如果搜尋沒有值
            apiLink = '/Api/WebServiceCn?Type=1&id=' + timeId;
        }
        getTimeLineCn(apiLink);
    } else {
        //如果搜尋有值
        if (srcVal) {
            apiLink = '/Api/WebServiceEn?Type=3&id=' + srcVal;
        } else {//如果搜尋沒有值
            apiLink = '/Api/WebServiceEn?Type=1&id=' + timeId;
        }
        getTimeLineEn(apiLink);
    }

    var timelineData1 = {
        "events": [
            {
                "media": {
                    //- "url": "https://www.youtube.com/watch?v=iTW1bv2eO9Y",
                    "url": "https://3.bp.blogspot.com/-t4dYO2lBp30/W86x9PZ1ZyI/AAAAAAAAAEw/ps9t08kd-XQK81EUKTixWKGehDrEDmp3gCEwYBhgL/s400/%25E8%2594%25A1%25E5%25BB%25B7%25E8%2598%25AD%25E9%2580%25B2%25E5%25A3%25AB%25E7%25AC%25AC%2528%25E9%2599%25B3%25E5%25BF%2597%25E5%258B%25A4%25E6%2594%259D%25E5%25BD%25B1%25E5%258F%258A%25E6%258E%2588%25E6%25AC%258A%2529.jpg",
                    //https://www.youtube.com/watch?v=iTW1bv2eO9Y
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "09",
                    "day": "01",
                    "display_date": "1651/09/01"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"1",
                //- 	"hour":null,
                //- 	"minute":null,
                //- 	"second":null,
                //- 	"millisecond":null,
                //- 	"display_date":"1651/09/02"
                //- },
                // "end_date": {
                //     "month": "00",
                //     "day": "00",
                //     "year": "00",
                //     "display_date": "0000"
                // },
                "text": {
                    "headline": "<a herf='###'>陳第來臺，撰〈東番記〉</a>",
                    "text": "陳第隨沈有容來臺，〈東番記〉為最早實地踏查平埔族人生活之文獻。<div class='his-timeline-tag-box'><a href='###'>#臺灣文學</a><a href='###'>#文化</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a></div>",
                },
            },
            {
                "media": {
                    "url": "https://drive.google.com/file/d/1A4pA-GF43Fbu-3wNtLVr9p_q94bulRpE/view?usp=sharing",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    "day": "3",
                    "display_date": "1651/09/03"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"4",
                //- 	"hour":null,
                //- 	"minute":null,
                //- 	"second":null,
                //- 	"millisecond":null,
                //- 	"display_date":"1651/09/04"
                //- },
                "text": {
                    "headline": '<a href="https://toolkit.culture.tw/literatureinfo_155_36.html" target="_blank">《失聲畫眉》獲「第四屆自立晚報百萬大獎」</a>',
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。"
                }
            },
            {
                "media": {
                    "url": "https://www.youtube.com/watch?v=iTW1bv2eO9Y",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    "day": "5",
                    "display_date": "1651/09/05"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"6",
                //- 	"display_date":"1651/09/06"
                //- },
                "text": {
                    "headline": "沈光文來臺沈光文來臺沈光文來臺",
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。<div class='his-timeline-tag-box'><a href='###'>#臺灣文學</a><a href='###'>#文化</a><a href='###'>#臺灣</a></div>"
                }
            },
            {
                "media": {
                    "url": "img/his_timeline01.png",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    "day": "7",
                    "display_date": "1651/09/07"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"4",
                //- 	"display_date":"1651/09/04"
                //- },
                "text": {
                    "headline": "沈光文來臺沈光文來臺沈光文來臺",
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。"
                }
            },
            {
                "media": {
                    "url": "img/his_timeline01.png",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1653",
                    "month": "9",
                    "day": "7",
                    "display_date": "1651/09/07"
                },
                "text": {
                    "headline": "沈光文遇颱來臺",
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。"
                }
            },
        ]
    }
    var timelineData2 = {
        "events": [
            {
                "media": {
                    "url": "https://www.youtube.com/watch?v=iTW1bv2eO9Y",
                    //- "url": "https://3.bp.blogspot.com/-t4dYO2lBp30/W86x9PZ1ZyI/AAAAAAAAAEw/ps9t08kd-XQK81EUKTixWKGehDrEDmp3gCEwYBhgL/s400/%25E8%2594%25A1%25E5%25BB%25B7%25E8%2598%25AD%25E9%2580%25B2%25E5%25A3%25AB%25E7%25AC%25AC%2528%25E9%2599%25B3%25E5%25BF%2597%25E5%258B%25A4%25E6%2594%259D%25E5%25BD%25B1%25E5%258F%258A%25E6%258E%2588%25E6%25AC%258A%2529.jpg",
                    //https://www.youtube.com/watch?v=iTW1bv2eO9Y
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    //- "day":"1",
                    "display_date": "1651/09/01"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"1",
                //- 	"hour":null,
                //- 	"minute":null,
                //- 	"second":null,
                //- 	"millisecond":null,
                //- 	"display_date":"1651/09/02"
                //- },
                //- "end_date": {
                //- 	//- "month": "8",
                //- 	//- "day": "9",
                //- 	"year": "1964"
                //- 	//- "display_date": "1963/10/10"
                //- },
                "text": {
                    "headline": "<a herf='###'>陳第來臺，撰〈東番記〉</a>",
                    "text": "陳第隨沈有容來臺，〈東番記〉為最早實地踏查平埔族人生活之文獻。<div class='his-timeline-tag-box'><a href='###'>#臺灣文學</a><a href='###'>#文化</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a></div>",
                },
            },
        ]
    }
    //為外連加上另開視窗
    $('.tl-headline').find('a').each(function (ind, item) {
        $(this).attr('target', '_blank');
    });

    //- var historyTimelineSwiper = new Swiper('#historyTimelineSwiper', {
    //- 	//- speed: 400,
    //- 	//- spaceBetween: 100
    //- 	autoHeight: true,
    //- 	navigation: {
    //- 		nextEl: '#timeLineN',
    //- 		prevEl: '#timeLineP',
    //- 	},
    //- });
});
//=== 時間軸api 中文 ===
function getTimeLineCn(apiLink, click) {
    $.get(apiLink, function (data) {
        // console.log(data);
        console.log(JSON.parse(data));
        if (data == "[]") {
            $('#timeLineNone').show();
            $('#timeLineNone').html('無資料。');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        var eventData = JSON.parse(data);
        $.each(eventData, function (ind, val) {
            var startTime, endTime, timeStamp, title, tagListL, tagList, tagListTxt, eventsListData;
            //=== 時間字串格式 ===
            //開始時間
            if (val.StarttimeDD !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM + '/' + val.StarttimeDD;
            } else if (val.StarttimeMM !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM;
            } else {
                startTime = val.StarttimeYY;
            }
            //為了時間排序下時間戳
            timeStamp = Date.parse(new Date(startTime));
            //結束時間
            if (val.EndtimeDD !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM + '/' + val.EndtimeDD;
            } else if (val.EndtimeMM !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM;
            } else {
                endTime = val.EndtimeYY;
            }
            //=== 標題是否有連結 ===
            if (val.Title_url) {
                title = '<a href="' + val.Title_url + '" target="_blank">' + val.Title_tw + '</a>';
            } else {
                title = val.Title_tw;
            }
            //=== 是否有標籤 ===
            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_tw + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }
            //=== 如果沒有結束時間 ===
            if (!val.EndtimeYY) {
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        // "day": Number(val.StarttimeDD),
                        "display_date": startTime
                    },
                    "text": {
                        "headline": title,
                        "text": val.Content_tw + tagListTxt
                    }
                }
            } else {//=== 如果有結束時間 ===
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        // "day": val.StarttimeDD,
                        "display_date": startTime
                    },
                    "end_date": {
                        "year": val.EndtimeYY,
                        "month": val.EndtimeMM,
                        // "day": val.EndtimeDD,
                        "display_date": endTime
                    },
                    "text": {
                        "headline": title,
                        "text": val.Content_tw + tagListTxt
                    }
                }
            }
            timelineDataCont.events.push(eventsListData);
            //=== 照開始日期時間戳排序 ===
            // timelineDataCont.events = timelineDataCont.events.sort(function (a, b) {
            //     return a.timeSort - b.timeSort;
            // });
            //這個排序會因為瀏覽起定義不同而排序結果不同
            // timelineDataCont.events = timelineDataCont.events.sort(function (a, b) {
            //     return a.timeSort > b.timeSort ? 1 : -1;
            // });
        });
    });
    //點擊是因為點擊標籤TAG 回到第一筆就好
    if (click) {
        //時間軸
        var TimeLineControlOptions = {
            language: 'zh-tw',
            // start_at_slide: mylistIndex,//時間軸起始slide
            scale_factor: '1',
            // hash_bookmark: true,
        };
    } else {
        console.log('時間軸資料');
        console.log(timelineDataCont);
        if (!timelineDataCont.events.length) {
            $('#timeLineNone').show();
            $('#timeLineNone').html('無資料。');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        //?Id = 391 & Kind=1 & LIid=1 如果Kind1 id就是事件id LIid是時期id
        var fKind = getLinkVal('Kind');
        var srcTimeId;
        if (fKind == '1') {
            srcTimeId = getLinkVal('Id') || getCookie('Lid');
        } else {
            srcTimeId = '0';
        }
        console.log('事件ID', srcTimeId);
        var mylistIndex;
        if (srcTimeId !== '0') {
            mylistIndex = timelineDataCont.events.findIndex(function (item) {
                return item.timeId == srcTimeId;
            });
            //如果搜尋不到 也要等於0
            if (mylistIndex == '-1') {
                mylistIndex = 0;
            }
            console.log(mylistIndex);
        } else {
            mylistIndex = 0;
        }

        //時間軸
        var TimeLineControlOptions = {
            language: 'zh-tw',
            start_at_slide: mylistIndex,//時間軸起始slide
            scale_factor: '1',
            // hash_bookmark: true,
        };
    }

    timeline = new TL.Timeline('timelineArea', timelineDataCont, TimeLineControlOptions);
    //=== 點擊標籤搜尋 ===
    tagClick();
}
//=== 時間軸api 英文===
function getTimeLineEn(apiLink, click) {
    $.get(apiLink, function (data) {
        // console.log(data);
        console.log(JSON.parse(data));
        if (data == "[]") {
            $('#timeLineNone').show();
            $('#timeLineNone').html('No Data.');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        var eventData = JSON.parse(data);
        $.each(eventData, function (ind, val) {
            var startTime, endTime, timeStamp, title, tagListL, tagList, tagListTxt, eventsListData;
            //=== 時間字串格式 ===
            //開始時間
            if (val.StarttimeDD !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM + '/' + val.StarttimeDD;
            } else if (val.StarttimeMM !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM;
            } else {
                startTime = val.StarttimeYY;
            }
            //為了時間排序下時間戳
            timeStamp = Date.parse(new Date(startTime));
            //結束時間
            if (val.EndtimeDD !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM + '/' + val.EndtimeDD;
            } else if (val.EndtimeMM !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM;
            } else {
                endTime = val.EndtimeYY;
            }
            //=== 標題是否有連結 ===
            if (val.Title_url) {
                title = '<a href="' + val.Title_url + '" target="_blank">' + val.Title_en + '</a>';
            } else {
                title = val.Title_en;
            }
            //=== 是否有標籤 ===
            tagListL = JSON.parse(val.data.TagidArray);
            if (tagListL.length) {
                var tagList = [];
                $.each(tagListL, function (ind, val) {
                    tagList.push('<a data-tagid="' + val.Tagid + '" class="tagBtn">#' + val.Name_en + '</a>');
                });
                tagListTxt = '<div class="his-timeline-tag-box">' + tagList.join() + '</div>';
            } else {
                tagListTxt = '';
            }
            //=== 如果沒有結束時間 ===
            if (!val.EndtimeYY) {
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_en
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        // "day": val.StarttimeDD,
                        "display_date": startTime
                    },
                    "text": {
                        "headline": title,
                        "text": val.Content_en + tagListTxt
                    }
                }
            } else {//=== 如果有結束時間 ===
                eventsListData = {
                    "timeId": val.Lid,
                    "timeSort": timeStamp,
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_en
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        // "day": val.StarttimeDD,
                        "display_date": startTime
                    },
                    "end_date": {
                        "year": val.EndtimeYY,
                        "month": val.EndtimeMM,
                        // "day": val.EndtimeDD,
                        "display_date": endTime
                    },
                    "text": {
                        "headline": title,
                        "text": val.Content_en + tagListTxt
                    }
                }
            }
            timelineDataCont.events.push(eventsListData);
            //=== 照開始日期時間戳排序 ===
            // timelineDataCont.events = timelineDataCont.events.sort(function (a, b) {
            //     return a.timeSort - b.timeSort;
            // });
        });
    });

    if (click) {
        //時間軸
        var TimeLineControlOptions = {
            language: 'en',
            //- start_at_slide:'3',
            scale_factor: '1',
            // hash_bookmark: true,
        };
    } else {
        console.log('時間軸資料');
        console.log(timelineDataCont);
        if (!timelineDataCont.events.length) {
            $('#timeLineNone').show();
            $('#timeLineNone').html('No Data.');
            $('#timelineArea').hide();
            return;
        } else {
            $('#timelineArea').show();
        }
        //?Id = 391 & Kind=1 & LIid=1 如果Kind1 id就是事件id LIid是時期id
        var fKind = getLinkVal('Kind');
        var srcTimeId;
        if (fKind == '1') {
            srcTimeId = getLinkVal('Id') || getCookie('Lid');
        } else {
            srcTimeId = '0';
        }
        console.log('事件ID', srcTimeId);
        var mylistIndex;
        if (srcTimeId !== '0') {
            mylistIndex = timelineDataCont.events.findIndex(function (item) {
                return item.timeId == srcTimeId;
            });
            //如果搜尋不到 也要等於0
            if (mylistIndex == '-1') {
                mylistIndex = 0;
            }
            console.log(mylistIndex);
        } else {
            mylistIndex = 0;
        }

        //時間軸
        var TimeLineControlOptions = {
            language: 'en',
            start_at_slide: mylistIndex,//時間軸起始slide
            scale_factor: '1',
            // hash_bookmark: true,
        };
    }

    timeline = new TL.Timeline('timelineArea', timelineDataCont, TimeLineControlOptions);
    //=== 點擊標籤搜尋 ===
    tagClick();
}

//=== 點擊標籤搜尋 ===
function tagClick() {
    $('.tagBtn').on('click', function () {
        var tagId = $(this).data('tagid');
        var tagTxt = $(this).html();
        timelineDataCont = {
            "events": []
        };
        if (lang == 'zh') {
            apiLink = '/Api/WebServiceCn?Type=2&id=' + tagId;
            // apiLink = domain+'/WebServiceCn.aspx?Type=1&id=' + timeId + '&Type=2&id=' + tagId;
            getTimeLineCn(apiLink, true);
            //加上點選標籤顯示
            $('#timeLineTag').html('標籤：' + tagTxt);
        } else {
            apiLink = '/Api/WebServiceEn?Type=2&id=' + tagId;
            // apiLink = domain+'/WebServiceEn.aspx?Type=1&id=' + timeId + '&Type=2&id=' + tagId;
            getTimeLineEn(apiLink, true);
            //加上點選標籤顯示
            $('#timeLineTag').html('Tag：' + tagTxt);
        }
        // timeline = new TL.Timeline('timelineArea', timelineData2, TimeLineControlOptions);
        //為外連加上另開視窗
        $('.tl-headline').find('a').each(function (ind, item) {
            $(this).attr('target', '_blank');
        });
    });
}
