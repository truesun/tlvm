const tagCanvas = document.querySelector("#wordCloudBox");
let fontSize, rotate;
var tagArry = [
    ['政治事件', 5, 'http://demotlvm.site/'],
    ['女性文學', 4, 'http://demotlvm.site/'],
    ['美術', 3, 'http://demotlvm.site/'],
    ['小說', 3, 'http://demotlvm.site/'],
    ['現實主義', 3, 'http://demotlvm.site/'],
    ['羅馬字', 3, 'http://demotlvm.site/'],
    ['移工', 3, 'http://demotlvm.site/'],
    ['政治小說', 3, 'http://demotlvm.site/'],
    ['政治文學', 3, 'http://demotlvm.site/'],
    ['後殖民', 2, 'http://demotlvm.site//test'],
    ['政治小說', 2, 'http://demotlvm.site/'],
    ['留學生文學', 2, 'http://demotlvm.site/'],
    ['馬華文學', 2, 'http://demotlvm.site/'],
    ['高雄縣文學貢獻獎', 2, 'http://demotlvm.site/'],
    ['動物小說', 2, 'http://demotlvm.site/'],
    ['動畫', 2, 'http://demotlvm.site/'],
    ['新電影', 2, 'http://demotlvm.site/'],
    ['文學館', 2, 'http://demotlvm.site/'],
    ['日文小說', 2, 'http://demotlvm.site/'],
    ['洪建全兒童文學獎', 1, 'http://demotlvm.site/'],
    ['皇民化時期文學', 1, 'http://demotlvm.site/'],
    ['兒童文學獎童話獎', 1, 'http://demotlvm.site/'],
    ['府城文學貢獻獎', 1, 'http://demotlvm.site/'],
    ['客家文學', 1, 'http://demotlvm.site/'],
    ['後設', 1, 'http://demotlvm.site/'],
    ['後設小說', 1, 'http://demotlvm.site/'],
    ['報章雜誌', 1, 'http://demotlvm.site/'],
    ['散文', 1, 'http://demotlvm.site/'],
    ['離散文學', 1, 'http://demotlvm.site/'],
    ['短篇小說', 1, 'http://demotlvm.site/'],
    ['筆匯月刊', 1, 'http://demotlvm.site/'],
    ['白話文', 1, 'http://demotlvm.site/'],
    ['同志文學', 1, 'http://demotlvm.site/'],
    ['後殖民', 1, 'http://demotlvm.site/'],
    ['科幻小說', 1, 'http://demotlvm.site/'],
    ['純文學月刊', 1, 'http://demotlvm.site/'],
    ['高雄市文藝獎', 1, 'http://demotlvm.site/'],
    ['女性作家', 1, 'http://demotlvm.site/'],
    ['文學史', 1, 'http://demotlvm.site/'],
    ['兒童文學', 1, 'http://demotlvm.site/'],
    ['後現代', 1, 'http://demotlvm.site/'],
    ['混語文學', 1, 'http://demotlvm.site/'],
    ['現代文學', 1, 'http://demotlvm.site/'],
    ['現代主義', 1, 'http://demotlvm.site/'],
    ['現代詩', 1, 'http://demotlvm.site/'],
    ['現代戲劇', 1, 'http://demotlvm.site/'],
    ['華文文學', 1, 'http://demotlvm.site/'],
    ['評審委員推薦獎', 1, 'http://demotlvm.site/'],
    ['話劇金鼎獎', 1, 'http://demotlvm.site/'],
    ['榮譽文藝獎章', 1, 'http://demotlvm.site/'],
];
const enTagArry = [
    ['Transnational', 5, 'https://demotlov.site/test'],
    ['Books publication', 4, 'https://demotlov.site/test'],
    ['Literature activity', 3, 'https://demotlov.site/test'],
    ['Political incident', 3, 'https://demotlov.site/test'],
    ['Classical poem', 3, 'https://demotlov.site/test'],
    ['Novel', 3, 'https://demotlov.site/test'],
    ['Regional literature', 3, 'https://demotlov.site/test'],
    ['Gender novel', 3, 'https://demotlov.site/test'],
    ['Mixes the philology', 3, 'https://demotlov.site/test'],
    ['Latter colonizes', 2, 'https://demotlov.site/test'],
    ['Latter modern age', 2, 'https://demotlov.site/test'],
    ['Modern poem', 2, 'https://demotlov.site/test'],
    ['Latter supposes', 2, 'https://demotlov.site/test'],
    ['Taiwanese literature', 2, 'https://demotlov.site/test'],
    ['Prose', 2, 'https://demotlov.site/test'],
    ['Periodical magazine', 2, 'https://demotlov.site/test'],
    ['Being homesick', 2, 'https://demotlov.site/test'],
    ['Prize in literature', 2, 'https://demotlov.site/test'],
    ['Moves the labor', 2, 'https://demotlov.site/test'],
    ['Luo Mazi', 1, 'https://demotlov.site/test'],
    ['Vernacular literature', 1, 'https://demotlov.site/test'],
    ['Japanese creation', 1, 'https://demotlov.site/test'],
    ['Literature event', 1, 'https://demotlov.site/test'],
    ['Left wing literature', 1, 'https://demotlov.site/test'],
    ['Folk literature', 1, 'https://demotlov.site/test'],
    ['Literature collection', 1, 'https://demotlov.site/test'],
    ['Play', 1, 'https://demotlov.site/test'],
    ['Science fiction', 1, 'https://demotlov.site/test'],
    ['Realism', 1, 'https://demotlov.site/test'],
    ['Fine arts', 1, 'https://demotlov.site/test'],
    ['Political novel', 1, 'https://demotlov.site/test'],
    ['Memorial hall', 1, 'https://demotlov.site/test'],
    ['Nature writing', 1, 'https://demotlov.site/test'],
    ['Reports the literature', 1, 'https://demotlov.site/test'],
    ['228 literatures', 1, 'https://demotlov.site/test'],
    ['New movie', 1, 'https://demotlov.site/test'],
    ['Deductive', 1, 'https://demotlov.site/test'],
    ['Network literature', 1, 'https://demotlov.site/test'],
    ['River novel', 1, 'https://demotlov.site/test'],
    ['Cruel literature', 1, 'https://demotlov.site/test'],
    ['Literature anthology', 1, 'https://demotlov.site/test'],
    ['Youth novel', 1, 'https://demotlov.site/test'],
    ['Child literature', 1, 'https://demotlov.site/test'],
    ['Ballad', 1, 'https://demotlov.site/test'],
    ['Short story', 1, 'https://demotlov.site/test'],
    ['Yilan literature', 1, 'https://demotlov.site/test'],
    ['5 Animation', 1, 'https://demotlov.site/test'],
    ['Japanese novel', 1, 'https://demotlov.site/test'],
    ['Novel', 1, 'https://demotlov.site/test'],
    ['Translation', 1, 'https://demotlov.site/test'],
];
var colors = ['#4297ab', '#40c9e0', '#f35b5b', '#98ce44', '#ffae48'];
// let option = {
//     list: tagArry,
//     classes: "tag-cloud-item",
//     gridSize: Math.round(5 * $('#wordCloudBox').width() / 1200),
//     weightFactor: fontSize,
//     // weightFactor: function (size) {
//     //     return Math.pow(size, 2.4) * $('#canvas').width() / 1200;
//     // },
//     drawOutOfBound: false,//不允許字詞超出寬高
//     shrinkToFit: false,
//     fontFamily: '"Noto Sans TC", sans-serif',
//     color: function (word, weight) {
//         //4
//         if (weight === 1) {
//             console.log('===', colors[0]);
//             return colors[0];
//         } else if (weight === 2) {
//             return colors[1];
//         } else if (weight === 3) {
//             return colors[2];
//         } else if (weight === 4) {
//             return colors[3];
//         } else if (weight === 5) {
//             return colors[4];
//         }
//     },
//     rotateRatio: 0.5,
//     rotationSteps: 2,
//     backgroundColor: 'transparent',
//     shuffle: false,//隨機渲染
//     clearCanvas: false,
//     click: (item) => { clickItem(item); }
// };
$(document).ready(function () {
    var lang = getCookie('CacheLang');
    var apiLink;
    lang == 'zh' ? $('#wordCloudArea').addClass('cn') : $('#wordCloudArea').addClass('en');
    lang == 'zh' ? rotate = 0 : rotate = 0;//0.5是直橫 0只有橫
    lang == 'zh' ? apiLink = '/Api/WordArt?Type=1' : apiLink = '/Api/WordArt?Type=2';
    $.get(apiLink, function (data) {
        var dataTxt = data.split('],[');
        //=== 文字列表 ===
        var dataList = dataTxt[0] + ']';
        console.log(JSON.parse(dataList));
        var wordList = JSON.parse(dataList);
        tagArry = [];
        $.each(wordList, function (ind, val) {
            var { name, Atotal, Link } = val;
            if (name != '') {
                tagArry.push([name, Number(Atotal), Link]);
            }
        });
        //=== 顏色列表 ===
        var colorsList = '[' + dataTxt[1];
        colors = JSON.parse(colorsList);
        console.log(colors);
    });
    const winw = document.body.offsetWidth;
    if (winw >= 480) {
        fontSize = 20;
    } else {
        fontSize = 12;
    }
    //== 重新設置字體大小 ==
    // option.weightFactor = fontSize;
    //== 啟動文字雲 ==
    tagArry.length != 0 ? setWordCloud(tagArry, colors, fontSize, rotate) : $('#wordCloudArea').remove();
});
let resizeTimer = null;
$(window).resize(function () {
    const winw = document.body.offsetWidth;
    if (winw >= 480) {
        fontSize = 20;
    } else {
        fontSize = 12;
    }
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        //== 重新設置字體大小 ==
        // option.weightFactor = fontSize;
        //== 啟動文字雲 ==
        tagArry.length != 0 ? setWordCloud(tagArry, colors, fontSize, rotate) : $('#wordCloudArea').remove();
        // setWordCloud(tagArry, colors, fontSize);
    }, 500);
});
//== 啟動文字雲 ==
function setWordCloud(tagArry, colors, fontSize, rotate) {
    var option = {
        list: tagArry,
        classes: "tag-cloud-item",
        gridSize: Math.round(5 * $('#wordCloudBox').width() / 1200) + 3,
        weightFactor: fontSize,
        // weightFactor: function (size) {
        //     return Math.pow(size, 2.4) * $('#canvas').width() / 1200;
        // },
        drawOutOfBound: false,//不允許字詞超出寬高
        shrinkToFit: false,
        fontFamily: '"Noto Sans TC", sans-serif',
        color: function (word, weight) {
            //4
            if (weight === 1) {
                return colors[0];
            } else if (weight === 2) {
                return colors[1];
            } else if (weight === 3) {
                return colors[2];
            } else if (weight === 4) {
                return colors[3];
            } else if (weight === 5) {
                return colors[4];
            }
        },
        rotateRatio: rotate,
        rotationSteps: 2,
        backgroundColor: 'transparent',
        shuffle: false,//隨機渲染
        clearCanvas: false,
        click: (item) => {
            clickItem(item);
        }
    };
    WordCloud(tagCanvas, option);
    tagCanvas.addEventListener('wordcloudstop', function (e) {
        // loop over all added elements (by class name)
        document.querySelectorAll('.tag-cloud-item').forEach(function (element) {
            const text = element.innerHTML;
            element.innerHTML = `<a target="_blank" style="color: inherit;">${text}</a>`;
        });
        $('.tag-cloud-item').find('a').each(function (ind, val) {
            var valTxt = $(val).text();
            if (valTxt == '') {
                $(val).remove();
            }
        });
    });
}
function clickItem(item) {
    // console.log(item[2]);
    window.open(item[2], '_blank');
    // location.href = item[2];
}