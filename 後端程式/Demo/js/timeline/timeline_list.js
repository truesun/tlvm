$(document).ready(function () {
    //時間軸資料
    var timeline;
    var timelineData = {
        "events": []
    };
    //一些JSON設定
    $.ajaxSetup({
        async: false,//異步設定(false是會按照頁面JS執行順序，true是不會同步執行會先跑JS 在AJAX) 
        crossDomain: true,
    });
    $.get('http://whitenight.work/WebServiceCn.aspx?id=1', function (data) {
        console.log(data);
        console.log(JSON.parse(data));
        var eventData = JSON.parse(data);
        $.each(eventData, function (ind, val) {
            var startTime, endTime, title, eventsListData;
            //時間字串格式
            //開始時間
            if (val.StarttimeDD !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM + '/' + val.StarttimeDD;
            } else if (val.StarttimeMM !== "00") {
                startTime = val.StarttimeYY + '/' + val.StarttimeMM;
            } else {
                startTime = val.StarttimeYY;
            }
            //結束時間
            if (val.EndtimeDD !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM + '/' + val.EndtimeDD;
            } else if (val.EndtimeMM !== "00") {
                endTime = val.EndtimeYY + '/' + val.EndtimeMM;
            } else {
                endTime = val.EndtimeYY;
            }
            //標題是否有連結
            if (val.Title_url) {
                title = '<a href="' + val.Title_url + '" target="_blank">' + val.Title_tw + '</a>';
            } else {
                title = val.Title_tw;
            }
            //如果沒有結束時間
            if (!val.EndtimeYY) {
                eventsListData = {
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "day": val.StarttimeDD,
                        "display_date": startTime
                    },
                    "text": {
                        "headline": title,
                        "text": val.Content_tw
                    }
                }
            } else {
                eventsListData = {
                    "media": {
                        "url": val.Comefrom_url,
                        "credit": val.Comefrom_tw
                    },
                    "start_date": {
                        "year": val.StarttimeYY,
                        "month": val.StarttimeMM,
                        "day": val.StarttimeDD,
                        "display_date": startTime
                    },
                    "end_date": {
                        "year": val.EndtimeYY,
                        "month": val.EndtimeMM,
                        "day": val.EndtimeDD,
                        "display_date": endTime
                    },
                    "text": {
                        "headline": title,
                        "text": val.Content_tw
                    }
                }
            }
            timelineData.events.push(eventsListData);
        });

    });
    // var aa = '{"id":"1"},{"id":"2"}';
    // console.log(JSON.parse(aa));

    //時間軸
    var TimeLineControlOptions = {
        language: 'zh-tw',
        //- start_at_slide:'3',
        scale_factor: '1',
        // hash_bookmark: true,
    };

    var timelineData1 = {
        "events": [
            {
                "media": {
                    //- "url": "https://www.youtube.com/watch?v=iTW1bv2eO9Y",
                    "url": "https://3.bp.blogspot.com/-t4dYO2lBp30/W86x9PZ1ZyI/AAAAAAAAAEw/ps9t08kd-XQK81EUKTixWKGehDrEDmp3gCEwYBhgL/s400/%25E8%2594%25A1%25E5%25BB%25B7%25E8%2598%25AD%25E9%2580%25B2%25E5%25A3%25AB%25E7%25AC%25AC%2528%25E9%2599%25B3%25E5%25BF%2597%25E5%258B%25A4%25E6%2594%259D%25E5%25BD%25B1%25E5%258F%258A%25E6%258E%2588%25E6%25AC%258A%2529.jpg",
                    //https://www.youtube.com/watch?v=iTW1bv2eO9Y
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "09",
                    "day": "01",
                    "display_date": "1651/09/01"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"1",
                //- 	"hour":null,
                //- 	"minute":null,
                //- 	"second":null,
                //- 	"millisecond":null,
                //- 	"display_date":"1651/09/02"
                //- },
                // "end_date": {
                //     "month": "00",
                //     "day": "00",
                //     "year": "00",
                //     "display_date": "0000"
                // },
                "text": {
                    "headline": "<a herf='###'>陳第來臺，撰〈東番記〉</a>",
                    "text": "陳第隨沈有容來臺，〈東番記〉為最早實地踏查平埔族人生活之文獻。<div class='his-timeline-tag-box'><a href='###'>#臺灣文學</a><a href='###'>#文化</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a></div>",
                },
            },
            {
                "media": {
                    "url": "https://drive.google.com/file/d/1A4pA-GF43Fbu-3wNtLVr9p_q94bulRpE/view?usp=sharing",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    "day": "3",
                    "display_date": "1651/09/03"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"4",
                //- 	"hour":null,
                //- 	"minute":null,
                //- 	"second":null,
                //- 	"millisecond":null,
                //- 	"display_date":"1651/09/04"
                //- },
                "text": {
                    "headline": "沈光文遇颱來臺",
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。"
                }
            },
            {
                "media": {
                    "url": "https://www.youtube.com/watch?v=iTW1bv2eO9Y",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    "day": "5",
                    "display_date": "1651/09/05"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"6",
                //- 	"display_date":"1651/09/06"
                //- },
                "text": {
                    "headline": "沈光文來臺沈光文來臺沈光文來臺",
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。<div class='his-timeline-tag-box'><a href='###'>#臺灣文學</a><a href='###'>#文化</a><a href='###'>#臺灣</a></div>"
                }
            },
            {
                "media": {
                    "url": "img/his_timeline01.png",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    "day": "7",
                    "display_date": "1651/09/07"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"4",
                //- 	"display_date":"1651/09/04"
                //- },
                "text": {
                    "headline": "沈光文來臺沈光文來臺沈光文來臺",
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。"
                }
            },
            {
                "media": {
                    "url": "img/his_timeline01.png",
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1653",
                    "month": "9",
                    "day": "7",
                    "display_date": "1651/09/07"
                },
                "text": {
                    "headline": "沈光文遇颱來臺",
                    "text": "沈光文以詩文記錄臺灣風土，被譽為「海東文獻初祖」。"
                }
            },
        ]
    }
    var timelineData2 = {
        "events": [
            {
                "media": {
                    "url": "https://www.youtube.com/watch?v=iTW1bv2eO9Y",
                    //- "url": "https://3.bp.blogspot.com/-t4dYO2lBp30/W86x9PZ1ZyI/AAAAAAAAAEw/ps9t08kd-XQK81EUKTixWKGehDrEDmp3gCEwYBhgL/s400/%25E8%2594%25A1%25E5%25BB%25B7%25E8%2598%25AD%25E9%2580%25B2%25E5%25A3%25AB%25E7%25AC%25AC%2528%25E9%2599%25B3%25E5%25BF%2597%25E5%258B%25A4%25E6%2594%259D%25E5%25BD%25B1%25E5%258F%258A%25E6%258E%2588%25E6%25AC%258A%2529.jpg",
                    //https://www.youtube.com/watch?v=iTW1bv2eO9Y
                    "credit": "沈有容《閩海贈言》",
                },
                "start_date": {
                    "year": "1651",
                    "month": "9",
                    //- "day":"1",
                    "display_date": "1651/09/01"
                },
                //- "end_date":{
                //- 	"year":"1651",
                //- 	"month":"9",
                //- 	"day":"1",
                //- 	"hour":null,
                //- 	"minute":null,
                //- 	"second":null,
                //- 	"millisecond":null,
                //- 	"display_date":"1651/09/02"
                //- },
                //- "end_date": {
                //- 	//- "month": "8",
                //- 	//- "day": "9",
                //- 	"year": "1964"
                //- 	//- "display_date": "1963/10/10"
                //- },
                "text": {
                    "headline": "<a herf='###'>陳第來臺，撰〈東番記〉</a>",
                    "text": "陳第隨沈有容來臺，〈東番記〉為最早實地踏查平埔族人生活之文獻。<div class='his-timeline-tag-box'><a href='###'>#臺灣文學</a><a href='###'>#文化</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a><a href='###'>#臺灣</a></div>",
                },
            },
        ]
    }
    timeline = new TL.Timeline('timelineArea', timelineData1, TimeLineControlOptions);
    // $('#test').on('click', function () {
    //     timeline = new TL.Timeline('timelineArea', timelineData2, TimeLineControlOptions);
    // });
    //- var historyTimelineSwiper = new Swiper('#historyTimelineSwiper', {
    //- 	//- speed: 400,
    //- 	//- spaceBetween: 100
    //- 	autoHeight: true,
    //- 	navigation: {
    //- 		nextEl: '#timeLineN',
    //- 		prevEl: '#timeLineP',
    //- 	},
    //- });
});